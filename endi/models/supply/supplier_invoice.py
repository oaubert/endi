import datetime

from beaker.cache import cache_region
from sqlalchemy import distinct
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    func,
    Integer,
    DateTime,
)
from sqlalchemy.orm import (
    relationship,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)
from endi_base.utils.date import (
    format_date,
)
from endi.compute.base_line import (
    BaseLineCompute,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi.compute.supplier_invoice import (
    SupplierInvoiceCompute,
)
from endi.models.node import Node
from endi.models.status import (
    PaidStatusHolderMixin,
    ValidationStatusHolderMixin,
)
from endi.models.task.actions import get_validation_state_manager
from endi.models.project.mixins import BusinessLinkedModelMixin
from endi.models.supply.mixins import LineModelMixin
from endi.models.services.supplier_invoice import (
    SupplierInvoiceService,
    SupplierInvoiceLineService,
)
from endi.utils import strings


class SupplierInvoice(
        SupplierInvoiceCompute,
        ValidationStatusHolderMixin,
        PaidStatusHolderMixin,
        Node,
):
    """
    A supplier invoice is linked :
    - 1..n SupplierOrder (constraint : same supplier and same percentage)
    - 1..n attachments (can map to multiple invoices form the supplier)
    - 0..n payments
    - 0..n lines (But that has little meaning with zero)
    """
    __tablename__ = 'supplier_invoice'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'supplier_invoice'}
    _endi_service = SupplierInvoiceService

    id = Column(
        ForeignKey('node.id'),
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    date = Column(
        DateTime(),
        default=datetime.datetime.now,
        info={'colanderalchemy': {'title': "Date de la facture"}},
    )

    supplier_id = Column(
        Integer,
        ForeignKey("supplier.id"),
        info={
            'export': {'exclude': True},
        },
    )

    supplier = relationship(
        "Supplier",
        primaryjoin="Supplier.id==SupplierInvoice.supplier_id",
        back_populates="invoices",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    @property
    def supplier_label(self):
        if self.supplier is not None:
            return self.supplier.label
        else:
            return 'indéfini'

    company_id = Column(
        Integer,
        ForeignKey('company.id'),
        info={
            'export': {'exclude': True},
            'colanderalchemy': {'exclude': True},
        },
        nullable=False,
    )

    company = relationship(
        "Company",
        primaryjoin="Company.id==SupplierInvoice.company_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    exported = Column(
        Boolean(),
        info={'colanderalchemy': {"title": "A déjà été exportée vers le logiciel de comptabilité ?"}},
        default=False
    )

    lines = relationship(
        "SupplierInvoiceLine",
        cascade="all, delete-orphan",
        order_by="SupplierInvoiceLine.id",
        info={
            "colanderalchemy": {
                "title": "Entrées",
                "description": "Vous pouvez soit lister le détail de "
                + "votre facture soit vous contenter d'un total global.",
            }
        }
    )

    validation_state_manager = get_validation_state_manager(
        'supplier_invoice',
        userid_attr='status_user_id',
    )

    def import_lines_from_order(self, supplier_order):
        """
        Copies all lines from a SupplierOrder
        """
        return self._endi_service.import_lines(
            dest_line_factory=SupplierInvoiceLine,
            src_obj=supplier_order,
            dest_obj=self,
            source_id_attr='source_supplier_order_line_id'
        )

    def get_default_name(self):
        return 'Facture {} du {}'.format(self.supplier.company_name, self.date)

    @classmethod
    def filter_by_year(cls, query, year):
        return cls._endi_service.filter_by_year(cls, query, year)

    # FIXME: factorize ?
    def check_validation_status_allowed(self, status, request, **kw):
        return self.validation_state_manager.check_allowed(
            status,
            self,
            request,
        )

    # FIXME: factorize ?
    def set_validation_status(self, status, request, **kw):
        return self.validation_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    @property
    def cae_percentage(self):
        # As far as now, we cannot handle invoices with
        # different percentages
        if len(self.supplier_orders) < 1:
            return 0  # Should not happen
        else:
            return self.supplier_orders[0].cae_percentage

    def get_company_id(self):
        # for company detection in menu display
        return self.company_id

    def get_company(self):
        # for dashboard
        return self.company

    def __json__(self, request):
        return dict(
            id=self.id,
            date=self.date,
            name=self.name,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),
            company_id=self.company_id,
            # user_id=self.user_id,
            paid_status=self.paid_status,
            # justified=self.justified,
            status=self.status,
            status_user_id=self.status_user_id,
            status_date=self.status_date.isoformat(),

            # From .supplier_orders
            orders_total=integer_to_amount(self.orders_total),
            orders_cae_total=integer_to_amount(self.orders_cae_total),
            orders_worker_total=integer_to_amount(self.orders_worker_total),
            orders_total_ht=integer_to_amount(self.orders_total_ht),
            orders_total_tva=integer_to_amount(self.orders_total_tva),
            cae_percentage=self.cae_percentage,

            supplier_name=self.supplier_label,
            lines=[line.__json__(request) for line in self.lines],
            payments=[payment.__json__(request) for payment in self.payments],
            attachments=[
                f.__json__(request)for f in self.children if f.type_ == 'file'
            ],
            supplier_orders=[order.id for order in self.supplier_orders],
        )


class SupplierInvoiceLine(
        LineModelMixin,
        BusinessLinkedModelMixin,
        DBBASE,
        BaseLineCompute,
):
    __tablename__ = 'supplier_invoice_line'
    __table_args__ = default_table_args
    _endi_service = SupplierInvoiceLineService
    parent_model = SupplierInvoice

    id = Column(
        Integer,
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    supplier_invoice_id = Column(
        Integer,
        ForeignKey("supplier_invoice.id", ondelete="cascade"),
        nullable=False,
        info={'colanderalchemy': {'exclude': True}}
    )

    supplier_invoice = relationship(
        "SupplierInvoice",
        primaryjoin="SupplierInvoice.id==SupplierInvoiceLine.supplier_invoice_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )
    source_supplier_order_line_id =  Column(
        Integer,
        ForeignKey("supplier_order_line.id", ondelete="SET NULL"),
        nullable=True, # NULL when created by hand
        info={'colanderalchemy': {'exclude': True}}
    )
    source_supplier_order_line = relationship(
        "SupplierOrderLine",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    expense_type = relationship(
        "ExpenseType",
        uselist=False,
        info={'colanderalchemy': {'exclude': True}}
    )

    def long_label(self):
        return '{} {}€ ({}) − {}'.format(
            self.description,
            strings.format_amount(self.total, grouping=False),
            self.expense_type.label,
            format_date(self.supplier_invoice.date),
        )

    @classmethod
    def linkable(cls, business):
        return cls._endi_service.linkable(cls, business)

    def __json__(self, request):
        ret = super(SupplierInvoiceLine, self).__json__(request)
        ret.update(dict(
            supplier_invoice_id=self.supplier_invoice_id,
        ))
        ret.update(dict(
            BusinessLinkedModelMixin.__json__(self, request),
        ))
        return ret


def get_supplier_invoices_years(kw=None):
    """
    Return a cached query for the years we have invoices configured

    :param kw: is here only for API compatibility
    """
    @cache_region("long_term", "supplier_invoices_years")
    def years():
        """
            return the distinct financial years available in the database
        """
        query = DBSESSION().query(
            distinct(func.year(SupplierInvoice.date))
        )
        query = query.order_by(SupplierInvoice.date)
        years = [year[0] for year in query]
        current = datetime.date.today().year
        if current not in years:
            years.append(current)
        return years
    return years()
