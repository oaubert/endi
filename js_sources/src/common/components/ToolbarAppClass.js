import Mn from 'backbone.marionette';

import ActionToolbar from '../views/ActionToolbar';

const ToolbarAppClass = Mn.Application.extend({
  /*
  Base classe for Toolbar management
  */
  region: '#js_actions',
  getResumeView(actions){
    return null;  // default
  },
  onStart(app, actions){
    const resume_view = this.getResumeView(actions);
    var view = new ActionToolbar({
      main: actions['main'],
      more: actions['more'],
      resume: resume_view
    });
    this.showView(view);
  }
});
export default ToolbarAppClass;
