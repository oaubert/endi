import Radio from 'backbone.radio';
import {
    strToFloat,
    getTvaPart,
    htFromTtc,
    formatAmount,
} from '../../math.js';
import BaseModel from '../../base/models/BaseModel.js';

const DiscountModel = BaseModel.extend({
    props: [
        'id',
        'amount',
        'tva',
        'ht',
        'description',
        'mode',
    ],
    validation: {
        description: {
            required: true,
            msg: "Remise : Veuillez saisir un objet",
        },
        amount: {
            required: true,
            pattern: "amount",
            msg: "Remise : Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        tva: {
            required: true,
            pattern: "number",
            msg: "Remise : Veuillez sélectionner une TVA"
        },
    },
    defaults: function(){
        console.log("Defaults");
        const result = {};
        result['mode'] = Radio.channel('config').request('get:options', 'compute_mode');

        // On va chercher la tva dans la session et par défaut 
        // on prendra la première que l'on trouve dans le document

        // On passe par le totalmodel pour avoir toutes les tvas du document
        let facade = Radio.channel('facade');
        console.log(facade);
        let totalmodel = facade.request('get:model', 'total');
        console.log(totalmodel);
        let tvas = totalmodel.tva_values();
        let first_tva = 0;
        if (tvas.length > 0){
            first_tva = tvas[0];
        }
        console.log("Discount defaults");
        console.log( Radio.channel('session').request('get', 'tva'));
        result['tva'] = Radio.channel('session').request('get', 'tva', first_tva);
        return result;
    },
    initialize(){
        this.user_session = Radio.channel('session');
        this.on('saved', this.onSaved, this);
    },
    onSaved(){
        this.user_session.request('set', 'tva', this.get('tva'));
        this.user_session.request('set', 'product_id', this.get('product_id'));
    },
    ht: function(){
        if (this.get('mode') == 'ht'){
            return -1 * strToFloat(this.get('amount'));
        } else {
            return htFromTtc(this.ttc(), this.tva_value());
        }
        
    },
    tva_value: function(){
        var tva = this.get('tva');
        if (tva < 0){
            tva = 0;
        }
        return tva;
    },
    tva: function(){
        return getTvaPart(this.ht(), this.tva_value());
    },
    ttc: function(){
        if (this.get('mode') == 'ht'){
            return this.ht() + this.tva();
        } else {
            return -1 * strToFloat(this.get('amount'));
        }
    },
    amount_label(){
        let result;
        if (this.get('mode') === 'ht'){
            result = formatAmount(this.ht(), false, false);
        } else {
            result = formatAmount(this.ttc(), false, false);
        }
        return result;
    }
});
export default DiscountModel;
