import _ from 'underscore';
import TaskLineCollection from './TaskLineCollection.js';
import BaseModel from "../../base/models/BaseModel.js";


const TaskGroupModel = BaseModel.extend({
    props: [
        'id',
        'order',
        'title',
        'description',
        'lines',
        'task_id',
    ],
    validation:{
        lines: function(value){
            console.log("Validating lines length");
            if (value.length === 0){
                return `Produit composé ${this.get('title')} : Veuillez saisir au moins un produit`;
            }
        }
    },
    initialize: function(){
        this.populate();
        this.on('change:id', this.populate.bind(this));
        this.listenTo(this.lines, 'saved', this.updateLines);
        this.listenTo(this.lines, 'destroyed', this.updateLines);
        this.listenTo(this.lines, 'synced', this.updateLines);
    },
    populate: function(){
        if (this.get('id')){
            this.lines = new TaskLineCollection(this.get('lines'));
            this.lines.url = this.url() + '/task_lines';
        }
    },
    updateLines(){
        console.log("updateLines in TaskGroupModel");
        this.set('lines', this.lines.toJSON());
    },
    ht: function(){
        return this.lines.ht();
    },
    tvaParts: function(){
        return this.lines.tvaParts();
    },
    ttc: function(){
        return this.lines.ttc();
    }
});
export default TaskGroupModel;
