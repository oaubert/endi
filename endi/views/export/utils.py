import deform
from sqlalchemy import or_
from sqlalchemy.orm import load_only

from endi_base.models.base import DBSESSION
from endi.models.task import Task, Invoice, CancelInvoice
from endi.forms.export import (
    PeriodSchema,
    AllSchema,
    BPFYearSchema,
    InvoiceNumberSchema,
    ExpenseIdSchema,
    ExpenseSchema,
    ExpenseAllSchema,
    SupplierInvoiceIdSchema,
    SupplierInvoiceSchema,
    SupplierInvoiceAllSchema,
)


EXPORT_BTN = deform.Button(name="submit", type="submit", title="Exporter")


HELPMSG_CONFIG = """Des éléments de configuration sont manquants, veuillez
configurer les informations comptables nécessaires à l'export des documents,
<a onclick="window.openPopup('{0}');" href='#'>Ici</a>"""


def find_task_status_date(official_number, year):
    """
    Query the database to retrieve a task with the given number and year and
    returns its status_date

    :param str official_number: The official number
    :param int year: The financial year associated to the invoice
    :returns: The document's status_date
    :rtype: datetime.dateime
    """
    return Task.find_task_status_date(official_number, year)


def query_tasks_for_export(
        start_number=None, end_number=None, year=None,
        start_date=None, end_date=None,
):
    query = Task.get_valid_tasks()
    if start_number:
        start_status_date = find_task_status_date(start_number, year)
        query = query.filter(
            Task.status_date >= start_status_date
        )

    if end_number:
        end_status_date = find_task_status_date(end_number, year)
        query = query.filter(
            Task.status_date <= end_status_date
        )

    if year:
        query = query.filter(
            or_(
                Invoice.financial_year == year,
                CancelInvoice.financial_year == year
            )
        )

    if start_date:
        query = query.filter(Task.date >= start_date)

    if end_date:
        query = query.filter(Task.date <= end_date)

    return query.order_by(Task.status_date)


def get_period_form(
    request,
    title="Exporter les factures sur une période donnée"
):
    """
        Return the period search form
    """
    schema = PeriodSchema(title=title)
    schema = schema.bind(request=request)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid='period_form',
    )


def get_all_form(
    request,
    counter,
    title="Exporter les factures non exportées",
):
    """
    Return a void form used to export all non-exported documents

    :param obj counter: An iterator used for form id generation
    """
    schema = AllSchema(title=title)
    schema = schema.bind(request=request)
    formid = 'all_form'
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid=formid,
        counter=counter,
    )


def get_expense_all_form(
    request,
    counter,
    title="Exporter les dépenses non exportées",
    prefix='',
):
    """
    Return a void form used to export all non-exported documents

    :param obj counter: An iterator used for form id generation
    """
    schema = ExpenseAllSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    formid = '%s_all_form' % prefix
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid=formid,
        counter=counter,
    )


def get_invoice_number_form(
    request,
    counter,
    title="Exporter les factures à partir d'un numéro"
):
    """
        Return the search form used to search invoices by number+year
    """
    schema = InvoiceNumberSchema(title=title)
    schema = schema.bind(request=request)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid='invoice_number_form',
        counter=counter,
    )


def get_expense_id_form(request, counter, title, prefix='expense'):
    """
    Return a form for expense export by id
    :param counter: the iterator used to insert various forms in the same page
    """
    schema = ExpenseIdSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid="%s_id_form" % prefix,
        counter=counter,
    )


def get_expense_form(request, counter, title, prefix='expense'):
    """
    Return a form for expense export
    :param obj request: Pyramid request object
    :returns: class:`deform.Form`

    """
    schema = ExpenseSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid="%s_main_form" % prefix,
        counter=counter,
    )


def get_supplier_invoice_all_form(
    request,
    counter,
    title="Exporter les factures fournisseurs non exportées",
    prefix='',
):
    """
    Return a void form used to export all non-exported supplier invoices

    :param obj counter: An iterator used for form id generation
    """
    schema = SupplierInvoiceAllSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    formid = '%s_all_form' % prefix
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid=formid,
        counter=counter,
    )


def get_supplier_invoice_id_form(request, counter, title, prefix='supplier_invoice'):
    """
    Return a form for supplier invoice export by id
    :param counter: the iterator used to insert various forms in the same page
    """
    schema = SupplierInvoiceIdSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid="%s_id_form" % prefix,
        counter=counter,
    )


def get_supplier_invoice_form(request, counter, title, prefix='supplier_invoice'):
    """
    Return a form for supplier invoice export
    :param obj request: Pyramid request object
    :returns: class:`deform.Form`

    """
    schema = SupplierInvoiceSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid="%s_main_form" % prefix,
        counter=counter,
    )


def get_bpf_year_form(request, title, prefix='bpf'):
    """
    :param obj request: Pyramid request object
    :rtype: `deform.Form`
    :returns: a form for bpf export (agregate of BusinessBPFData)
    """
    schema = BPFYearSchema(title=title)
    schema = schema.bind(request=request, prefix=prefix)
    return deform.Form(
        schema=schema,
        buttons=(EXPORT_BTN,),
        formid="%s_main_form" % prefix,
        counter=0,
    )
