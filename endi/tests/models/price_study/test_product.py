
def test_product_compute(price_study, mk_price_study_product):
    product = mk_price_study_product(
        supplier_ht=100000,
        general_overhead=0.11,
        margin_rate=0.12,
        quantity=3.33,
        study=price_study,
    )
    assert product.flat_cost() == 100000
    assert int(product.cost_price()) == 111000
    assert int(product.intermediate_price()) == 126136
    assert int(product.unit_ht()) == 126136
    # In int computing : assert int(product.compute_total_ht()) == 420032
    assert int(product.compute_total_ht()) == 420034

    product.on_before_commit('add')
    assert int(product.ht) == 126136
    assert int(product.total_ht) == 420034
