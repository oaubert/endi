<%inherit file="${context['main_template'].uri}" />

<%block name='afteractionmenu'>
<div class='row'>
    % if activities:
        <div class='message neutral'>
            <p>
                <span class="icon" role="presentation"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span>
                Vous avez ${len(activities)} rendez-vous programmés&nbsp;:
            </p>
            <ul class='list_count activities layout flex two_cols'>
            % for activity in activities[:5]:
                <li>
                    <a href="${activity.url}">
                        <span class='activity_time'>${api.format_datetime(activity.datetime)}</span>
                        <span>${', '.join(api.format_account(p) for p in activity.participants)}</span>
                    </a>
                </li>
                % endfor
            </ul>
        </div>
    % endif
</div>
</%block>

<%block name="content">
<div class='layout flex dashboard'>
    <div class='columns'>

        <!-- DOCUMENTS EN ATTENTE DE VALIDATION -->
        % for dataset, table_title, perm, icon, file_hint in (\
            (estimations, "Devis en attente de validation", "valid.estimation", "file-list", "Voir le devis"), \
            (invoices, "Factures et Avoirs en attente de validation", "valid.invoice", "file-invoice-euro", "Voir la facture"), \
            (supply_docs, "Commandes et factures fournisseur", "valid.supplier_order", "box", "Voir le document"), \
        ):
            % if request.has_permission(perm):
            <div class="dash_elem">
                <h2>
                    <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg></span>
                    <span>${table_title}</span>
                </h2>
                <div class='panel-body'>
                    <table class="hover_table">
                        <caption class="screen-reader-text">Liste des ${table_title}</caption>
                        <thead>
                            <tr>
                                <th scope="col" class='col_text'>Nom</th>
                                <th scope="col" class='col_text'>Enseigne</th>
                                <th scope="col" class='col_date'>Demandé le</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for task in dataset:
                                <tr class="clickable-row" data-href="${task.url}" title="${file_hint}&nbsp;: ${task.name}">
                                    <td class='col_text'><a href="${task.url}">${task.name}</a></td>
                                    <td class='col_text'>${task.get_company().name}</td>
                                    <td class='col_date'>${api.format_date(task.status_date)}</td>
                                </tr>
                            % endfor
                            % if not dataset:
                                <tr><td class="col_text" colspan='3'>Aucun document en attente</td></tr>
                            % endif
                        </tbody>
                    </table>
                </div>
            </div>
            % endif
        % endfor

        <!-- RENDEZ-VOUS / ACTIVITES A VENIR -->
        <div class="dash_elem">
            <h2>
                <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#calendar-alt"></use></svg></span>
                <a href="${request.route_path('activities', _query=dict(__formid__='deform', conseiller_id=request.user.id))}" title="Voir tous les Rendez-vous & Activités">
                    Rendez-vous & Activités à venir
                	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg>
                </a>
            </h2>
            <div class='panel-body'>
                <table class="hover_table">
                    <caption class="screen-reader-text">Liste de mes rendez-vous et activités à venir</caption>
                    <thead>
                        <tr>
                            <th scope="col" class="col_date">Date</th>
                            <th scope="col" class="col_text">Participant</th>
                            <th scope="col" class="col_text">Mode</th>
                            <th scope="col" class="col_text">Nature du rendez-vous</th>
                        </tr>
                    </thead>
                    <tbody>
                        % for activity in activities:
                            <tr class="clickable-row" data-href="${activity.url}" title="Voir le détail du rendez-vous ou de l’activité">
                                <td class="col_date">${api.format_datetime(activity.datetime)}</td>
                                <td class="col_text">
                                    <ul>
                                        % for participant in activity.participants:
                                            <li>${api.format_account(participant)}</li>
                                        % endfor
                                    </ul>
                                </td>
                                <td class="col_text">${activity.mode}</td>
                                <td class="col_text">${activity.type_object.label}</td>
                            </tr>
                        % endfor
                        % if not activities:
                            <tr><td class="col_text" colspan='4'><em>Aucune activité n'est prévue</em></td></tr>
                        % endif
                    </tbody>
                </table>
            </div>
        </div>

    % if request.has_permission('admin_expense'):
    
        <!-- DEPENSES EN ATTENTE DE VALIDATION -->
        <div class="dash_elem">
            <h2>
                <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#credit-card"></use></svg></span>
                <a href="/expenses" title="Voir toutes les Notes de dépense">
                    Notes de dépense en attente de validation
                	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg>
                </a>
            </h2>
            <div class='panel-body'>
                <table class="hover_table">
                    <caption class="screen-reader-text">Liste des notes de dépenses en attente de validation</caption>
                    <thead>
                        <tr>
                            <th scope="col" class="col_text">Période</th>
                            <th scope="col" class="col_text">Entrepreneur</th>
                            <th scope="col" class="col_date">Demandé le</th>
                        </tr>
                    </thead>
                    <tbody>
                        % for expense in expenses:
                            <tr class="clickable-row" data-href="${expense.url}" title="Voir la note de dépenses&nbsp;: ${api.month_name(expense.month)} ${expense.year} pour ${api.format_account(expense.user)}">
                                <td class="col_text"><a href="${expense.url}">${api.month_name(expense.month)} ${expense.year}</a></td>
                                <td class="col_text">${api.format_account(expense.user)}</td>
                                <td class="col_date">${api.format_date(expense.status_date)}</td>
                            </tr>
                        % endfor
                        % if not expenses:
                            <tr><td class="col_text" colspan='3'><em>Aucune note de dépenses en attente</em></td></tr>
                        % endif
                    </tbody>
                </table>
            </div>
        </div>
% endif

    </div>
</div>
</%block>
