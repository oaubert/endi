"""
    Json API views
"""


def json_model_view(request):
    """
        Return a json representation of a model
    """
    return request.context


def includeme(config):
    """
        Configure the views for this module
    """
    for route_name in "project", "company", "customer":
        config.add_view(
            json_model_view,
            route_name=route_name,
            renderer='json',
            request_method='GET',
            xhr=True,
            permission='view_%s' % route_name
        )
