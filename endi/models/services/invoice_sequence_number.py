import string

from endi_base.models.base import DBSESSION
from endi.models.task.sequence_number import (
    GlobalInvoiceSequence,
    MonthInvoiceSequence,
    MonthCompanyInvoiceSequence,
    SequenceNumber,
    YearInvoiceSequence,
)


ALLOWED_VARS = ['YYYY', 'YY', 'MM', 'ANA']


class InvoiceNumberFormatter(string.Formatter):
    """
    str.format()-like but with custom vars to allow applying an invoice number
    template. containing vars and sequence numbers.
    """
    def __init__(self, invoice, sequences_map):
        self._invoice = invoice
        self._sequences_map = sequences_map

    def _get_var_value(self, key):
        if key == 'YYYY':
            return '{:%Y}'.format(self._invoice.date)
        elif key == 'YY':
            return '{:%y}'.format(self._invoice.date)
        elif key == 'MM':
            return '{:%m}'.format(self._invoice.date)
        elif key == 'ANA':
            return '{}'.format(self._invoice.company.code_compta)

    def _get_seq_value(self, key):
        return self._sequences_map[key].get_next_index(self._invoice)

    def get_value(self, key, args, kwargs):
        if key in ALLOWED_VARS:
            return self._get_var_value(key)
        elif key in self._sequences_map:
            return self._get_seq_value(key)
        else:
            return super(InvoiceNumberFormatter, self).get_value(
                key, args, kwargs)


class InvoiceNumberService(object):
    SEQUENCES_MAP = {
        'SEQGLOBAL': GlobalInvoiceSequence,
        'SEQYEAR': YearInvoiceSequence,
        'SEQMONTH': MonthInvoiceSequence,
        'SEQMONTHANA': MonthCompanyInvoiceSequence,
    }
    ALLOWED_KEYS = ALLOWED_VARS + list(SEQUENCES_MAP.keys())

    @classmethod
    def _validate_variable_names(cls, tpl_vars):
        for key in tpl_vars:
            if key is not None and key not in cls.ALLOWED_KEYS:
                raise ValueError(
                    "{{{}}} n'est pas une clef valide (disponibles : {})".format(
                        key,
                        ', '.join('{{{}}}'.format(i) for i in cls.ALLOWED_KEYS)
                    ))

    @classmethod
    def _vars_ensure_unicity(cls, var_names):
        """
        Test if the given template vars ensures number uniqueness

        :param list var_names: The list of variables 
        :rtype: bool
        """

        def has(var_name):
            return var_name in var_names

        reqs = [
            [
                'SEQGLOBAL',
                True
            ],
            [
                'SEQYEAR',
                has('YYYY') or has('YY')
            ],
            [
                'SEQMONTH',
                (has('YYYY') or has('YY')) and has('MM')
            ],
            [
                'SEQMONTHANA',
                (has('YYYY') or has('YY')) and has('MM') and has('ANA')
            ],
        ]
        unicity = False
        for var_name, req in reqs:
            if var_name in var_names and req:
                unicity = True
        return unicity

    @classmethod
    def _validate_generated_nums_uniqueness(cls, tpl_vars):
        """
        Check the given tpl_vars ensure uniqueness

        :raises ValueError: When the vars doesn't ensure uniqueness
        """
        unicity = cls._vars_ensure_unicity(tpl_vars)

        if not unicity:
            raise ValueError('Ce gabarit produit des numéros non uniques.')

    @classmethod
    def _get_vars_from_template(cls, template):
        """
        Collect string formatting variables used in template

        :param str template: The template string
        :returns: [
        :rtype: list
        """
        fmt = string.Formatter()
        # parse returns tuples 
        # In [4]: list(string.Formatter().parse(a))
        # Out[4]: [('', 'SEQYEAR', '', None), (' ', 'YYYY', '', None)]
        return [i[1] for i in fmt.parse(template)]

    @classmethod
    def validate_template(cls, template):
        """
        Validate the correctness of the invoice number template
        """
        tpl_vars = cls._get_vars_from_template(template)
        cls._validate_variable_names(tpl_vars)
        cls._validate_generated_nums_uniqueness(tpl_vars)

    @classmethod
    def get_involved_sequences(cls, invoice, template):
        """
        Tell which sequences are to be used and what indexes they will give

        :returns: the sequences that would be used by this template and their
           next index
        :rtype: list of couples [<sequence>, <sequence_number>]
        """
        out = []
        used_sequences = set()  # to avoid duplicates in out
        tpl_vars = cls._get_vars_from_template(template)

        for key in tpl_vars:
            if key in cls.SEQUENCES_MAP:

                seq = cls.SEQUENCES_MAP[key]
                if seq not in used_sequences:
                    out.append([seq, seq.get_next_index(invoice)])
                    used_sequences.add(seq)

        return out

    @classmethod
    def _ensure_not_used(cls, invoice_id, invoice_number):
        # Imported here to avoid circular dependencies
        from endi.models.task import Task, Invoice, CancelInvoice

        query = Task.query().with_polymorphic([Invoice, CancelInvoice])
        for i in query:
            print(i.id, i.official_number)
        query = query.filter(
            Task.official_number == invoice_number,
            Task.id != invoice_id,
            Task.legacy_number == False,
        ).scalar()

        if query is not None:
            # This case is exceptionnal, we can afford a crash here
            # Context manager will take care of rolling back
            # subtransaction.
            raise ValueError(
                'Invoice number collision, rolling back to avoid it'
            )

    @classmethod
    def assign_number(cls, invoice, template):
        """
        This function should be run within an SQL transaction to enforce
        sequence index unicity.
        """
        if invoice.official_number:
            raise ValueError('This invoice already have an official number')

        db = DBSESSION()
        formatter = InvoiceNumberFormatter(invoice, cls.SEQUENCES_MAP)
        invoice_number = formatter.format(template)

        involved_sequences = cls.get_involved_sequences(invoice, template)

        with db.begin_nested():
            # Create SequenceNumber objects (the index useages have not been
            # booked until now).
            for sequence, next_index in involved_sequences:
                sn = SequenceNumber(
                    sequence=sequence.db_key,
                    index=next_index,
                    task_id=invoice.id,
                )
                db.add(sn)
            invoice.official_number = invoice_number
            db.merge(invoice)
            # Only check for invoices using this number if the current
            # invoice number template should ensure unicity
            tpl_vars = cls._get_vars_from_template(template)
            
            if cls._vars_ensure_unicity(tpl_vars):
                cls._ensure_not_used(invoice.id, invoice_number)

        return invoice_number
