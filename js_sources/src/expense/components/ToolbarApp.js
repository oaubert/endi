import BaseToolbarAppClass from "../../common/components/ToolbarAppClass";
import ActionModel from "../../common/models/ActionModel";
import ToggleWidget from "../../widgets/ToggleWidget";

const ToolbarAppClass = BaseToolbarAppClass.extend({
    getResumeView(actions){
      let resume_view = null;
      if (! _.isUndefined(actions['justify'])){
        const model = new ActionModel(actions['justify']);
        resume_view = new ToggleWidget({model: model});
      }
      return resume_view;
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;
