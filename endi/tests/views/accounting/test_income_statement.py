
class TestYearGlobalGrid:
    def get_global_grid(self, grids=(), turnover=10000):
        from endi.views.accounting.income_statement_measures import (
            YearGlobalGrid,
        )
        return YearGlobalGrid(
            grids=grids,
            turnover=turnover
        )

    def test_compile_rows(
        self, income_statement_measure_type_categories, income_statement_measure_types,
        income_statement_measure_grid,
    ):
        global_grid = self.get_global_grid(
            grids=[income_statement_measure_grid],
            turnover=10000
        )
        print((global_grid.rows))

        assert global_grid.rows[0] == (
            income_statement_measure_types[0],
            [0, 0, 8000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8000, 80]
        )
        assert global_grid.rows[1] == (
            income_statement_measure_types[1],
            [0, 0, 2000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2000, 20]
        )
        assert global_grid.rows[2] == (
            income_statement_measure_types[2],
            [0, 0, -1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1000, -10]
        )
        assert global_grid.rows[3] == (
            income_statement_measure_types[3],
            [0, 0, -500, 0, 0, 0, 0, 0, 0, 0, 0, 0, -500, -5]
        )
        assert global_grid.rows[4] == (
            income_statement_measure_types[4],
            [0, 0, 8500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8500, 85]
        )
        assert global_grid.rows[5] == (
            income_statement_measure_types[5],
            [0, 0, -15, 0, 0, 0, 0, 0, 0, 0, 0, 0, -15, -0.15]
        )
        assert global_grid.rows[6] == (
            income_statement_measure_types[6],
            [0, 0, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9000, 90]
        )
