Guide de style
==============


Gestion des fichiers de style
-----------------------------

Les feuilles de style sont basées sur des sources SASS situées dans le répertoire css_sources.
On peut les compiler automatiquement à chaque modification avec la commande ``make css``.
Les fichiers .sass sont structurés en fragments correspondant aux diverses parties structurelles du site (mise en forme générale, menu, tableaux, etc… ).


Règles générales
----------------

Quand on signale l’absence de contenu (« Pas de produit », « Aucun contenu n’a été ajouté », « Pas de devis »…), mettre le texte correspondant en italique au moyen d’un élément ``<em>``.


Colonnes
--------

2 à 4 Colonnes 
~~~~~~~~~~~~~~

Pour structurer le contenu d’une ``<div>`` en deux à quatre colonnes, appliquer les classes ``layout`` et ``flex``, et une des classes ``two_cols`` ``three_cols`` ou  ``four_cols``. Cela répartit tous les éléments directement contenus dans cette ``<div>`` en 2, 3 ou 4 colonnes de largeur égale.

Pour les structures en 2 colonnes, 4 classes à ajouter à  ``layout flex two_cols`` permettent d’obtenir deux colonnes de largeurs différentes :

- ``third`` colonne de gauche 1/3, colonne de droite 2/3
- ``third_reverse`` colonne de gauche 2/3, colonne de droite 1/3
- ``quarter`` colonne de gauche 1/4, colonne de droite 3/4
- ``quarter_reverse`` colonne de gauche 3/4, colonne de droite 1/4

Rangs divisés en colonnes
~~~~~~~~~~~~~~~~~~~~~~~~~

Une ``<div>`` considérée comme un rang (particulièrement dans les formulaires) est structurée en colonnes en utilisant les classes ``col-md-1`` (1/12ème) à ``col-md-12`` (12/12èmes, 100%) sur chaque élément du rang. 
La gestion responsive de la largeur des colonnes dispense d’utiliser les classes de type ``col-xs`` ou autre, qui n’ont pas d’effet et n’espacent pas les colonnes de formulaires.


Tableaux
--------

Tableaux ``<table>``
~~~~~~~~~~~~~~~~~~~~

- La classe ``top_align_table`` permet d’aligner verticalement le contenu de chaque case du tbody en haut de case. À utiliser pour les tableaux dont certaines cases ont des contenus très longs, pour faciliter la lecture ligne à ligne. Sans cette classe, l’alignement vertical par défaut est centré.
- La classe ``hover_table`` permet d’ajouter une couleur au survol à chaque ligne du tableau. À utiliser quand une ou plusieurs actions peuvent être réalisées en cliquant sur une ligne ou sur le contenu des cases autres que ``col_actions``. Cf. :ref:`Navigation au clic sur les lignes de tableaux`
- La classe ``spaced_table`` permet d’ajouter de l’espace au-dessus et au-dessous du tableau.
- La classe ``full_width`` permet de forcer la largeur du tableau à 100% de son contenant.

Rangs ``<tr>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de chaque case du rang en haut de case. Cf. ``top_align_table`` pour l’utilisation.

En-têtes ``<th>``
~~~~~~~~~~~~~~~~~

- utiliser l’attribut scope pour définir si l’en-tête est un en-tête de colonne (``scope="col"``) ou un en-tête de ligne (``scope="row"``)

Cases ``<td>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de la case en haut de case. Cf. ``top_align_table`` pour l’utilisation.

Boutons d’action dans les lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Quand au maximum 4 actions faciles à différencier graphiquement sont réalisables sur une ligne, les boutons d’action sont insérés directement dans la colonne ``col_actions`` sous forme de boutons icônes.
- Quand il y a plus d’actions où qu’elles ne sont pas différenciables graphiquement, les boutons sont insérés dans un sous-menu qui s’ouvre au clic sur un bouton (icône ``#dots``)
- Il faut éviter de mélanger bouton d’action et bouton avec sous-menu dans une même case.

Pour éviter que les cases d’actions s’élargissent trop quand l’écran est large, ajouter sur le ``<td class="col_actions">`` une classe correspondant au nombre de boutons présents dans la case :

- ``width_one`` pour 1 bouton ( ``<td class="col_actions width_one">``)
- ``width_two`` pour 2 boutons ( ``<td class="col_actions width_two">``)
- ``width_three`` pour 3 boutons ( ``<td class="col_actions width_three">``)
- ``width_four`` pour 4 boutons ( ``<td class="col_actions width_four">``)

Navigation au clic sur les lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand une ligne de tableau concerne un document ou que l’action principale est de consulter une information, on permet la navigation en cliquant sur la ligne ou sur les cases de la ligne.

On ajoute ``onclick="…navigation…" title="Cliquer pour voir …" aria-label="Cliquer pour voir …"``

- Quand il n’y a pas de case d’action ou de lien vers une autre navigation dans la ligne on ajoute ce code sur le ``<tr>``
- Quand il y a une case d’action ou un ``<td>`` contenant un lien qui provoque une autre action que celle qu’on va ajouter, on ajoute ce code sur les autres ``<td>``.


Pop-ins
-------

Les pop-ins sont insérés à l’intérieur d’une balise ``<section class="modal_view">``  qui positionne la pop-in dans l’écran. C’est elle qu’on affiche ou cache avec par exemple ``style="display: none;"``.

Les trois classes ``size_small``,  ``size_middle`` et  ``size_large`` permettent d’obtenir des pop-ins de plus en plus grandes.

La ``<div role="dialog">`` réfère le titre de la pop-in pour l’accessiblité.

La ``<div class="modal_layout">`` permet la mise en forme du contenu.

Le ``<header>`` contient le bouton de fermeture et le titre de la pop-in (``<h2>`` avec un id référencé par la ``<div role="dialog">``).
            
Pour afficher des onglets, on peut ajouter la zone de navigation  ``<nav>`` optionnelle.
            
Le contenu est placé dans un ``<main>``.

Les boutons d’actions sur l’ensemble de la pop-in sont situés dans le ``<footer>``.


.. code-block:: html

	<section id="customer_add_form" class="modal_view size_middle" style="display: none;">
		<div role="dialog" id="" aria-modal="true" aria-labelledby="customer-forms_title">
			<div class="modal_layout">
				<header>
					<button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('customer_add_form'); return false;">
						<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
					</button>
					<h2 id="customer-forms_title">Ajouter un client</h2>
				</header>
				<nav>
					<ul class="nav nav-tabs modal-tabs" role="tablist" aria-label="Type de client">
						<li role="presentation" class="active">
							<a href="#companyForm" aria-controls="companyForm" role="tab" aria-selected="true" id="company">Personne morale</a>
						</li>
						<li role="presentation">
							<a href="#individualForm" aria-controls="individualForm" role="tab" aria-selected="false" id="individual" tabindex="-1">Personne physique</a>
						</li>
					</ul>
				</nav>
				<main>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active row" id="companyForm" aria-labelledby="company" tabindex="0">
							<h3>Titre</h3>
							Contenu
						</div>
						<div role="tabpanel" class="tab-pane row" id="individualForm" aria-labelledby="individual" tabindex="0" hidden>
							<h3>Titre</h3>
							Contenu
						</div>
					</div>
				</main>
				<footer>
					<button class='btn btn-primary' {{#if has_file_warning}}disabled aria-disabled='true'{{/if}} type='submit' name='submit' value='{{status}}'>{{label}}</button>
					<button class='btn' data-dismiss='modal'>Annuler</button>
				</footer>
			</div>
		</div>
	</section>



Boutons
-------

Quand on a un groupe de boutons, le bouton majeur (``class="btn-primary``) est toujours à gauche du ou des bouton·s mineur·s.

Les boutons **« Annuler »** ne prennent la classe ``negative`` que si ils correspondent à une destruction d’informations (pas la fermeture d’une popin ou d’un layer).


Barre de boutons
----------------

Les boutons d’action indépendants ou portant sur tout le contenu de la page sont affichés dans la barre de boutons présente en haut d’écran.

- Les boutons peuvent être séparés en groupes
- Les actions principales sont affichées en haut à gauche
- Les actions secondaires sont affichées en haut à droite


Formulaires
-----------

Les formulaires complexes sont structurés en blocs (``<fieldset>``).
Ces blocs sont séparés graphiquement avec les classes ``separate_block`` (ombre et espacement) et ``border_left_block`` (bordure colorée à gauche du bloc).

Quand un formulaire ne contient qu’un bloc, ne pas le séparer graphiquement (ne pas utiliser ``separate_block`` et ``border_left_block``).

Quand on veut présenter une donnée non modifiable dans un formulaire, utiliser la structure 


.. code-block:: html

	<span class="label">Nom de la donnée</span>
	<span class="data">Contenu de la donnée</span>

à la place de ``<label><input>``.


Icônes
------

Les icones sont insérées sous la forme d’un fragment SVG. On insère le fichier svg suivi de l’identifiant du fragment à utiliser.

``<svg><use href="/static/icons/endi.svg#identifiant"></use></svg>``

ou

``<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#identifiant"></use></svg>``


Une liste des icônes utilisées dans enDI et des identifiants correspondants est disponible ci-dessous :

.. raw:: html

    <iframe src="_static/icones.html" height="500px" width="100%"></iframe>
    <p>
    	<a href="_static/icones.html">Lien vers la liste des icônes</a>
    </p>


Icônes de téléchargement
~~~~~~~~~~~~~~~~~~~~~~~~

Pour les boutons permettant de télécharger un fichier :

- quand le format du fichier est inconnu ou fourni par l’utilisateur, utiliser #download

- quand le format est connu, utiliser l’icône correspondante (file-pdf, file-excel, file-csv,…)


Statuts
-------

Liste des styles et fragments d’icônes correspondant aux divers statuts des documents et actions.

+--------------------------+----------------+----------------------------+-----------------------+
| Statut                   | Statut en base | Classe du span             | Fragment svg          |
+==========================+================+============================+=======================+
| **Tous**                                                                                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Brouillon modifié        | draft          | icon status draft          | #pen                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Validation demandée      | caution        | icon status caution        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Validé(e)                | valid          | icon status valid          | #check-circle         |
+--------------------------+----------------+----------------------------+-----------------------+
| Invalidé(e)              | invalid        | icon status invalid        | #times-circle         |
+--------------------------+----------------+----------------------------+-----------------------+
| Non modifiable           | closed         | icon status closed         | #lock                 |
+--------------------------+----------------+----------------------------+-----------------------+
| **Devis**                                                                                      |
+--------------------------+----------------+----------------------------+-----------------------+
| Annulé, sans suite       | closed         | icon status closed         | #times                |
+--------------------------+----------------+----------------------------+-----------------------+
| Envoyé                   | valid          | icon status valid          | #envelope             |
+--------------------------+----------------+----------------------------+-----------------------+
| Signé                    | valid          | icon status valid          | #check                |
+--------------------------+----------------+----------------------------+-----------------------+
| Factures générées        | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| **Factures**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée partiellement      | partial_unpaid | icon status partial_unpaid | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Soldée                   | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Non payée depuis         | caution        | icon status caution        | #euro-slash           |
| moins de 45 jours        |                |                            |                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Non payée depuis         | invalid        | icon status invalid        | #euro-slash           |
| plus de 45 jours         |                |                            |                       |
+--------------------------+----------------+----------------------------+-----------------------+
| **Dépenses**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| En attende de validation | caution        | icon status caution        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée partiellement      | partial_unpaid | icon status partial_unpaid | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée intégralement      | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Justificatifs reçus      | valid          | icon status valid          | #file-check           |
+--------------------------+----------------+----------------------------+-----------------------+
| **Affaires**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| En cours                 | valid          | icon status valid          | #folder-open          |
+--------------------------+----------------+----------------------------+-----------------------+
| Clôturée                 | closed         | icon status closed         | #folder               |
+--------------------------+----------------+----------------------------+-----------------------+
| **Jobs**                                                                                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Planifié                 | planned        | icon status planned        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Terminé                  | completed      | icon status completed      | #check                |
+--------------------------+----------------+----------------------------+-----------------------+
| Échec                    | failed         | icon status failed         | #exclamation-triangle |
+--------------------------+----------------+----------------------------+-----------------------+
| **Écritures**                                                                                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Associée                 | valid          | icon status valid          | #link                 |
+--------------------------+----------------+----------------------------+-----------------------+
| Non associée             | caution        | icon status caution        | #exclamation-triangle |
+--------------------------+----------------+----------------------------+-----------------------+
| **Commandes**                                                                                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Annulée                  | closed         | icon status closed         | #times                |
+--------------------------+----------------+----------------------------+-----------------------+


Accessibilité
-------------

Title et aria-label
~~~~~~~~~~~~~~~~~~~

Sur un ``<button>`` sans contenu textuel (``class="icon only"``), les deux servent : le title comme info-bulle pour les visuels, le aria-label pour les lecteurs d’écran (qui ne lisent souvent pas le title).

Sur un ``<button>`` avec contenu textuel, ils ne servent que s’ils précisent le bouton (par exemple Bouton **« XLS »** avec title et aria-label « Exporter cette liste au format Excel (.xls) » ).
