"""
    Task computing tool
    Used to compute invoice, estimation or cancelinvoice totals
"""
import operator
from endi.models.tva import Tva
from endi.compute import math_utils
from endi.compute.task import (
    TaskComputeMixin,
    GroupComputeMixin,
    LineComputeMixin,
    DiscountLineMixin,
)


def get_default_tva():
    """
        Return the default tva
    """
    try:
        default_tva = Tva.get_default()
    except:
        default_tva = None

    if default_tva:
        return default_tva.value
    else:
        return 1960


class TaskCompute(TaskComputeMixin):
    """
        class A(TaskCompute):
            pass

        A.total()
    """
    task = None

    def __init__(self, task):
        TaskComputeMixin.__init__(self, task)

    def total_ht(self):
        """
            compute the HT amount
        """
        expenses_ht = getattr(self.task, "expenses_ht") or 0  # TODO refactore
        total_ht = self.groups_total_ht() - \
            self.discount_total_ht() + \
            expenses_ht
        return self.floor(total_ht)

    def get_tvas(self):
        """
            return a dict with the tvas amounts stored by tva
            {1960:450.56, 700:45}
        """
        expense = None

        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in list(group.get_tvas().items()):
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get(discount.tva, 0)
            val -= discount.tva_amount()
            ret_dict[discount.tva] = val

        expenses_ht = getattr(self.task, "expenses_ht", 0)
        tva_amount = 0
        if expenses_ht != 0:
            expense = self.get_expense_ht()
            tva_amount = expense.tva_amount()

        if tva_amount != 0 and expense is not None:
            val = ret_dict.get(expense.tva, 0)

            val += expense.tva_amount()
            ret_dict[expense.tva] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def get_tvas_by_product(self):
        """
        Return tvas stored by product type
        """
        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in list(group.get_tvas_by_product().items()):
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get("rrr", 0)
            val += discount.tva_amount()
            ret_dict["rrr"] = val

        expense_ht = getattr(self.task, "expenses_ht", 0)
        tva_amount = 0
        if expense_ht != 0:
            expense = self.get_expense_ht()
            tva_amount = expense.tva_amount()

        if tva_amount > 0:
            val = ret_dict.get("expense", 0)
            val += tva_amount
            ret_dict["expense"] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def tva_amount(self):
        """
            Compute the sum of the TVAs amount of TVA
        """
        return self.floor(
            sum(tva for tva in list(self.get_tvas().values()))
        )

    def no_tva(self):
        """
            return True if all the tvas are below 0
        """
        ret = True
        for key in self.get_tvas():
            if key >= 0:
                ret = False
                break
        return ret

    def total_ttc(self):
        """
            Compute the TTC total
        """
        return self.total_ht() + self.tva_amount()

    def total(self):
        """
            Compute TTC after tax removing
        """
        return self.total_ttc() + self.expenses_amount()

    def expenses_amount(self):
        """
            return the amount of the expenses
        """
        expenses = self.task.expenses or 0
        result = int(expenses)
        return result

    def get_expenses_tva(self):
        """
            Return the tva for the HT expenses
        """
        expenses_tva = getattr(self.task, "expenses_tva", -1)
        if expenses_tva == -1:
            self.task.expenses_tva = get_default_tva()
        return self.task.expenses_tva

    def get_expense_ht(self):
        """
            Return a line object for the HT expense handling
        """
        from endi.models.task import TaskLine  # couldn't figure out why global import failed
        return TaskLine(tva=self.get_expenses_tva(), cost=self.task.expenses_ht, quantity=1, mode='ht')

    def tva_ht_parts(self):
        """
        Return a dict with the HT amounts stored by corresponding tva value
        dict(tva=ht_tva_part,)
        for each tva value
        """
        ret_dict = {}
        lines = []
        for group in self.task.line_groups:
            lines.extend(group.lines)
        ret_dict = self.add_ht_by_tva(ret_dict, lines)
        ret_dict = self.add_ht_by_tva(ret_dict, self.task.discounts, operator.sub)
        expense = self.get_expense_ht()
        if expense.cost != 0:
            ret_dict = self.add_ht_by_tva(ret_dict, [expense])
        return ret_dict

    def tva_ttc_parts(self):
        """
        Return a dict with TTC amounts stored by corresponding tva
        """
        ret_dict = {}
        ht_parts = self.tva_ht_parts()
        tva_parts = self.get_tvas()

        for tva_value, amount in list(ht_parts.items()):
            ret_dict[tva_value] = amount + tva_parts.get(tva_value, 0)
        return ret_dict


class InvoiceCompute(object):
    """
        Invoice computing object
        Handles payments
    """
    task = None

    def __init__(self, task):
        self.task = task

    def payments_sum(self):
        """
        Return the amount covered by the recorded payments
        """
        return sum([payment.amount for payment in self.task.payments])

    def cancelinvoice_amount(self):
        """
        Return the amount covered by th associated cancelinvoices
        """
        result = 0
        for cancelinvoice in self.task.cancelinvoices:
            if cancelinvoice.status == 'valid':
                # cancelinvoice total is negative
                result += -1 * cancelinvoice.total()
        return result

    def paid(self):
        """
            return the amount that has already been paid
        """
        return self.payments_sum() + self.cancelinvoice_amount()

    def topay(self):
        """
        Return the amount that still need to be paid

        Compute the sum of the payments and what's part of a valid
        cancelinvoice
        """
        result = self.task.total() - self.paid()
        return result

    def tva_paid_parts(self):
        """
        return the amounts already paid by tva
        """
        result = {}
        for payment in self.task.payments:
            if payment.tva is not None:
                key = payment.tva.value
            else:
                key = list(self.task.tva_ht_parts().keys())[0]

            result.setdefault(key, 0)
            result[key] += payment.amount

        return result

    def tva_cancelinvoice_parts(self):
        """
        Returns the amounts already paid through cancelinvoices by tva
        """
        result = {}
        for cancelinvoice in self.task.cancelinvoices:
            if cancelinvoice.status == 'valid':
                ttc_parts = cancelinvoice.tva_ttc_parts()
                for key, value in list(ttc_parts.items()):
                    if key in result:
                        result[key] += value
                    else:
                        result[key] = value
        return result

    def topay_by_tvas(self):
        """
        Returns the amount to pay by tva part
        """
        result = {}
        paid_parts = self.tva_paid_parts()
        cancelinvoice_tva_parts = self.tva_cancelinvoice_parts()
        for tva_value, amount in list(self.task.tva_ttc_parts().items()):
            val = amount
            val = val - paid_parts.get(tva_value, 0)
            val = val + cancelinvoice_tva_parts.get(tva_value, 0)
            result[tva_value] = val
        return result


class EstimationCompute(object):
    """
        Computing class for estimations
        Adds the ability to compute deposit amounts ...
    """
    task = None

    def __init__(self, task):
        self.task = task

    def get_default_tva(self):
        """
            Silly hack to get a default tva for deposit and intermediary
            payments (configured ttc)
        """
        tvas = list(self.task.get_tvas().keys())
        return tvas[0]

    def deposit_amounts(self):
        """
            Return the lines of the deposit for the different amount of tvas
        """
        ret_dict = {}
        for tva, total_ht in list(self.task.tva_ht_parts().items()):
            ret_dict[tva] = self.task.floor(
                math_utils.percentage(total_ht, self.task.deposit)
            )
        return ret_dict

    def deposit_amount(self):
        """
            Compute the amount of the deposit
        """
        import warnings
        warnings.warn("deprecated", DeprecationWarning)
        if self.task.deposit > 0:
            total = self.task.total_ht()
            return self.task.floor(total * int(self.task.deposit) / 100.0)
        return 0

    def get_nb_payment_lines(self):
        """
            Returns the number of payment lines configured
        """
        return len(self.task.payment_lines)

    def paymentline_amounts(self):
        """
        Compute payment lines amounts in case of equal payment repartition:

            when manualDeliverables is 0

        e.g :

            when the user has selected 3 time-payment

        :returns: A dict describing the payments {'tva1': amount1, 'tva2':
            amount2}
        """
        ret_dict = {}
        totals = self.task.tva_ht_parts()
        deposits = self.deposit_amounts()
        # num_parts set the number of equal parts
        num_parts = self.get_nb_payment_lines()
        for tva, total_ht in list(totals.items()):
            rest = total_ht - deposits[tva]
            line_ht = rest / num_parts
            ret_dict[tva] = line_ht
        return ret_dict

    def manual_payment_line_amounts(self):
        """
            Computes the ht and tva needed to reach each payment line total

            self.payment_lines are configured with TTC amounts


            return a list of dict:
                [{tva1:ht_amount, tva2:ht_amount}]
            each dict represents a configured payment line
        """
        # Cette méthode recompose un paiement qui a été configuré TTC, sous
        # forme de part HT + TVA au regard des différentes tva configurées dans
        # le devis
        ret_data = []
        parts = self.task.tva_ht_parts()
        # On enlève déjà ce qui est inclu dans l'accompte
        for tva, ht_amount in list(self.deposit_amounts().items()):
            parts[tva] -= ht_amount

        for payment in self.task.payment_lines[:-1]:
            payment_ttc = payment.amount
            payment_lines = {}

            items = list(parts.items())
            for tva, total_ht in items:
                payment_ht = math_utils.compute_ht_from_ttc(
                    payment_ttc,
                    tva,
                    False,
                    division_mode=(self.task.mode != 'ttc'),
                )
                if total_ht >= payment_ht:
                    # Le total ht de cette tranche de tva est suffisant pour
                    # recouvrir notre paiement
                    # on la récupère
                    payment_lines[tva] = payment_ht
                    # On enlève ce qu'on vient de prendre de la tranche de tva
                    # pour le calcul des autres paiements
                    parts[tva] = total_ht - payment_ht
                    ret_data.append(payment_lines)
                    break
                else:
                    # On a besoin d'une autre tranche de tva pour atteindre
                    # notre paiement, on prend déjà ce qu'il y a
                    payment_lines[tva] = parts.pop(tva)
                    # On enlève la part qu'on a récupéré dans cette tranche de
                    # tva du total de notre paiement
                    payment_ttc -= total_ht + math_utils.compute_tva(
                        total_ht,
                        tva,
                    )

        # Ce qui reste c'est donc pour notre facture de solde
        sold = parts
        ret_data.append(sold)
        return ret_data

    # Computations for estimation display
    def deposit_amount_ttc(self):
        """
            Return the ttc amount of the deposit (for estimation display)
        """
        from endi.models.task import TaskLine
        if self.task.deposit > 0:
            total_ttc = 0
            for tva, total_ht in list(self.deposit_amounts().items()):
                line = TaskLine(cost=total_ht, tva=tva)
                total_ttc += line.total()
            return self.task.floor(total_ttc)
        return 0

    def paymentline_amount_ttc(self):
        """
            Return the ttc amount of payment (in equal repartition)
        """
        from endi.models.task import TaskLine
        total_ttc = 0
        for tva, total_ht in list(self.paymentline_amounts().items()):
            line = TaskLine(cost=total_ht, tva=tva)
            total_ttc += self.task.floor(line.total())
        return total_ttc

    def sold(self):
        """
            Compute the sold amount to finish on an exact value
            if we divide 10 in 3, we'd like to have something like :
                3.33 3.33 3.34
            (for estimation display)
        """
        from endi.models.task import TaskLine
        result = 0
        total_ttc = self.task.total()
        deposit_ttc = self.deposit_amount_ttc()
        rest = total_ttc - deposit_ttc

        payment_lines_num = self.get_nb_payment_lines()
        if payment_lines_num == 1 or not self.get_nb_payment_lines():
            # No other payment line
            result = rest
        else:
            if self.task.manualDeliverables == 0:
                line_ttc = self.paymentline_amount_ttc()
                result = rest - ((payment_lines_num - 1) * line_ttc)
            else:
                sold_lines = self.manual_payment_line_amounts()[-1]
                result = 0
                for tva, total_ht in list(sold_lines.items()):
                    line = TaskLine(tva=tva, cost=total_ht)
                    result += line.total()

        return result


class GroupCompute(GroupComputeMixin):
    task_line_group = None

    def __init__(self, task_line_group):
        GroupComputeMixin.__init__(self, task_line_group)

    def total_ttc(self):
        """
        Returns the TTC total for this group
        """
        return self.total_ht() + self.tva_amount()


class LineCompute(LineComputeMixin):
    """
        Computing tool for line objects
    """
    task_line = None

    def __init__(self, task_line):
        LineComputeMixin.__init__(self, task_line)

    def unit_ht(self):
        """
        Unit Ht value

        :rtype: float
        """
        return self.task_line.cost

    def total_ht(self):
        """
            Compute the line's total
        """
        cost = self.task_line.cost or 0
        quantity = self._get_quantity()
        return cost * quantity

    def tva_amount(self):
        """
            compute the tva amount of a line
        """
        total_ht = self.total_ht()
        return math_utils.compute_tva(total_ht, self.task_line.tva)

    def total(self):
        """
            Compute the ttc amount of the line
        """
        return self.tva_amount() + self.total_ht()


class DiscountLineCompute(DiscountLineMixin):
    """
        Computing tool for discount_line objects
    """
    discount_line = None

    def __init__(self, discount_line):
        DiscountLineMixin.__init__(self, discount_line)

    def total_ht(self):
        return float(self.discount_line.amount)

    def tva_amount(self):
        """
            compute the tva amount of a line
        """
        total_ht = self.total_ht()
        return math_utils.compute_tva(total_ht, self.discount_line.tva)

    def total(self):
        return self.tva_amount() + self.total_ht()
