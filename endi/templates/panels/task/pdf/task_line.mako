<%doc>
Renders a TaskLine object
</%doc>

<%namespace file="/base/utils.mako" import="format_text" />

<tr>
    <td class="col_text description">${format_text(line.description, False)}</td>
    % if display_units == 1:
        <%doc>
        We display the unit ht value if :
        - we're in ht mode
        - we're in ttc mode with display_ttc set to False
        </%doc>
        % if is_tva_on_margin_mode:
            <td class="col_number price">${task.format_amount(line.cost*1.2, trim=False, precision=5)}&nbsp;€</td>
        % elif task.mode == 'ttc' and task.display_ttc:
            <td class="col_number price">${task.format_amount(line.cost, trim=False, precision=5)}&nbsp;€</td>
        % else:
            <td class="col_number price">${task.format_amount(line.unit_ht(), trim=False, precision=5)}&nbsp;€</td>
        % endif
        <td class="col_number quantity">${api.format_quantity(line.quantity)}</td>
        <td class="col_text unity">${line.unity}</td>
    % elif show_progress_invoicing:
        <td class="col_number quantity">${progress_invoicing_percentage}&nbsp;%</td>
    % endif
    <td class="col_number price_total">
        % if is_tva_on_margin_mode:
            ${task.format_amount(line.total(), trim=False, precision=5)}&nbsp;€
        % else:
            ${task.format_amount(line.total_ht(), trim=False, precision=5)}&nbsp;€
        % endif
    </td>
    % if display_tvas_column and not is_tva_on_margin_mode:
        <td class='col_number tva'>
            % if line.tva>=0:
                ${task.format_amount(line.tva, precision=2)}&nbsp;%
            % else:
                0 %
            % endif
        </td>
    % endif
    % if display_ttc:
        <td class="col_number price">${task.format_amount(line.total(), trim=False, precision=5)}&nbsp;€</td>
    % endif
</tr>
