<%inherit file="/tasks/view_only.mako" />
<%namespace name="utils" file="/base/utils.mako" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
	<% estimation = request.context %>
	<div role="group">
		% if api.has_permission('geninv.estimation'):
			<% geninv_url =  request.route_path('/estimations/{id}/geninv', id=estimation.id) %>
			% if len(estimation.invoices) or estimation.geninv:
				<%utils:post_action_btn url="${geninv_url}" icon="copy" _class="btn" title="Transformer à nouveau ce devis en facture">
					Re-facturer
				</%utils:post_action_btn>
			% else:
				<%utils:post_action_btn url="${geninv_url}" icon="file-invoice-euro" _class="btn" title="Transformer ce devis en facture">
					Facturer
				</%utils:post_action_btn>
			% endif
		% elif api.has_permission('genbusiness.estimation'):
			% if estimation.business_id:
				<a class='btn' title="Voir l’affaire : ${estimation.business.name}" aria-label="Voir l’affaire : ${estimation.business.name}" href="${request.route_path('/businesses/{id}', id=estimation.business_id)}">
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder"></use></svg>
					Voir l’affaire
				</a>
			% else:
				<% genbusiness_url =  request.route_path('/estimations/{id}/genbusiness', id=estimation.id)%>
				<%utils:post_action_btn url="${genbusiness_url}" icon="folder" _class="btn" title="Générer une affaire (${estimation.business_type.label}) au sein de laquelle facturer">
					Générer une affaire
				</%utils:post_action_btn>
			% endif
		% endif
		% if not estimation.invoices:
			<a class='btn' title="Rattacher ce devis à des factures" href="${request.route_path('/estimations/{id}/attach_invoices', id=estimation.id)}">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#link"></use></svg>
				Rattacher<span class="no_mobile"> à des factures</span>
			</a>
		% endif
        % if estimation.price_study_id is not None:
            <a class='btn' title="Voir l’étude de prix à l’origine de ce devis" href="${request.route_path('/price_studies/{id}', id=estimation.price_study_id)}">
             <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#calculator"></use></svg> Voir l’étude de prix
            </a>
        % endif
		% if api.has_permission('draft.estimation'):
			<% set_draft_url = request.route_path('/estimations/{id}/set_draft', id=estimation.id) %>
			<%utils:post_action_btn url="${set_draft_url}" icon="pen-square" _class="btn icon" title="Repasser ce devis en brouillon">
				Brouillon
			</%utils:post_action_btn>
		% endif
	</div>
	<div role="group">
		% if api.has_permission('duplicate.estimation'):
			<a class='btn icon only' title="Dupliquer ce devis" aria-label="Dupliquer ce devis" href="${request.route_path('/estimations/{id}/duplicate', id=estimation.id)}">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#copy"></use></svg>
			</a>
		% endif
			<a class='btn icon only' title="Modifier ce devis" aria-label="Modifier ce devis" href="${request.route_path('/estimations/{id}/set_metadatas', id=estimation.id)}">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
			</a>
		% if hasattr(next, 'before_actions'):
			${next.before_actions()}
		% endif
		<% href_pdf = request.route_path('/%ss/{id}.pdf' % request.context.type_, id=request.context.id) %>
		<a class='btn btn-primary icon only' title="Voir le PDF de ce devis" aria-label="Voir le PDF de ce devis" href="${href_pdf}">
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>
		</a>
		% if hasattr(next, 'moreactions'):
			${next.moreactions()}
		% endif
	</div>
</div>
</%block>

<%block name='panel_heading'>
${request.context.name}
</%block>

<%block name='before_summary'>
<% estimation = request.context %>
% if api.has_permission('set_signed_status.estimation'):
	<div class="separate_bottom content_vertical_padding">
		<% signed_status_url = request.route_path('/api/v1/estimations/{id}', id=estimation.id, _query={'action': 'signed_status'}) %>
		<div class="icon_choice layout flex signed_status_group" data-toggle="buttons" data-url="${signed_status_url}">
			% for action  in actions:
				<label
					class="${action.options['css']} ${'active' if estimation.signed_status == action.name else ''}"
					title="${action.options['label']}"
					aria-label="${action.options['label']}"
					>
					<input
						type="radio"
						title="${action.options['title']}"
						name="${action.status_attr}"
						value="${action.name}"
						autocomplete="off"
						class="visuallyhidden"
						% if estimation.signed_status == action.name:
							checked="checked"
						% endif
						>
						<span>
							<svg><use href="/static/icons/endi.svg#${action.options['icon']}"></use></svg>
							<span>${action.options['label']}</span>
						</span>
				</label>
			% endfor
		</div>
	</div>
% endif
% if estimation.invoices:
	<div class="separate_bottom content_vertical_padding">
		<h3>
			Factures
			<% attach_invoices_url = request.route_path('/estimations/{id}/attach_invoices', id=estimation.id) %>
			<a class="btn icon only unstyled" title="Rattacher une facture" aria-label="Rattacher une facture" href="${attach_invoices_url}">
				<svg><use href="/static/icons/endi.svg#link"></use></svg>
			</a>
		</h3>
		<ul>
			% for invoice in estimation.invoices:
				<li>
					La facture (${api.format_invoice_status(invoice, full=False)})&nbsp;: \
					<a href="${request.route_path('/invoices/{id}.html', id=invoice.id)}">
						${invoice.internal_number}
						% if invoice.official_number:
							(${invoice.official_number})
						% endif
					</a>
					a été générée depuis ce devis.
				</li>
			% endfor
		</ul>
	</div>
% endif
</%block>
