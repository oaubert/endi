import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import InputWidget from '../../widgets/InputWidget.js';
import PercentInputWidget from '../../widgets/PercentInputWidget.js';
import Radio from 'backbone.radio';


const SupplierOrderFormView = Mn.View.extend({
    tagName: 'div',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierOrderFormView.mustache'),
	regions: {
		name: '.name',
        advance_percent: '.advance_percent',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'order:modified',
    },
    childViewEvents: {
        'change': 'onChange',
    },

    initialize(){
        var channel = Radio.channel('config');
        channel.trigger();
    },
    onChange(name, value){
        this.model.set(name, value);
    },
    onRender(){
        var view;
        let editable = this.getOption('edit');
        view = new PercentInputWidget({
            value: this.model.get('cae_percentage'),
            title: 'Part de règlement direct par la CAE',
            field_name: 'cae_percentage',
            editable: editable,
        });
        this.showChildView('advance_percent', view);

        view = new InputWidget({
            value: this.model.get('name'),
            title: 'Nom',
            field_name: 'name',
            editable: editable,
        });
        this.showChildView('name', view);
    },

    onSuccessSync(){
        let facade = Radio.channel('facade');
        facade.trigger('navigate', 'index');
    },

});
export default SupplierOrderFormView;
