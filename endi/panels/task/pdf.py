"""
Weasyprint pdf task panel
"""
from endi.models.task import Invoice
from endi.models.task import Estimation
from endi.panels.task.task import CompanySerializer


def pdf_header_panel(context, request):
    """
    Panel for task pdf file header
    Only shown once in the rendering (not on all pages)
    """
    result = {
        'company': context.company,
        'has_header': False,
        'config': request.config,
        'task': context
    }
    if context.company.header_file:
        result['has_header'] = True
    return result


def pdf_footer_panel(context, request, **kwargs):
    """
    Panel for task pdf file footer

    Show on all pages
    """
    config = request.config
    result = {
        'title': config.get('coop_pdffootertitle'),
        'text': config.get('coop_pdffootertext'),
        'pdf_current_page': '',
        'pdf_page_count': '',
    }
    if context.is_training():
        result['more_text'] = config.get('coop_pdffootercourse')

    if isinstance(context, Estimation):
        number_label = "Devis {}".format(context.internal_number)
    elif context.status == 'valid':
        number_label = "Facture {}".format(context.official_number)
    else:
        number_label = "Document non numéroté"

    result['number'] = number_label

    return {**result, **kwargs}


def pdf_content_panel(context, request, with_cgv=True):
    """
    Panel generating the main html page for a task pdf's output
    """
    tvas = context.get_tvas()
    # Si on a plusieurs TVA dans le document, cela affecte l'affichage
    multiple_tvas = len([val for val in tvas if val]) > 1

    is_tva_on_margin_mode = context.business_type.tva_on_margin

    # Contexte de templating qui sera utilisé pour les mentions et autres
    # textes configurables avec une notion de templating
    tmpl_context = CompanySerializer.compile_obj(context.company)

    # Calcul des nombres de
    if context.display_units:
        column_count = 5
        first_column_colspan = 4
    else:
        column_count = 2
        first_column_colspan = 1

    if multiple_tvas:
        column_count += 1

    if context.display_ttc:
        column_count += 1

    if isinstance(context, Invoice) and \
            context.invoicing_mode == context.PROGRESS_MODE:
        show_progress_invoicing = True
        column_count += 1
        first_column_colspan += 1
    else:
        show_progress_invoicing = False

    return dict(
        task=context,
        groups=context.get_groups(),
        project=context.project,
        company=context.project.company,
        multiple_tvas=multiple_tvas,
        tvas=tvas,
        config=request.config,
        mention_tmpl_context=tmpl_context,
        first_column_colspan=first_column_colspan,
        column_count=column_count,
        show_progress_invoicing=show_progress_invoicing,
        with_cgv=with_cgv,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def pdf_task_line_group_panel(
    context,
    request,
    group,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
    is_tva_on_margin_mode,
):
    """
    A panel representing a TaskLineGroup
    """
    display_subtotal = False
    if len(context.get_groups()) > 1:
        display_subtotal = True

    return dict(
        task=context,
        group=group,
        display_subtotal=display_subtotal,
        display_units=context.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=context.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def pdf_task_line_panel(
    context,
    request,
    line,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
    is_tva_on_margin_mode,
):
    """
    A panel representing a single TaskLine

    :param obj context: The current task to be rendered
    :param obj line: A taskline
    """
    percentage = 0
    if show_progress_invoicing:
        from endi.models.progress_invoicing import ProgressInvoicingLine
        percentage = ProgressInvoicingLine.find_percentage(line.id)
        if percentage is None:
            percentage = 0

    return dict(
        task=context,
        line=line,
        display_units=context.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=context.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        progress_invoicing_percentage=percentage,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def pdf_cgv_panel(context, request):
    """
    Panel used to render cgv
    """
    cae_cgv = request.config.get('coop_cgv')
    company_cgv = context.company.cgv
    return dict(
        cae_cgv=cae_cgv,
        company_cgv=company_cgv
    )


def pdf_content_wrapper_panel(context, request):
    """
    Used to wrap the content inside an html page structure
    """
    return dict(task=context)


def includeme(config):
    config.add_panel(
        pdf_header_panel,
        'task_pdf_header',
        renderer="panels/task/pdf/header.mako"
    )
    config.add_panel(
        pdf_footer_panel,
        'task_pdf_footer',
        renderer="panels/task/pdf/footer.mako"
    )
    for document_type in ('estimation', 'invoice', 'cancelinvoice'):
        panel_name = "task_pdf_{0}_content".format(document_type)
        template = "panels/task/pdf/{0}_content.mako".format(document_type)
        config.add_panel(
            pdf_content_panel,
            panel_name,
            renderer=template
        )

    config.add_panel(
        pdf_content_wrapper_panel,
        'task_pdf_content',
        renderer="panels/task/pdf/content_wrapper.mako"
    )
    config.add_panel(
        pdf_task_line_group_panel,
        'task_pdf_task_line_group',
        renderer="panels/task/pdf/task_line_group.mako",
    )
    config.add_panel(
        pdf_task_line_panel,
        'task_pdf_task_line',
        renderer="panels/task/pdf/task_line.mako",
    )
    config.add_panel(
        pdf_cgv_panel,
        'task_pdf_cgv',
        renderer="panels/task/pdf/cgv.mako",
    )
