import Mn from 'backbone.marionette';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Select2Widget from '../../widgets/Select2Widget.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import Radio from 'backbone.radio';


const ExpenseKmFormView = Mn.View.extend({
    behaviors: [ModalFormBehavior],
    template: require('./templates/ExpenseKmFormView.mustache'),
    id: "expensekm-form-popup-modal",
    regions: {
        'category': '.category',
        'date': '.date',
        'type_id': '.type_id',
        'start': '.start',
        'end': '.end',
        'km': '.km',
        'description': '.description',
        'business_link': '.business_link',
    },
    ui: {
        modalbody: "main",
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:modified',
    },
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        var channel = Radio.channel('config');
        this.type_options = channel.request(
            'get:options',
            'expensekm_types',
        );
        this.categories = channel.request(
            'get:options',
            'categories',
        );
        this.today = channel.request(
            'get:options',
            'today',
        );
        this.businesses = channel.request(
            'get:options',
            'businesses',
        );
    },
    refreshForm(){
        var view = new SelectWidget({
            value: this.model.get('category'),
            field_name: 'category',
            options: this.categories,
            id_key: 'value',
            title: "Catégorie",
        });
        this.showChildView('category', view);

        view = new DatePickerWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            current_year: true,
            default_value: this.today,
            required: true,
        });
        this.showChildView("date", view);

        view = new Select2Widget({
            value: this.model.get('type_id'),
            title: 'Type de frais',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
            required: true,
        });
        this.showChildView('type_id', view);

        view = new InputWidget({
            value: this.model.get('start'),
            title: 'Point de départ',
            field_name: 'start',
            required: true,
        });
        this.showChildView('start', view);

        view = new InputWidget({
            value: this.model.get('end'),
            title: "Point d'arrivée",
            field_name: 'end',
            required: true,
        });
        this.showChildView('end', view);

        view = new InputWidget({
            value: this.model.get('km'),
            title: "Nombre de kilomètres",
            field_name: 'km',
            addon: "km",
            required: true,
        });
        this.showChildView('km', view);

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Prestation',
            field_name: 'description'
        });
        this.showChildView('description', view);

        if(this.model.get('category')!=1) {
            view = new SelectBusinessWidget({
                title: 'Affaire concernée',
                options: this.businesses,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
            });
            this.showChildView('business_link', view);
        }
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
        };
    },
    onRender(){
        this.refreshForm();
    },
    onChildChange(attribute, value) {
        if(attribute=="category"){
            if(value==1) {
                this.getRegion('business_link').empty();
            }else{
                var view = new SelectBusinessWidget({
                    title: 'Affaire concernée',
                    options: this.businesses,
                    customer_value: this.model.get('customer_id'),
                    project_value: this.model.get('project_id'),
                    business_value: this.model.get('business_id'),
                });
                this.showChildView('business_link', view);
            }
        }
    },
    onSuccessSync(){
        if (this.add){
            var this_ = this;
            var modalbody = this.getUI('modalbody');

            modalbody.effect(
                'highlight',
                {color: 'rgba(0,0,0,0)'},
                800,
                this_.refresh.bind(this)
            );
            modalbody.addClass('action_feedback success');
        } else {
            this.triggerMethod('modal:close');
        }
    },
    onModalBeforeClose(){
        this.model.rollback();
    },
});
export default ExpenseKmFormView;
