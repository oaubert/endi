<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_filelist" />
<%block name="headtitle">
    ${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>
<%block name='actionmenucontent'>
    <div id="js_actions"></div>
</%block>
<%block name="beforecontent">
    <% supplier_invoice = request.context %>
    <% multi_order = len(supplier_invoice.supplier_orders) > 1 %>
    <div>
	% if len(supplier_invoice.validation_status_history) > 0 :
	    <div class='layout flex two_cols hidden-print'>
    % else :
    	<div class='layout flex hidden-print'>
    % endif
            <div>
                <h3>
                    ${request.layout_manager.render_panel('status_title', context=request.context)}
                </h3>
                <ul class="content_vertical_padding">
                    <li class="hidden-print">
                        ${api.format_supplier_invoice_status(supplier_invoice)}
                    </li>

                    % if request.has_permission('admin_treasury'):
                        % if supplier_invoice.status == 'valid':
                            <li>
                                Numéro de pièce :
                                <strong>${supplier_invoice.id}</strong>
                            </li>
                        % endif
                        <li>
                            % if supplier_invoice.exported:
                                Ce document a déjà été exporté vers le
                                logiciel de comptabilité
                            % else:
                                Ce document n'a pas encore été exporté vers le logiciel de comptabilité
                            % endif
                        </li>
                    % endif
                </ul>
            </div>
            % if len(supplier_invoice.validation_status_history) > 0 :
            <div class="hidden-print">
                <h4 class="history">Historique des Communications Entrepreneurs-CAE</h4>
                % for entry in supplier_invoice.validation_status_history:
                    % if entry.comment.strip():
                        <blockquote>
                            <p>
                                <span class="icon">${api.icon(api.status_icon(entry))}</span>
                                ${format_text(entry.comment)}
                            </p>
                            <footer>
                                ${api.format_account(entry.user)} le ${api.format_date(entry.datetime)}
                            </footer>
                        </blockquote>
                    % endif
                % endfor
            </div>
            % endif
        </div>
		<div class="separate_top content_vertical_padding">
			<h3>
			% if not supplier_invoice.children:
				Aucun justificatif n’a été déposé
			% else :
				Justificatifs
			% endif
			</h3>
			<div class="content_vertical_padding">
				${format_filelist(supplier_invoice)}
			</div>
			<a
				href="${request.route_path('/suppliers_invoices/{id}/addfile', id=supplier_invoice.id)}"
				class="btn btn-primary secondary-action">
				${api.icon('paperclip')}
				Ajouter un fichier
			</a>
		</div>
    </div>
</%block>
<%block name='content'>
    <div id="js-main-area"></div>
</%block>
<%block name='footerjs'>
    var AppOption = {};
    AppOption['context_url'] = "${context_url}";
    AppOption['form_config_url'] = "${form_config_url}"
    % if request.has_permission("edit.supplier_invoice"):
        AppOption['edit'] = true;
    % else:
        AppOption['edit'] = false;
    % endif
</%block>
