import functools

import colander
from colanderalchemy import SQLAlchemySchemaNode
import deform
import deform_extensions

from endi_base.models.base import DBSESSION
from endi.compute.math_utils import integer_to_amount
from endi import forms
from endi.forms.company import company_choice_node
from endi.forms.custom_types import AmountType
from endi.forms.expense import expense_type_choice_node
from endi.forms.files import FileNode
from endi.forms.payments import (
    deferred_amount_default,
    deferred_payment_mode_widget,
    deferred_payment_mode_validator,
    deferred_bank_account_widget,
    deferred_bank_account_validator,
)
from endi.forms.third_party.supplier import (
    globalizable_supplier_choice_node_factory
)
from endi.forms.supply import (
    get_add_edit_line_schema,
    get_list_schema,
)
from endi.forms.supply.supplier_order import (
    get_deferred_supplier_order_select_validator,
    supplier_order_choice_node,
)
from endi.models.supply import (
    get_supplier_invoices_years,
    SupplierOrder,
    SupplierInvoice,
    SupplierInvoiceLine,
)


def _customize_edit_schema(schema):
    customize = functools.partial(forms.customize_field, schema)

    if 'supplier_orders' in schema:
        # On s'assure qu'on sélectionne une liste de type parmis des existants
        # (et qu'on en rajoute pas à la volée)
        customize(
            'supplier_orders',
            children=forms.get_sequence_child_item(
                SupplierOrder,
                child_attrs=('id', 'name'),
            ),
            validator=forms.DeferredAll(
                same_percentage_supplier_order_validator,
                same_supplier_supplier_order_validator,
                get_deferred_supplier_order_select_validator(
                    multiple=True,
                    required=False,
                ),
            ),
        )
    return schema


def get_supplier_invoice_edit_schema():
    schema = SQLAlchemySchemaNode(SupplierInvoice)
    schema = _customize_edit_schema(schema)
    return schema


def get_supplier_invoice_list_schema(is_global=False):
    schema = get_list_schema(
        years_func=get_supplier_invoices_years,
        is_global=is_global,
    )
    return schema


def _invoice_amount_from_request(request):
    if type(request.context) is SupplierInvoice:
        supplier_invoice = request.context
    else:
        supplier_invoice = request.context.supplier_invoice
    return supplier_invoice.topay()


@colander.deferred
def deferred_payment_amount_validator(node, kw):
    """
    Validate the amount to keep the sum under the of what CAE sohuld pay.
    """
    topay = _invoice_amount_from_request(kw['request'])
    max_msg = "Le montant payé par la CAE ne doit pas dépasser {}".format(
        integer_to_amount(topay)
    )
    min_msg = "Le montant doit être positif"

    return colander.Range(
        min=0,
        max=topay,
        min_err=min_msg,
        max_err=max_msg,
    )


@colander.deferred
def deferred_payment_amount_default(node, kw):
    return _invoice_amount_from_request(kw['request'])


@colander.Function
def same_percentage_supplier_order_validator(value):
    """
    Validate that all targeted SupplierOrder do use the same per-centage
    """
    suppliers_orders_ids = value
    query = DBSESSION().query(SupplierOrder.cae_percentage)
    query = query.filter(
        SupplierOrder.id.in_(suppliers_orders_ids),
    ).distinct()
    if query.count() > 1:
        return (
            "Toutes les commandes sélectionnées doivent avoir le même "
            + "pourcentage de paiement CAE."
        )
    else:
        return True


@colander.Function
def same_supplier_supplier_order_validator(value):
    """
    Validate that all targeted SupplierOrder are from same supplier
    """
    suppliers_orders_ids = value
    query = DBSESSION().query(SupplierOrder.supplier_id)
    query = query.filter(
        SupplierOrder.id.in_(suppliers_orders_ids),
    ).distinct()
    if query.count() > 1:
        return (
            "Toutes les commandes sélectionnées doivent avoir le même "
            + "fournisseur."
        )
    else:
        return True



def get_invoicable_supplier_orders(request):
    company = request.context
    return SupplierOrder.query_for_select(
        valid_only=True,
        invoiced=False,
        company_id=company.id
    )


class SupplierInvoiceAddSchema(colander.MappingSchema):
    suppliers_orders_ids = supplier_order_choice_node(
        title='Commande(s) fournisseur',
        multiple=True,
        description=(
            "Vous pouvez associer votre facture à une ou "
            + "plusieurs commandes préalablement validées. "
            + "Les lignes des commandes seront importées dans la facture."
        ),
        query_func=get_invoicable_supplier_orders,
        extra_validator=colander.All(
            same_percentage_supplier_order_validator,
            same_supplier_supplier_order_validator,
        ),
    )


class SupplierPaymentSchema(colander.MappingSchema):
    """
    Add form for SupplierOrderPayment
    """
    come_from = forms.come_from_node()
    date = forms.today_node()
    amount = colander.SchemaNode(
        AmountType(),
        title="Montant du paiement",
        validator=deferred_payment_amount_validator,
        default=deferred_amount_default,
    )
    mode = colander.SchemaNode(
        colander.String(),
        title="Mode de paiement",
        widget=deferred_payment_mode_widget,
        validator=deferred_payment_mode_validator,
    )
    bank_id = colander.SchemaNode(
        colander.Integer(),
        title="Banque",
        widget=deferred_bank_account_widget,
        validator=deferred_bank_account_validator,
    )
    resulted = colander.SchemaNode(
        colander.Boolean(),
        title="Soldé",
        label="",
        description=(
            "Indique que le document est soldé (ne recevra plus de paiement), "
            + "si le montant indiqué correspond au montant de la facture "
            + "fournisseur, celui-ci est soldée automatiquement."
        ),
        missing=False,
        default=False,
    )
    bank_remittance_id = colander.SchemaNode(
        colander.String(),
        title="Référence du paiement",
        description=(
            "Ce champ est un indicateur permettant de retrouver l'opération "
            + "bancaire à laquelle ce décaissement est associé, par exemple "
            + "pour la communiquer à un fournisseur"
        ),
        missing='',
    )


INVOICE_LINE_GRID = (
    (
        ('company_id', 3),
        ('type_id', 4),
        ('description', 3),
        ('ht', 1),
        ('tva', 1),
    ),
)


def get_supplier_invoice_line_dispatch_schema():
    schema = get_add_edit_line_schema(
        SupplierInvoiceLine,
        widget=deform_extensions.GridMappingWidget(
            named_grid=INVOICE_LINE_GRID
        ),
        title='ligne',
        excludes=['type_id'],
    )
    forms.customize_field(
        schema,
        'tva',
        title='TVA',
        validator=colander.Range(min=0),
    )
    forms.customize_field(
        schema,
        'ht',
        title='HT',
        validator=colander.Range(min=0),
    )
    schema.add(
        company_choice_node(
            name="company_id",
            title="Enseigne",
        )
    )
    schema.add(
        expense_type_choice_node(
            name="type_id",
            title="Type",
        )
    )

    return schema


class InvoiceLineSequenceSchema(colander.SequenceSchema):
    lines = get_supplier_invoice_line_dispatch_schema()


def _get_linkable_lines(node, kw):
    business = kw['request'].context
    assert business.__name__ == 'business'
    return SupplierInvoiceLine.linkable(business)


def _get_deferred_supplier_invoice_line_choices(widget_options):
    default_option = widget_options.pop('default_option', None)

    @colander.deferred
    def deferred_supplier_invoice_line_choices(node, kw):
        query = _get_linkable_lines(node, kw)
        # most recent first
        query = query.order_by(
            SupplierInvoice.date.desc(),
            SupplierInvoice.id.desc(),
        )
        values = [(v.id, v.long_label()) for v in query]
        if default_option:
            values.insert(0, default_option)
        return deform.widget.Select2Widget(
            values=values,
            **widget_options
        )
    return deferred_supplier_invoice_line_choices


def supplier_invoice_line_node(multiple=False, **kw):
    widget_options = kw.pop('widget_options', {})
    widget_options.setdefault('default_option', ('', ''))
    return colander.SchemaNode(
        colander.Set() if multiple else colander.Integer(),
        widget=_get_deferred_supplier_invoice_line_choices(widget_options),
        validator=forms.deferred_id_validator(
            _get_linkable_lines,
        ),
        **kw
    )


supplier_invoice_line_choice_node = forms.mk_choice_node_factory(
    supplier_invoice_line_node,
    resource_name='une ligne de facture fournisseur',
)


class SupplierInvoiceLineSeq(colander.SequenceSchema):
    line = supplier_invoice_line_choice_node()


class SupplierInvoiceDispatchSchema(colander.MappingSchema):
    date = forms.today_node()
    invoice_file = FileNode(title="Document")
    supplier_id = globalizable_supplier_choice_node_factory(
        description=(
            "Seuls les fournisseurs présents dans au moins une enseigne "
            + "et avec un n° d'immatriculation renseigné sont proposés. Si "
            + "un fournisseur est manquant, il faudra commencer par le "
            + "saisir dans les fournisseurs d'une enseigne."
        )
    )
    total_ht = colander.SchemaNode(
        AmountType(),
        title="Total HT",
        validator=colander.Range(min=0),
    )

    total_tva = colander.SchemaNode(
        AmountType(),
        title="Total TVA",
        validator=colander.Range(min=0),
    )

    lines = InvoiceLineSequenceSchema(
        title='Lignes de facture',
        widget=deform.widget.SequenceWidget(min_len=1),
    )

    name = colander.SchemaNode(
        colander.String(),
        missing=colander.drop,
        title='Titre de la facture',
        description=(
        'Laisser vide pour « Commande {FOURNISSEUR} du {DATE FACTURE} »'
        ),
    )

    def _validate_sum(self, line_fieldname, total_fieldname, values):
        """
        :rtype list:
        :return: the SchemaNodes with an error
        """
        total = values.get(total_fieldname)
        lines_sum = sum(
            line[line_fieldname]
            for line in values['lines']
        )
        return total == lines_sum

    def validator(self, form, values):
        # the error is not very detailed as the user should already have been
        # noticed front-end side (JS).
        valid = (
            self._validate_sum('ht', 'total_ht', values)
            and
            self._validate_sum('tva', 'total_tva', values)
        )
        if not valid:
            raise colander.Invalid(form, msg='Totaux incohérents')
