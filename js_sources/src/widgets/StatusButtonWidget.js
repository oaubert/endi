import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

/** 
 * A button sending status modifications to the facade radio channel
 */
const StatusButtonWidget = Mn.View.extend({
    tagName: 'div',
    template: require('./templates/StatusButtonWidget.mustache'),
    ui: {
        button: 'button'
    },
    events: {
        'click @ui.button': 'onClick'
    },
    onClick: function(event){
        let status = this.model.get('status');
        let title = this.model.get('title');
        let label = this.model.get('label');
        let url = this.model.get('url');
        this.triggerMethod('status:change', status, title, label, url);
        var facade = Radio.channel('facade');
        facade.trigger('status:change', status, title, label, url);
    }
});
export default StatusButtonWidget;
