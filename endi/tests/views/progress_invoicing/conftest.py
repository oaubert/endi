import pytest


@pytest.fixture
def business(dbsession, mk_business, full_estimation, default_business_type):
    business = mk_business(invoicing_mode='progress')
    business.estimations = [full_estimation]
    dbsession.merge(business)
    full_estimation.status = 'valid'
    full_estimation.business_type_id = default_business_type.id
    full_estimation.businesses = [business]
    dbsession.merge(full_estimation)
    dbsession.flush()
    business.set_progress_invoicing_mode()
    dbsession.merge(business)
    dbsession.flush()
    return business


@pytest.fixture
def invoice(
    dbsession, business, user
):
    invoice = business.add_invoice(user)

    # On construit la structure de données attendues pour la génération des
    # lignes de prestation
    appstruct = {}
    for status in business.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for line_status in status.line_statuses:
            appstruct[status.id][line_status.id] = 10
    # On populate notre facture
    business.populate_progress_invoicing_lines(
        invoice,
        appstruct,
    )
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice
