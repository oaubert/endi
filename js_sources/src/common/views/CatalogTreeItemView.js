/*
 * Module name : CatalogTreeItemView
 */
import Mn from 'backbone.marionette';
import RadioWidget from '../../widgets/RadioWidget.js';
import CheckboxWidget from '../../widgets/CheckboxWidget.js';
import { getOpt } from "../../tools.js";

const template = require('./templates/CatalogTreeItemView.mustache');

const CatalogTreeItemView = Mn.View.extend({
    tagName: 'tr',
    template: template,
    regions: {td_action: 'td.action'},
    modelEvents: {
        'change:selected': 'render',
    },
    // Listen to child view events
    childViewEvents: {
        'finish': "onChange"
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        if (getOpt(this, 'selected', false)){
            this.model.set('selected');
        }
        this.compute_mode = getOpt(this, "compute_mode", "ht");
        this.multiple = getOpt(this, 'multiple', false);
    },
    onRender(){  
        let widget;
        let options = {
            field_name: 'check',
            ariaLabel: 'Sélectionner ce produit',
        };
        if (this.multiple){
            options = Object.assign(
                options, 
                {
                    value: this.model.get('selected'),
                    true_val: true,
                    false_val: false,
                }
            );
            widget = new CheckboxWidget(options);
        } else {
            options['value'] = true;
            widget = new RadioWidget(options);
        }
        this.showChildView('td_action', widget);
    },
    onChange(field_name, value){
        this.model.collection.setSelected(this.model, value);
    },
    templateContext(){
        let amount_label;
        if (this.compute_mode == 'ht'){
            amount_label = this.model.ht_label();
        } else {
            amount_label = this.model.ttc_label();
        }
        return {
            amount_label: amount_label,
        };
    }
});
export default CatalogTreeItemView
