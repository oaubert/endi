<%inherit file="${context['main_template'].uri}" />

<%block name='actionmenucontent'>
<div class="layout flex main_actions">
    <div role='group'>
        <a class='btn btn-primary'  href='#entries/add' title='Ajouter une nouvelle entrée statistique'>
            <svg><use href="/static/icons/endi.svg#plus"></use></svg> Ajouter<span class="no_mobile"> une entrée</span>
        </a>
    </div>
    <div role='group'>
        <% csv_url = request.route_path('statistic', id=request.context.id, _query=dict(format="csv")) %>
        <a class='btn' href='${csv_url}' title='Générer la sortie CSV pour cette feuille de statistiques'>
            <svg><use href="/static/icons/endi.svg#file-csv"></use></svg> CSV
        </a>
    </div>
</div>
</%block>

<%block name='afteractionmenu'>
    <div class="alert alert-info">
        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
        Chaque entrée statistique peut être composée d'un ou plusieurs critères et être exportée indépendamment.
    </div>
</%block>

<%block name="content">
<div class='content_vertical_padding limited_width width40'>
	<div id='sheet'></div>
    <div class='content_vertical_padding' id='entrylist'></div>
    <div class='content_vertical_padding' id='entry_edit'></div>
</div>
<div id='messageboxes'></div>
<div id='popup_container'></div>
</%block>

<%block name="footerjs">
AppOptions = {};
AppOptions['loadurl'] = "${loadurl}";
AppOptions['contexturl'] = "${contexturl}";
</%block>
