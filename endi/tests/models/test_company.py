def test_disable(company):
    company.disable()
    assert company.active == False


def test_enable(company):
    company.enable()
    assert company.active == True


def test_company_general_customer_account(company3):
    assert company3.general_customer_account == "00099988"


def test_company_third_party_customer_account(company3):
    assert company3.third_party_customer_account == "00055566"


def test_company_general_supplier_account(company3):
    assert company3.general_supplier_account == "0002332415"


def test_company_third_party_supplier_account(company3):
    assert company3.third_party_supplier_account == "000056565656"