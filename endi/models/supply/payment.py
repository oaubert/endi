from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi_base.models.base import (
    DBBASE,
    default_table_args,
)

from endi_base.models.mixins import (
    PersistentACLMixin,
    TimeStampedMixin,
)
from endi.models.payments import PaymentModelMixin
from sqlalchemy.orm import (
    relationship,
    backref,
)


class SupplierPayment(
        PersistentACLMixin,
        TimeStampedMixin,
        PaymentModelMixin,
        DBBASE,
):
    """
    Payment issued from the CAE, to a Supplier

    The payment is linked to a SupplierInvoice, and covers the CAE percentage
    of it.
    """
    __tablename__ = 'supplier_payment'
    __table_args__ = default_table_args
    id = Column(Integer, primary_key=True)

    supplier_invoice_id = Column(
        Integer,
        ForeignKey('supplier_invoice.id', ondelete='cascade'),
        info={
            'export': {'exclude': True},
        },
        nullable=True,
    )

    supplier_invoice = relationship(
        "SupplierInvoice",
        primaryjoin='SupplierInvoice.id==SupplierPayment.supplier_invoice_id',
        backref='payments',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    bank_id = Column(
        Integer,
        ForeignKey('bank_account.id'),
        info={
            'export': {'exclude': True},
        },
        nullable=True,
    )

    bank = relationship(
        "BankAccount",
        backref=backref(
            'supplier_payments',
            order_by="SupplierPayment.date",
            info={'colanderalchemy': {'exclude': True}},
        ),
    )

    @property
    def parent(self):
        return self.supplier_invoice

    def __json__(self, request):
        return dict(
            id=self.id,
            mode=self.mode,
            amount=integer_to_amount(self.amount),
            date=self.date.isoformat(),
            bank_remittance_id=self.bank_remittance_id,
        )
