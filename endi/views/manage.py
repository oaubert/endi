"""
    Manage view :
        - last documents page
"""
import logging

from endi.interfaces import IValidationStatusHolderService
from endi.resources import dashboard_resources
from endi.models.task.task import Task
from endi.models.expense.sheet import ExpenseSheet
from endi.models.activity import Activity
from endi.models.supply import (
    SupplierInvoice,
    SupplierOrder,
)
from endi.forms.user.user import User

log = logging.getLogger(__name__)


def manage(request):
    """
    The manage view
    """
    dashboard_resources.need()

    # DEVIS
    estimations = Task.get_waiting_estimations().all()
    for item in estimations:
        item.url = request.route_path('/estimations/{id}', id=item.id)

    # FACTURES
    invoices = Task.get_waiting_invoices().all()
    for item in invoices:
        item.url = request.route_path('/%ss/{id}' % item.type_, id=item.id)

    # DEPENSES
    expenses = ExpenseSheet.query()\
        .filter(ExpenseSheet.status == 'wait')\
        .order_by(ExpenseSheet.month)\
        .order_by(ExpenseSheet.status_date).all()
    for expense in expenses:
        expense.url = request.route_path("/expenses/{id}", id=expense.id)

    # COMMANDES/FACTURES FORUNISSEURS
    status_docs_service = request.find_service(
        IValidationStatusHolderService
    )
    supply_docs = list(status_docs_service.waiting(SupplierOrder, SupplierInvoice))
    for doc in supply_docs:
        if isinstance(doc, SupplierOrder):
            doc.url = request.route_path('/suppliers_orders/{id}', id=doc.id)
        elif isinstance(doc, SupplierInvoice):
            doc.url = request.route_path('/suppliers_invoices/{id}', id=doc.id)
        else:
            raise ValueError()
        print((doc.url))

    # RENDEZ-VOUS
    user_id = request.user.id
    query = Activity.query()
    query = query.join(Activity.conseillers)
    query = query.filter(Activity.conseillers.any(User.id == user_id))
    query = query.filter(Activity.status == 'planned')
    query = query.order_by(Activity.datetime).limit(10)
    activities = query.all()
    for activity in activities:
        activity.url = request.route_path("activity", id=activity.id)

    return dict(
        title="Mon tableau de bord",
        invoices=invoices,
        estimations=estimations,
        expenses=expenses,
        activities=activities,
        supply_docs=supply_docs
    )


def includeme(config):
    config.add_route(
        "manage",
        "/manage",
    )
    config.add_view(
        manage,
        route_name="manage",
        renderer="manage.mako",
        permission="manage",
    )
