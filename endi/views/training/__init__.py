
def includeme(config):
    config.include(".routes")
    config.include('.trainer')
    config.include('.lists')
    config.include('.dashboard')
    config.include('.business_bpf')
