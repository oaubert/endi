<%doc>
 Task list panel template
</%doc>
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/utils.mako" import="format_customer" />
<%namespace file="/base/utils.mako" import="format_project" />
<%namespace file="/base/utils.mako" import="table_btn"/>
<div class='dash_elem'>
    <h2>
		<span class='icon'>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg>
		</span>
	    <span>Dernières activités sur vos documents</span>
    </h2>
    <div class='panel-body'>
     	<p style="display: none;">
		   Afficher <select id='number_of_tasks'>
			  % for i in (5, 10, 15, 50):
			  <option value='${i}'
			  % if tasks.items_per_page == i:
				selected=true
			  % endif
			  >
			  ${i}
			  </option>
			  % endfor
			</select>
			éléments à la fois
     	</p>
        <table class='hover_table'>
            <thead>
				<th scope="col" class="col_status" title="Statut">
					<span class="screen-reader-text">Statut</span>
				</th>
                <th scope="col" class="col_text">
                    Statut<span class="screen-reader-text"> suite à la dernière modification</span>
                </th>
                <th scope="col" class="col_text" title="Nom du document">
                    Nom<span class="screen-reader-text"> du document</span>
                </th>
                <th scope="col" class="col_text">
                    Client
                </th>
                <th scope="col" class="col_actions" title="Actions">
                    <span class="screen-reader-text">Actions</span>
                </th>
            </thead>
            <tbody>
                % for task in tasks:
					% if task.type_ == 'estimation':
						<% status_text = api.format_estimation_status(task, full=False) %>
					% elif task.type_ == 'invoice':
						<% status_text = api.format_invoice_status(task, full=False) %>
					% else:
						<% status_text = api.format_cancelinvoice_status(task, full=False) %>
					% endif
                    <tr>
                        % if api.has_permission("edit.%s" % task.type_, task):
                            <% url = request.route_path("/%ss/{id}" % task.type_, id=task.id) %>
                        % else:
                            <% url = request.route_path("/%ss/{id}.html" % task.type_, id=task.id) %>
                        % endif
                        <% onclick = "document.location='{url}'".format(url=url) %>
                        <td onclick="${onclick}" class="col_status"  title="Statut suite à la dernière modification : ${status_text} - Cliquer pour voir le document">
							<span class="icon status ${task.status}">
								<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(task)}"></use></svg>
							</span>
                        </td>
                        <td onclick="${onclick}" class="col_text"  title="Statut suite à la dernière modification : ${status_text} - Cliquer pour voir le document">
	                        ${status_text}
                        </td>
                        <td onclick="${onclick}" class="col_text"  title="Statut suite à la dernière modification : ${status_text} - Cliquer pour voir le document">
                            <a href="${url}">${task.name}</a>
                        </td>
                        <td onclick="${onclick}" class="col_text"  title="Statut suite à la dernière modification : ${status_text} - Cliquer pour voir le document">
                            ${format_customer(task.customer, False)}
                        </td>
                        <td class="col_actions width_one">
                            ${table_btn(
                                request.route_path("/%ss/{id}.pdf" % task.type_, id=task.id),
                                "",
                                "Télécharger ce document au format pdf",
                                icon="file-pdf")}
                        </td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>
</div>
