import BaseToolbarAppClass from "../../common/components/ToolbarAppClass";
import Radio from 'backbone.radio';
import ResumeView from '../views/ResumeView';

const ToolbarAppClass = BaseToolbarAppClass.extend({
    getResumeView(actions){
        const facade = Radio.channel('facade');
        const model = facade.request('get:totalmodel');
        return new ResumeView({model: model});
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;
