"""
Business templates related views
"""
import logging
from pyramid.httpexceptions import HTTPFound
from genshi.template.eval import UndefinedError
from py3o.template import Template
from io import BytesIO

from endi.export.utils import write_file_to_request
from endi.models.files import FileType
from endi.models.project.file_types import BusinessTypeFileTypeTemplate
from endi.views.project.business import ProjectBusinessListView
from endi.views.project.project import remember_navigation_history
from endi.views.business.routes import BUSINESS_ITEM_PY3O_ROUTE
from endi.views import (
    BaseView,
    TreeMixin,
)

logger = logging.getLogger(__name__)


def get_key_from_genshi_error(err):
    """
    Genshi raises an UndefinedError, but doesn't store the key name in the
    Exception object
    We get the missing key from the resulting message
    """
    msg = err.msg
    if " not defined" in msg:
        return msg.split(" not defined")[0]
    else:
        return msg


class BusinessFileGeneration(BaseView, TreeMixin):
    help_message = """
    Vous pouvez générer et télécharger des documents modèles définis
    par la coopérative qui seront pré-remplis avec vos coordonnées et
    celles du client."""

    @property
    def title(self):
        return "Génération de documents pour l'affaire {0}".format(
            self.context.name
        )

    @property
    def tree_url(self):
        return self.request.route_path(self.route_name, id=self.context.id)

    def get_template_datas(self):
        company = self.context.project.company
        company_datas = {
            'name': company.name,
            'address': company.address,
            'zip_code': company.zip_code,
            'city': company.city,
            'country': company.country,
            'email': company.email,
            'phone': company.phone,
            'mobile': company.mobile,
        }
        customer = self.context.get_customer()
        customer_datas = {
            'label': customer.label,
            'company_name': customer.company_name,
            'civilite': customer.civilite,
            'lastname': customer.lastname,
            'firstname': customer.firstname,
            'function': customer.function,
            'address': customer.address,
            'zip_code': customer.zip_code,
            'city': customer.city,
            'country': customer.country,
            'email': customer.email,
            'phone': customer.phone,
            'mobile': customer.mobile,
            'fax': customer.fax,
            'registration': customer.registration,
            'tva_intracomm': customer.tva_intracomm,
        }
        return dict(company=company_datas, customer=customer_datas)

    def py3o_action_view(self, business_type_id, file_type_id):
        model = self.context
        template = BusinessTypeFileTypeTemplate.query().filter_by(
            business_type_id=business_type_id
        ).filter_by(
            file_type_id=file_type_id
        ).first()
        if template:
            logger.debug(
                " + Templating ({}, {})".format(
                    template.file.name,
                    template.file_id,
                )
            )
            try:
                output_buffer = BytesIO()
                odt_builder = Template(template.file.data_obj, output_buffer)
                odt_builder.render(self.get_template_datas())
                write_file_to_request(
                    self.request, template.file.name, output_buffer
                )
                return self.request.response
            except UndefinedError as err:
                key = get_key_from_genshi_error(err)
                msg = """Erreur à la compilation du modèle la clé {0}
n'est pas définie""".format(key)
                logger.exception(msg)
                self.session.flash(msg, "error")
            except IOError:
                logger.exception("Le template n'existe pas sur le disque")
                self.session.flash(
                    "Erreur à la compilation du modèle, le modèle de fichier "
                    "est manquant sur disque. Merci de contacter votre "
                    "administrateur.",
                    "error",
                )
            except Exception:
                logger.exception(
                    "Une erreur est survenue à la compilation du template "
                    "{} avec un contexte de type {} et d'id {}".format(
                        template.file.id,
                        model.__class__,
                        model.id,
                    )
                )
                self.session.flash(
                    "Erreur à la compilation du modèle, merci de contacter "
                    "votre administrateur",
                    "error"
                )
        else:
            self.session.flash("Erreur : ce modèle est manquant", "error")
        return HTTPFound(self.request.current_route_path(_query={}))

    def __call__(self):
        remember_navigation_history(self.request, self.context.id)
        self.populate_navigation()
        business_type_id = self.context.business_type_id
        file_type_id = self.request.GET.get('file')
        if file_type_id:
            return self.py3o_action_view(business_type_id, file_type_id)
        else:
            available_templates = BusinessTypeFileTypeTemplate.query()
            available_templates = available_templates.filter_by(
                business_type_id=business_type_id
            ).join(FileType).order_by(FileType.label)
        return dict(
            title=self.title,
            help_message=self.help_message,
            templates=available_templates.all(),
        )


def includeme(config):
    config.add_tree_view(
        BusinessFileGeneration,
        route_name=BUSINESS_ITEM_PY3O_ROUTE,
        parent=ProjectBusinessListView,
        permission="py3o.business",
        renderer="/business/py3o.mako",
        layout="business",
    )
