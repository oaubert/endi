import colander
import pytest


def test_company_customer_schema():
    from endi.forms.third_party.customer import get_company_customer_schema
    schema = get_company_customer_schema()

    args = {
        "company_name": "Test customer",
        "civilite": "Monsieur",
        "address": "1 rue Victor Hugo",
        "lastname": "Lastname",
        "zip_code": "21000",
        "city": "Paris",
    }
    result = schema.deserialize(args)
    assert result['company_name'] == 'Test customer'

    # mandatory fields
    wrong = args.copy()
    wrong.pop('company_name')
    with pytest.raises(colander.Invalid):
        schema.deserialize(wrong)

    wrong = args.copy()
    wrong['email'] = 'wrongmail'
    with pytest.raises(colander.Invalid):
        schema.deserialize(wrong)

    wrong = args.copy()
    wrong['civilite'] = 'wrongone'
    with pytest.raises(colander.Invalid):
        schema.deserialize(wrong)


def test_individual_customer_schema():
    from endi.forms.third_party.customer import get_individual_customer_schema
    schema = get_individual_customer_schema()

    args = {
        "civilite": "M. et Mme",
        "address": "1 rue Victor Hugo",
        "lastname": "Lastname",
        "zip_code": "21000",
        "city": "Paris",
    }
    result = schema.deserialize(args)
    assert result['lastname'] == 'Lastname'

    # mandatory fields
    for field in ('lastname',):
        wrong = args.copy()
        wrong.pop(field)
        with pytest.raises(colander.Invalid):
            schema.deserialize(wrong)
