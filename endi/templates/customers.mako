<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <div role='group'>
        % if api.has_permission('add_customer'):
            <button class='btn btn-primary' onclick="toggleModal('customer_add_form'); return false;">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>Ajouter<span class="no_mobile"> un client</span>
            </button>
            <a class='btn' href="${request.route_path('company_customers_import_step1', id=request.context.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-import"></use></svg>Importer<span class="no_mobile"> des clients</span>
            </a>
        % endif
    </div>
    <%
    ## We build the link with the current search arguments
    args = request.GET
    url = request.route_path('customers.csv', id=request.context.id, _query=args)
    %>
    <a class='btn' href='${url}' title="Export au format CSV" >
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-csv"></use></svg>CSV
    </a>
</div>
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        <table class="hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
                    <th scope="col">${sortable("Code", "code")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du client", "label")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du contact principal", "lastname")}</th>
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
                % if records:
                    % for customer in records:
                        <tr class='tableelement' id="${customer.id}">
                            <% url = request.route_path("customer", id=customer.id) %>
                            <% onclick = "document.location='{url}'".format(url=url) %>
                            <td onclick="${onclick}" class="col_date" >${api.format_date(customer.created_at)}</td>
                            <td onclick="${onclick}">${customer.code}</td>
                            <td onclick="${onclick}" class="col_text" >
                                % if customer.archived:
                                    <small title="Ce client a été archivé"><span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#archive"></use></svg> Client archivé</span></small><br />
                                % endif
                                ${customer.label}
                            </td>
                            <td onclick="${onclick}" class="col_text" >
                                ${customer.get_name()}
                            </td>
                            <td class="col_actions width_four">
                                ${request.layout_manager.render_panel('action_buttons', buttons=stream_actions(customer))}
                            </td>
                        </tr>
                    % endfor
                % else:
                    <tr>
                        <td colspan='5' class="col_text">
                            <em>Aucun client n’a été référencé</em>
                        </td>
                    </tr>
                % endif
            </tbody>
        </table>
	</div>
	${pager(records)}
</div>

<section id="customer_add_form" class="modal_view size_middle" style="display: none;">
    <div role="dialog" id="customer-forms" aria-modal="true" aria-labelledby="customer-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('customer_add_form'); return false;">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
                </button>
                <h2 id="customer-forms_title">Ajouter un client</h2>
            </header>
            <nav>
                <ul class="nav nav-tabs modal-tabs" role="tablist" aria-label="Type de client">
                    <li role="presentation" class="active">
                        <a href="#companyForm" aria-controls="companyForm" role="tab" aria-selected="true" id="company">Personne morale</a>
                    </li>
                    <li role="presentation">
                        <a href="#individualForm" aria-controls="individualForm" role="tab" aria-selected="false" id="individual" tabindex="-1">Personne physique</a>
                    </li>
                </ul>
            </nav>
            <main>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active row" id="companyForm" aria-labelledby="company" tabindex="0">
                        <h3>${forms[0][0]|n}</h3>
                        ${forms[0][1].render()|n}
                    </div>
                    <div role="tabpanel" class="tab-pane row" id="individualForm" aria-labelledby="individual" tabindex="0" hidden>
                        <h3>${forms[1][0]|n}</h3>
                        ${forms[1][1].render()|n}
                    </div>
                </div>
            </main>
        </div>
    </div>
</section>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
