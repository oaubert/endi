import $ from 'jquery';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import EstimationLinkedModel from "../models/EstimationLinkedModel";

const template = require("./templates/EstimationLinkedView.mustache");

const EstimationLinkedView = Mn.View.extend({
    template: template,
    templateContext(){
        return {
            has_estimation: this.model.hasEstimationId(),
            estimation_id: this.model.get('estimation_id'),
            label: this.model.label(),
        };
    },
});

export default EstimationLinkedView;