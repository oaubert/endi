<%doc>
    Base template for task rendering
</%doc>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="" type="image/x-icon" />
        <meta name="description" comment="">
        <meta name="KEYWORDS" CONTENT="">
        <meta NAME="ROBOTS" CONTENT="INDEX,FOLLOW,ALL">
        <link href="${request.static_url('endi:static/css/pdf.css', _app_url='')}" rel="stylesheet"  type="text/css" />
        <% task = tasks[0] %>
            % if task.type_ == 'estimation':
                <% watermark = 'watermark_estimation.jpg' %>
            % else:
                <% watermark = 'watermark_invoice.jpg' %>
            % endif

<%doc>
PIED DE PAGE
Le calcul de la taille du pied de page dépend de la quantité de
contenu que l'on y affiche (les frames xhtml2pdf ne supporte pas d'overflow)
On calcule le nombre de ligne et on le multiplie par un coefficient "qui fonctionne"

Le coefficient de 0.8 utilisé ci-dessous fonctionne pour un pied de page
contenant entre 2 et 10 lignes.
NB : LE PROBLÈME SE POSE SURTOUT POUR LES PIEDS DE PAGE AYANT PEU DE LIGNES
(qui n'apparaissent pas quand on réduit le coef).
</%doc>
            <%
course_footer_height = common_footer_height = 0
if 'coop_pdffootertext' in request.config:
    common_footer_height = len(config.get('coop_pdffootertext').splitlines())
if 'coop_pdffootercourse' in request.config:
    course_footer_height = common_footer_height + len(config.get('coop_pdffootercourse').splitlines())
course_footer_height *= 0.8
common_footer_height *= 0.8
%>
<%doc>
Utilisation de différentes frame
Lorsque l'on fait des exports massifs de facture au format PDF, on a besoin que
les footer puissent être différents en fonction du type de facture (les
factures de formations ont des pieds de page différents).

On définit donc deux types de page @page et @page alternate et on change si
besoin de type de page lorsque l'on parcourt la liste des Task dont on fait le
rendu (cf tout en bas)
</%doc>
<% start_with_course = tasks[0].is_training() %>

        <style>
            @page {
                % if not task.status == 'valid':
                    background-image: url("${request.static_url('endi:static/{0}'.format(watermark), _app_url='')}");
                % endif
                @frame content_frame {
                    margin: 1cm;
                    % if start_with_course:
                        margin-bottom: ${course_footer_height + 1.5}cm;
                    % else:
                        margin-bottom: ${common_footer_height + 1.5}cm;
                    % endif
                    border: 0pt solid white;
                }
                @frame footer {
                    % if start_with_course:
                        -pdf-frame-content: coursefooter;
                        height: ${course_footer_height}cm;
                    %else:
                        -pdf-frame-content: commonfooter;
                        height: ${common_footer_height}cm;
                    % endif
                    bottom: 1.5cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    border: 0pt solid white;
                }
                % if task.status == 'valid':
                    @frame paging{
                        -pdf-frame-content: page-number;
                        bottom: 1cm;
                        margin-right: 1cm;
                        margin-left: 1cm;
                        height: 0.5cm;
                        font-size: 0.3cm;
                    }
                % endif
            }
            @page alternate {
                % if not task.status == 'valid':
                    background-image: url("${request.static_url('endi:static/{0}'.format(watermark), _app_url='')}");
                % endif
                @frame content_frame {
                    margin: 1cm;
                    % if start_with_course:
                        margin-bottom: ${common_footer_height + 1.5}cm;
                    % else:
                        margin-bottom: ${course_footer_height + 1.5}cm;
                    % endif
                    border: 0pt solid white;
                }
                @frame footer {
                    % if start_with_course:
                        -pdf-frame-content: commonfooter;
                        height: ${common_footer_height}cm;
                    %else:
                        -pdf-frame-content: coursefooter;
                        height: ${course_footer_height}cm;
                    % endif
                    bottom: 1.5cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    border: 0pt solid white;
                }
                % if task.status == 'valid':
                    @frame paging{
                        -pdf-frame-content: page-number;
                        bottom: 1cm;
                        margin-right: 1cm;
                        margin-left: 1cm;
                        height: 0.5cm;
                        font-size: 0.3cm;
                    }
                % endif
            }
        </style>
    </head>
    <body class="endi pdf_export
    % if task.status != 'valid':
    preview-view
    % endif
    ">
    	<div class="task_view">
        % for task in tasks:
            ${request.layout_manager.render_panel('{0}_html'.format(task.type_), task=task, bulk=bulk)}
            % if not loop.last:
                % if tasks[loop.index + 1].is_training() and not start_with_course:
                    <pdf:nexttemplate name="alternate"/>
                    <pdf:nextpage />
                % elif not task.is_training() and start_with_course:
                    <pdf:nexttemplate name="alternate"/>
                    <pdf:nextpage />
                %else:
                    <pdf:nexttemplate loop.index=0 />
                    <pdf:nextpage />
                % endif
            % endif
        % endfor
    	</div>
    </body>
</html>
