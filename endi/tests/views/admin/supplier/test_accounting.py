import pytest

from endi.models.config import (
    get_config,
)
pytest.mark.usefixtures("config")


def test_config_cae_success(config, dbsession, get_csrf_request_with_db):
    from endi.views.admin.supplier.accounting import (
        SupplierAccountingConfigView,
        SUPPLIER_ACCOUNTING_URL,
    )

    SupplierAccountingConfigView.back_link = SUPPLIER_ACCOUNTING_URL

    appstruct = {
        'cae_general_supplier_account': "00000111",
        'cae_third_party_supplier_account': "000002222",
    }
    view = SupplierAccountingConfigView(get_csrf_request_with_db())
    view.submit_success(appstruct)
    config = get_config()
    for key, value in list(appstruct.items()):
        assert config[key] == value
