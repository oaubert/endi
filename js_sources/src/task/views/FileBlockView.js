import Mn from 'backbone.marionette';
import FileRequirementView from './FileRequirementView.js';

var template = require("./templates/FileBlockView.mustache");


export const FileRequirementCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: FileRequirementView
});

const FileBlockView = Mn.View.extend({
    tagName: 'div',
    className: 'form-section',
    template: template,
    regions: {
        files: '.files',
    },
    onRender: function(){
        this.listenTo(this.collection, 'sync', this.render.bind(this));
        var view = new FileRequirementCollectionView(
            {collection: this.collection}
        );
        this.showChildView('files', view);
    }
});
export default FileBlockView;
