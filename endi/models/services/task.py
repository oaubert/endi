import datetime
from sqlalchemy import or_
from sqlalchemy.orm import load_only
from endi_base.models.base import DBSESSION
from endi.models.tva import (
    Tva,
    Product,
)
from endi.utils import strings


class TaskService(object):
    models = None

    @classmethod
    def get_tva_objects(cls, task_obj):
        """
        :param task_obj: The Task object we want to collect tvas for
        :returns: tva stored by amount
        :rtype: dict
        """
        tva_values = set()
        for group in task_obj.line_groups:
            for line in group.lines:
                tva_values.add(line.tva)

        # Cas des certificats énergie
        # (on peut avoir une remise à 0% de tva indépendamment des taux de tva
        # du document)
        for discount in task_obj.discounts:
            tva_values.add(discount.tva)

        tvas = Tva.query().filter(
            Tva.value.in_(list(tva_values))
        ).all()
        return dict([(tva.value, tva) for tva in tvas])

    @classmethod
    def get_valid_tasks(cls, task_cls, *args):
        from endi.models.task import Invoice, CancelInvoice
        query = super(task_cls, task_cls).query(*args)
        query = query.with_polymorphic([Invoice, CancelInvoice])
        query = query.filter(task_cls.status == 'valid')
        query = query.filter(task_cls.type_.in_(('invoice', 'cancelinvoice')))
        return query

    @classmethod
    def get_waiting_estimations(cls, *args):
        from endi.models.task import Estimation
        query = Estimation.query(*args)
        query = query.filter(Estimation.status == 'wait')
        query = query.order_by(Estimation.status_date)
        return query

    @classmethod
    def get_waiting_invoices(cls, task_cls, *args):
        from endi.models.task import Invoice, CancelInvoice
        query = super(task_cls, task_cls).query(*args)
        query = query.with_polymorphic([Invoice, CancelInvoice])
        query = query.filter(task_cls.type_.in_(('invoice', 'cancelinvoice')))
        query = query.filter(task_cls.status == 'wait')
        query = query.order_by(task_cls.type_).order_by(task_cls.status_date)
        return query

    @classmethod
    def from_price_study(
        cls, task_class, price_study, user, customer, **kwargs
    ):
        from endi.models.task.task import TaskLineGroup
        from endi.models.task.task import DiscountLine
        task_line_groups = []
        for product in price_study.products:
            task_line_groups.append(
                TaskLineGroup.from_price_study_product(product)
            )

        task = task_class(
            user,
            price_study.company,
            project=price_study.project,
            customer=customer,
            financial_year=datetime.date.today().year,
            price_study_id=price_study.id,
            **kwargs
        )
        task.line_groups = task_line_groups
        task.set_default_validity_duration()

        for discount in price_study.discounts:
            for line in DiscountLine.from_price_study_discount(discount):
                task.discounts.append(line)
        return task

    @classmethod
    def sync_with_price_study(cls, task, price_study):
        from endi_base.models.base import DBSESSION
        from endi.models.task.task import TaskLineGroup
        from endi.models.task.task import DiscountLine

        for group in task.line_groups:
            DBSESSION().delete(group)

        for product in price_study.products:
            task.line_groups.append(
                TaskLineGroup.from_price_study_product(product)
            )

        for discount in task.discounts:
            DBSESSION().delete(discount)

        for discount in price_study.discounts:
            for line in DiscountLine.from_price_study_discount(discount):
                task.discounts.append(line)
        return task

    @classmethod
    def find_task_status_date(cls, taskclass, official_number, year):
        """
        Query the database to retrieve a task with the given number and year
        and returns its status_date

        :param str official_number: The official number
        :param int year: The financial year associated to the invoice
        :returns: The document's status_date
        :rtype: datetime.dateime
        """
        from endi.models.task import Invoice, CancelInvoice
        query = DBSESSION().query(taskclass).with_polymorphic(
            [Invoice, CancelInvoice]
        ).options(load_only('status_date')).filter_by(
            official_number=official_number
        )
        if year:
            query = query.filter(
                or_(
                    Invoice.financial_year == year,
                    CancelInvoice.financial_year == year
                )
            )
        return query.one().status_date

    @classmethod
    def has_price_study(cls, task):
        return task.price_study_id is not None

    @classmethod
    def get_price_study(cls, task):
        return task.price_study

    @classmethod
    def format_amount(
            cls, task, amount, trim=True, grouping=True, precision=2
    ):
        """
        Return a formatted amount in the context of the current task

        if the amount is not supposed to be trimmed, we retrieve the Task's
        decimal_to_show and pass it to the format_amount function

        :param obj task: Instance of class <endi.models.task.Task>
        :param int amount: The amount for format
        """
        display_precision = None
        if not trim:
            display_precision = task.decimal_to_display
        return strings.format_amount(
            amount, trim=trim, grouping=grouping, precision=precision,
            display_precision=display_precision
        )


class InvoiceService(TaskService):
    @classmethod
    def get_price_study(cls, invoice):
        result = None
        if invoice.price_study:
            result = invoice.price_study
        elif invoice.estimation.price_study:
            result = invoice.estimation.price_study
        return result

    @classmethod
    def has_price_study(cls, invoice):
        estimation_has_price_study = False

        if invoice.estimation_id is not None:
            estimation_has_price_study = invoice.estimation.has_price_study()

        return TaskService.has_price_study(invoice) or \
            estimation_has_price_study


class CancelInvoiceService(TaskService):
    @classmethod
    def get_price_study(cls, task):
        return None

    @classmethod
    def has_price_study(cls, task):
        return False


class TaskLineGroupService(object):
    @classmethod
    def from_price_study_product(cls, group_class, product):
        from endi.models.price_study.product import PriceStudyProduct
        from endi.models.price_study.work import PriceStudyWork

        from endi.models.task.task import TaskLine

        group = group_class()
        if isinstance(product, PriceStudyProduct):
            group.lines = [
                TaskLine.from_price_study_product(product)
            ]
        elif isinstance(product, PriceStudyWork):
            group.title = product.title
            if product.display_details:
                group.description = product.description
                for item in product.items:
                    group.lines.append(
                        TaskLine.from_price_study_work_item(item)
                    )
            else:
                # On crée une seule ligne directement depuis le ProductWork
                group.lines = [
                    TaskLine.from_price_study_work(product)
                ]

        return group

    @classmethod
    def from_sale_product_work(cls, group_class, product, mode='ht'):
        from endi.models.task.task import TaskLine

        group = group_class()
        group.title = product.title
        group.description = product.description
        for item in product.items:
            group.lines.append(
                TaskLine.from_sale_product_work_item(item, mode=mode)
            )
        return group


class TaskLineService(object):

    @classmethod
    def from_price_study_product(cls, line_class, product):
        from endi.models.tva import Tva
        result = line_class()
        result.description = product.description
        result.cost = product.ht
        result.unity = product.unity
        result.quantity = product.quantity
        if product.tva:
            result.tva = product.tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = product.product_id
        return result

    @classmethod
    def from_price_study_work(cls, line_class, product_work):
        from endi.models.tva import Tva
        result = line_class()
        result.description = product_work.description
        result.cost = product_work.ht
        result.unity = product_work.unity
        result.quantity = product_work.quantity

        tva = product_work.find_common_value('tva')
        if tva:
            result.tva = tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = product_work.find_common_value('product_id')
        return result

    @classmethod
    def from_price_study_work_item(cls, line_class, work_item):
        from endi.models.tva import Tva
        result = line_class()
        result.description = work_item.description
        result.cost = work_item.ht
        result.unity = work_item.unity
        result.quantity = work_item.total_quantity
        if work_item.tva:
            result.tva = work_item.tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = work_item.product_id
        return result

    @classmethod
    def from_sale_product_work_item(cls, line_class, work_item, mode='ht'):
        result = line_class()
        result.description = work_item.description
        if mode == 'ht':
            result.cost = work_item.ht
        else:
            result.cost = work_item.total_ttc()
        result.mode = mode
        result.unity = work_item.unity
        result.quantity = work_item.quantity
        cls._set_tva_and_product(result, work_item)
        return result

    @classmethod
    def from_sale_product(cls, line_class, sale_product, mode='ht'):
        result = line_class()
        result.description = sale_product.description
        result.mode = mode
        if mode == 'ht':
            result.cost = sale_product.ht
        else:
            result.cost = sale_product.ttc()

        result.unity = sale_product.unity

        result.quantity = 1
        cls._set_tva_and_product(result, sale_product)
        return result

    @classmethod
    def _set_tva_and_product(cls, task_line, sale_product_entry):
        if sale_product_entry.tva:
            task_line.tva = sale_product_entry.tva.value
            task_line.product_id = sale_product_entry.product_id
        else:
            task_line.tva = Tva.get_default().value

        if task_line.tva and not task_line.product_id:
            task_line.product_id = Product.first_by_tva_value(task_line.tva)


class DiscountLineService(object):
    @classmethod
    def from_price_study_discount(cls, price_study_discount):
        from endi.models.task import DiscountLine

        for tva, ht in list(price_study_discount.ht_by_tva().items()):
            result = DiscountLine()
            result.description = price_study_discount.description
            result.amount = ht
            result.tva = tva.value
            yield result
