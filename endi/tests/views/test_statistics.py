import pytest
from endi.models import statistics as models


@pytest.fixture
def sheet(dbsession):
    sheet = models.StatisticSheet(title="test_sheet")
    dbsession.add(sheet)
    dbsession.flush()
    return sheet


@pytest.fixture
def full_sheet(dbsession, sheet):
    entry = models.StatisticEntry(
        title="test_entry",
        description="test entry",
        sheet_id=sheet.id,
    )
    dbsession.add(entry)
    dbsession.flush()
    criterion_1 = models.CommonStatisticCriterion(
        key="parcours_num_hours",
        method="true",
        entry_id=entry.id
    )
    criterion_2 = models.OrStatisticCriterion(
        entry_id=entry.id,
    )
    dbsession.add(criterion_1)
    dbsession.add(criterion_2)
    dbsession.flush()

    criterion_21 = models.CommonStatisticCriterion(
        key="coordonnees_firstname",
        method="nnll",
        entry_id=entry.id,
        parent_id=criterion_2.id,
    )
    criterion_22 = models.CommonStatisticCriterion(
        key="coordonnees_lastname",
        method="sw",
        entry_id=entry.id,
        parent_id=criterion_2.id,
    )
    dbsession.add(criterion_21)
    dbsession.add(criterion_22)
    dbsession.flush()
    return sheet


def test_rest_sheet(full_sheet, get_csrf_request_with_db):
    from endi.views.statistics import RestStatisticSheet
    request = get_csrf_request_with_db()
    request.context = full_sheet
    view = RestStatisticSheet(request)
    res = view.get()

    assert res['sheet'].title == 'test_sheet'
    # Le lien entre criterion est secondaire
    assert len(res['entries'][0].criteria) == 4


def test_rest_entry_add(full_sheet, get_csrf_request_with_db):
    from endi.views.statistics import RestStatisticEntry
    appstruct = {
        "title": "Nouvelle entrée",
        "description": "Description",
    }
    request = get_csrf_request_with_db()
    request.context = full_sheet
    request.json_body = appstruct
    view = RestStatisticEntry(request)
    res = view.post()

    entry = full_sheet.entries[1]
    assert entry.title == 'Nouvelle entrée'
    assert entry.description == "Description"


def test_rest_entry_edit(full_sheet, get_csrf_request_with_db):
    from endi.views.statistics import RestStatisticEntry
    appstruct = {
        "title": "Entrée éditée",
    }
    request = get_csrf_request_with_db()
    request.context = full_sheet.entries[0]
    request.json_body = appstruct
    view = RestStatisticEntry(request)
    res = view.put()

    entry = full_sheet.entries[0]
    assert entry.title == 'Entrée éditée'
    assert entry.description == "test entry"

