/*
 * Module name : WorkItemComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ButtonModel from '../../base/models/ButtonModel.js';
import ButtonWidget from '../../widgets/ButtonWidget.js'

import { getOpt } from '../../tools.js';
import WorkItemModel from '../models/WorkItemModel.js';
import WorkItemCollectionView from './WorkItemCollectionView.js';

import WorkItemForm from './WorkItemForm.js';

const template = require('./templates/WorkItemComponent.mustache');

const WorkItemComponent = Mn.View.extend({
    tagName: "fieldset",
    className: "separate_block border_left_block",
    template: template,
    regions: {
        list: {el: 'tbody', replaceElement: true},
        addButton: '.add',
        popin: '.popin',
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to collection events
    collectionEvents: {
    },
    childViewEventPrefix: 'work:item',
    // Listen to child view events
    childViewEvents: {
        'action:clicked': "onActionClicked",
        'cancel:form': 'render',
        'destroy:modal': 'render',
        'model:edit': 'onModelEdit',
        'model:delete': 'onModelDelete',
        'work:item:model:delete': 'onModelDelete',
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
        this.app = Radio.channel('app');
    },
    showAddButton(){
        let model = new ButtonModel({
            label: "Ajouter un produit",
            icon: "plus",
            action: "add"
        });
        let view = new ButtonWidget({model: model});
        this.showChildView('addButton', view);
    },
    showModelForm(model, edit){
        let view = new WorkItemForm(
            {
                model: model,
                edit:edit,
                destCollection: this.collection,
            }
        );
        this.app.trigger('show:modal', view);
    },
    onRender(){
        let view = new WorkItemCollectionView({collection: this.collection});
        this.showChildView('list', view);
        this.showAddButton();
    },
    templateContext(){
        return {};
    },
    onActionClicked(action){
        if (action == 'add'){
            let model = new WorkItemModel();
            this.showModelForm(model, false);
        }
    },
    onModelDeleteSuccess: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('success', this, "Vos données ont bien été supprimées");
    },
    onModelDeleteError: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('error', this,
                           "Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    onModelDelete(model, childView){
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce produit ?");
        if (result){
            childView.model.destroy(
                {
                    success: this.onModelDeleteSuccess.bind(this),
                    error: this.onModelDeleteError.bind(this),
                    wait: true
                }
            );
        }
    },
    onModelEdit(model, childView){
        this.showModelForm(model, true);
    }
});
export default WorkItemComponent
