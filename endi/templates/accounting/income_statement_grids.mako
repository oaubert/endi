<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='content'>

${searchform()}

% if records is not None:
    <div>
        <div>
            ${records.item_count} Résultat(s)
        </div>
        <div class='table_container'>
            <table class="hover_table">
                <thead>
                    <tr>
                        <th scope="col" class="col_text">${sortable("Enseigne", "company")}</th>
                        <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                    </tr>
                </thead>
                <tbody>
                % if records:
                    % for record in records:
                        <tr>
                            <td class="col_text">Compte de résultat de l'enseigne ${record.name}</td>
                            <td class='col_actions width_one'>
                                ${request.layout_manager.render_panel('action_dropdown', links=stream_actions(record))}
                            </td>
                        </tr>
                    % endfor
                % else:
                    <tr>
                        <td colspan='6' class="col_text">Aucun état n'a été généré</td>
                    </tr>
                % endif
                </tbody>
            </table>
        </div>
        ${pager(records)}
    </div>
% endif
</%block>
