import BaseToolbarAppClass from "../../common/components/ToolbarAppClass";
import PriceStudyResume from '../views/PriceStudyResume';
import Radio from 'backbone.radio';

const ToolbarAppClass = BaseToolbarAppClass.extend({
    getResumeView(actions){
        const facade = Radio.channel('facade');
        const model = facade.request('get:model', 'price_study');
        return new PriceStudyResume({model: model});
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;