import Mn from 'backbone.marionette';
import InputWidget from '../../../widgets/InputWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import ModalFormBehavior from '../../../base/behaviors/ModalFormBehavior.js';
import { ajax_call, getOpt } from '../../../tools.js';
import DatePickerWidget from '../../../widgets/DatePickerWidget.js';

var template = require('./templates/PaymentLineFormView.mustache');

const PaymentLineFormView = Mn.View.extend({
    id: 'payments_form',
    behaviors: [ModalFormBehavior],
    template: template,
    regions: {
        'order': '.order',
        'description': ".description",
        "date": ".date",
        "amount": ".amount",
    },
    onRender: function(){
        this.showChildView(
            'order',
            new InputWidget({
                value: this.model.get('order'),
                field_name:'order',
                type: 'hidden',
            })
        );
        var view = new TextAreaWidget(
            {
                field_name: "description",
                value: this.model.get('description'),
                title: "Intitulé",
                required: true,
            }
        );
        this.showChildView('description', view);

        if (this.getOption('show_date')){
            view = new DatePickerWidget(
                {
                    date: this.model.get('date'),
                    title: "Date",
                    field_name: "date",
                    required: true,
                }
            );
            this.showChildView('date', view);
        }

        if (this.getOption('edit_amount')){
            view = new InputWidget(
                {
                    field_name: 'amount',
                    value: this.model.get('amount'),
                    title: "Montant",
                    required: true,
                }
            );
            this.showChildView('amount', view);
        }
    },
    templateContext(){
        return {
            title: this.getOption('title')
        }
    }
});
export default PaymentLineFormView;
