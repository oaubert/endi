def includeme(config):
    config.include('.routes')
    config.include('.operations')
    config.include('.treasury_measures')
    config.include('.income_statement_measures')
    config.include('.rest_api')
    config.include('.bank_remittances')
