/*
 * Module name : CategoryForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior';
import InputWidget from '../../widgets/InputWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';

const template = require('./templates/CategoryForm.mustache');

const CategoryForm = Mn.View.extend({
    template: template,
    id: "category-modal",
    behaviors: [ModalFormBehavior],
    regions: {
        title: ".field-title",
        description: ".field-description"
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onSyncSuccess(){

    },
    onRender(){
        this.showChildView(
            'title',
            new InputWidget({
                field_name: 'title',
                value: this.model.get('title'),
                label: "Titre de la catégorie",
                required: true
            })
        );
        this.showChildView(
            'description',
            new TextAreaWidget(
                {
                    field_name: 'description',
                    value: this.model.get('description'),
                    label: "Description"
                }
            )
        );
    },
    templateContext(){
        let title = "Ajouter";
        if (this.getOption('edit')){
            title = "Modifier";
        }
        return {edit: this.getOption('edit'), title: title};
    }
});
export default CategoryForm
