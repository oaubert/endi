"""
    script utility, allows the use of the app's context (database, models ...)
    from within command line calls
"""
import logging
from docopt import docopt
from pyramid.paster import bootstrap
from transaction import commit
from pyramid.paster import setup_logging
from endi_base.utils import ascii


def command(func, doc):
    """
    Usefull function to wrap command line scripts
    """
    logging.basicConfig()
    args = docopt(doc)
    pyramid_env = bootstrap(args['<config_uri>'])
    setup_logging(args['<config_uri>'])
    try:
        func(args, pyramid_env)
    finally:
        pyramid_env['closer']()
    commit()
    return 0


def get_argument_value(arguments, key, default=None):
    """
    Return the value for an argument named key in arguments or default

    :param dict arguments: The cmd line arguments returned by docopt
    :param str key: The key we look for (type => --type)
    :param str default: The default value (default None)

    :returns: The value or default
    :rtype: str
    """
    val = arguments.get('<%s>' % key)
    if not val:
        val = default

    return ascii.force_unicode(val)


def get_value(arguments, key, default=None):
    """
    Return the value of an option named key in arguments or default

    :param dict arguments: The cmd line arguments returned by docopt
    :param str key: The key we look for (type => --type)
    :param str default: The default value (default None)

    :returns: The value or default
    :rtype: str
    """
    val = arguments.get('--%s' % key)
    if not val:
        val = default

    return ascii.force_unicode(val)


def has_value(arguments, key):
    """
    Check if a key is provided in the arguments

    :param dict arguments: The cmd line arguments returned by docopt
    :param str key: The key we look for (type => --type)

    :returns: True or False
    :rtype: bool
    """
    return arguments.get('--%s' % key) is not None
