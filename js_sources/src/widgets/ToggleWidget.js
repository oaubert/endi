import Mn from 'backbone.marionette';
import { updateSelectOptions, ajax_call, showLoader} from '../tools.js';

var template = require('./templates/ToggleWidget.mustache');

const ToggleWidget = Mn.View.extend({
    template:template,
    ui: {
        buttons: '.btn'
    },
    events: {
        'click @ui.buttons': 'onClick',
   },
   onClick(event){
        showLoader();
        let url = this.model.get('options').url;
        var value = $(event.target).parent().parent().find('input').val();
        ajax_call(url, {'submit': value}, 'POST', {success: this.refresh});
    },
    refresh: function(){
        window.location.reload();
    },
    templateContext(){
        let buttons = this.model.get('options').buttons;
        let current_value = this.model.get('options').current_value;
        updateSelectOptions(buttons, current_value, "status");

        return {
            name: this.model.get('options').name,
            buttons: buttons
        }
    },
});

export default ToggleWidget;
