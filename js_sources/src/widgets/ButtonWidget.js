import { getOpt } from '../tools.js';
import Mn from 'backbone.marionette';
import IconWidget from './IconWidget.js';

const ButtonWidget = Mn.View.extend({
    /*
     * View for a ButtonModel
     * Returns a button wrapped in the surroudingTagName or span (default)
     *
     *
     * Supports :
     * surroundingTagName
     *
     * :param obj model: A ButtonModel instance
     * :param str surroudingTagName: The tagName of this view (default: span)
     *
     * Full example :
     *
     *      let model = new ButtonModel({label: 'click me', icon: 'plus', showLabel: false});
     *      this.showChildView('button-container', new ButtonWidget({model: model}));
     */
    template: require('./templates/ButtonWidget.mustache'),
    tagName: function(){
        return getOpt(this, 'surroundingTagName', 'span');
    },
    regions: {
        icon: {el: 'icon', replaceElement: true},
    },
    ui: {
        btn: 'button'
    },
    events: {
        'click @ui.btn': 'onButtonClicked'
    },
    onButtonClicked(event){
        let actionName = $(event.currentTarget).data('action');
        console.log("action:clicked trigger %s", actionName);
        this.triggerMethod('action:clicked', actionName);
    },
    onRender(){
        let icon = this.model.get('icon');
        if (icon !== false){
            this.showChildView('icon', new IconWidget({icon: icon}));
        } else {
            this.removeRegion('icon');
        }
    },
    templateContext(){
        let css = getOpt(this, 'css', '');
        if (this.model.get('css')){
            css += ' ' + this.model.get('css');
        }
        let result = {
            surroundingTagName: getOpt(this, 'surroundingTagName', false),
            css: css
        }
        if (! this.model.has('title')){
            result['title'] = this.model.get('label');
        }
        return result;
    }
});
export default ButtonWidget;
