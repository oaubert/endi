<%doc>
    invoice panel template
</%doc>
<%inherit file="/panels/task/base.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<h3>Facture N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.official_number}</strong></h3>
	<strong>Libellé : </strong>${task.internal_number}<br />
	% if task.estimation is not None:
		<strong>Cette facture est associée au devis : </strong>${task.estimation.internal_number}<br />
	% endif
	<strong>Objet : </strong>${format_text(task.description)}<br />
	% if config.get('coop_invoiceheader'):
		<div class="coop_header">${format_text(config['coop_invoiceheader'])}</div>
	% endif
</div>
</%block>

<%block name="notes_and_conditions">
## DATE DE DEBUT DES PRESTATIONS
% if task.start_date:
	<div class='pdf_mention_block start_date'>
		<p class="content"><strong>Date de début des prestations :</strong> le ${api.format_date(task.start_date, False)}</p>
	</div>
% endif
## LIEU D'EXECUTION
% if task.workplace:
	<div class='pdf_mention_block workplace'>
		<h4>Lieu d'exécution</h4>
		<p class="content">${format_text(task.workplace)}</p>
	</div>
% endif
## CONDITIONS DE PAIEMENT
%if task.payment_conditions:
    ${table("Conditions de paiement", task.payment_conditions)}
% endif
</%block>

<%block name='footer_number'>
Facture N<span class="screen-reader-text">umér</span><sup>o</sup> ${task.official_number}
</%block>
