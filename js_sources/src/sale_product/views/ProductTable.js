/*
 * Module name : ProductTable
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ProductView  from './ProductView.js';
import ProductEmptyView  from './ProductEmptyView.js';
import { sortCollection } from "../../tools";

const template = require('./templates/ProductTable.mustache');

const ProductCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: ProductView,
    emptyView: ProductEmptyView,
    collectionEvents: {
        'sync': 'render'
    },
    // Bubble up child view events
    childViewTriggers: {
        'model:edit': 'model:edit',
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
    },
});

const ProductTable = Mn.View.extend({
    template: template,
    regions: {
        body: {el: 'tbody', replaceElement: true}
    },
    ui: {},
    // Listen to the current's view events
    events: {
        "click .sortable": "sortTable"
    },
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
        'model:edit': 'model:edit',
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
    },
    initialize(){
        this.facade = Radio.channel('facade');
    },
    onBeforeRender: function() {
        sortCollection(this.collection, 'id', 'desc');
    },
    onRender(){
        this.showChildView(
            'body',
            new ProductCollectionView({collection: this.collection})
        );
    },
    templateContext(){
        return {
            totalRecords: this.collection.state.totalRecords,
            currentPage: this.collection.state.currentPage + 1,
            totalPages: this.collection.state.totalPages,
            hasPreviousPage: this.collection.hasPreviousPage(),
            hasNextPage: this.collection.hasNextPage(),
        };
    },
    sortTable: function(e){
        e.preventDefault();

        const $e = $(e.currentTarget);
        const sortBy = $e.data('sort');
        const direction = $e.hasClass('asc') ? 'desc' : 'asc';
        const baseSvgFragment = '#sort-arrow';

        $(".sortable").each(function(){
            $(this).removeClass('asc desc');
            $(this).children('a').removeClass('asc desc current');
            const currentSvgFragment = $(this).find('use').attr('href').split('#');
            $(this).find('use').attr('href', `${currentSvgFragment[0]}${baseSvgFragment}`)
        });

        $e.addClass(`${direction} current`);
        $e.children('a').addClass(`${direction} current`);
        const currentSvgFragment = $e.find("use").attr("href").split("#");
        const nextSvgFragment = direction === "asc" ? "#sort-asc" : "#sort-desc";
        $e.find("use").attr("href", `${currentSvgFragment[0]}${nextSvgFragment}`) ;

        sortCollection(this.collection, sortBy, direction);
    }
});
export default ProductTable
