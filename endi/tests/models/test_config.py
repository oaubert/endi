import pytest
import datetime


def test_load_value(dbsession):
    from endi.models.config import get_config, Config
    dbsession.add(Config(name="name", value="value"))
    dbsession.flush()
    all_ = get_config()
    assert "name" in list(all_.keys())
    assert all_["name"] == "value"


def test_load_date_value(dbsession):
    from endi.models.config import Config
    dbsession.add(Config(name="datekey", value="2018-01-01"))
    dbsession.flush()

    assert Config.get_value("datekey", type_=datetime.date) == datetime.date(
        2018, 0o1, 0o1
    )

    dbsession.add(Config(name="falsydatekey", value=""))
    dbsession.flush()
    assert Config.get_value(
        "falsydatekey", type_=datetime.date, default=""
    ) == ""

    with pytest.raises(ValueError):
        Config.get_value("falsydatekey", type_=datetime.date)


def test_load_int_value(dbsession):
    from endi.models.config import Config
    dbsession.add(Config(name="intkey", value="125"))
    dbsession.flush()

    assert Config.get_value("intkey", type_=int) == 125

    dbsession.add(Config(name="falsyintkey", value=""))
    dbsession.flush()
    assert Config.get_value(
        "falsyintkey", type_=int, default=0
    ) == 0

    with pytest.raises(ValueError):
        Config.get_value("falsyintkey", type_=int)
