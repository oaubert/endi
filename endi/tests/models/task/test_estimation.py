import datetime


def test_estimation_set_numbers(full_estimation):
    full_estimation.date = datetime.date(1969, 7, 1)
    full_estimation.set_numbers(5, 18)
    assert full_estimation.internal_number == "Company 1969-07 D5"
    assert full_estimation.name == "Devis 18"
    assert full_estimation.project_index == 18


def test_duplicate_estimation(full_estimation):
    newestimation = full_estimation.duplicate(
        full_estimation.owner,
        project=full_estimation.project,
        phase=full_estimation.phase,
        customer=full_estimation.customer,
    )
    for key in "customer", "address", "expenses_ht", "workplace", \
            "start_date", "validity_duration":
        assert getattr(newestimation, key) == getattr(full_estimation, key)
    assert newestimation.status == 'draft'
    assert newestimation.project == full_estimation.project
    assert newestimation.status_person == full_estimation.owner
    assert newestimation.internal_number.startswith("Company {0:%Y-%m}".format(
        datetime.date.today()
    ))
    assert newestimation.phase == full_estimation.phase
    assert newestimation.mentions == full_estimation.mentions
    assert len(full_estimation.default_line_group.lines) == len(
        newestimation.default_line_group.lines
    )
    assert len(full_estimation.payment_lines) == len(
        newestimation.payment_lines
    )
    assert len(full_estimation.discounts) == len(newestimation.discounts)


def test_duplicate_estimation_mode_ht(full_estimation):
    duplicated_estimation = full_estimation.duplicate(
        full_estimation.owner,
        project=full_estimation.project,
    )
    assert duplicated_estimation.mode == full_estimation.mode
    assert duplicated_estimation.mode == 'ht'  # default value


def test_duplicate_estimation_mode_ttc(full_estimation):
    full_estimation.mode = 'ttc'
    duplicated_estimation = full_estimation.duplicate(
        full_estimation.owner,
        project=full_estimation.project,
    )
    assert duplicated_estimation.mode == full_estimation.mode
    assert duplicated_estimation.mode == 'ttc'


def test_duplicate_estimation_decimal_to_display(full_estimation, company):
    duplicated_estimation = full_estimation.duplicate(
        full_estimation.owner,
        project=full_estimation.project,
    )
    assert duplicated_estimation.decimal_to_display == 2

    company.decimal_to_display = 5
    duplicated_estimation = full_estimation.duplicate(
        full_estimation.owner,
        project=full_estimation.project,
    )
    assert duplicated_estimation.decimal_to_display == 5


def test_duplicate_payment_line(payment_line):
    newline = payment_line.duplicate()
    for i in ('order', 'description', 'amount'):
        assert getattr(newline, i) == getattr(payment_line, i)

    today = datetime.date.today()
    assert newline.date == today


def test_set_default_validity_duration(mk_estimation):
    # https://framagit.org/endi/endi/-/issues/2181
    from endi.models.config import Config
    Config.set("estimation_validity_duration_default", "AAA")

    estimation1 = mk_estimation()
    estimation1.set_default_validity_duration()
    assert estimation1.validity_duration == 'AAA'

    estimation2 = mk_estimation(validity_duration='BBB')

    estimation3 = mk_estimation()
    estimation3.set_default_validity_duration()
    assert estimation3.validity_duration == 'BBB'
