import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import Validation from 'backbone-validation';
import FormBehavior from "../../base/behaviors/FormBehavior.js";

import InputWidget from '../../widgets/InputWidget.js';
import StatusHistoryView from './StatusHistoryView.js';


var template = require("./templates/GeneralView.mustache");


const GeneralView = Mn.View.extend({
    /*
     * Wrapper around the component making part of the 'common'
     * invoice/estimation form, provide a main layout with regions for each
     * field
     */
    behaviors: [
        {
            behaviorClass: FormBehavior,
            errorMessage: "Vérifiez votre saisie"
        }
    ],
    tagName: 'div',
    className: 'separate_block border_left_block',
    template: template,
    regions: {
        status_history: '.status_history',
        name: '.name',
        financial_year: '.financial_year',
    },
    modelEvents: {
        'validated:invalid': 'showErrors',
        'validated:valid': 'hideErrors',
        'sync': 'render',
    },
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    initialize(options){
        this.section = options['section'];
        this.attachments = Radio.channel('facade').request('get:attachments');

        this.business_types_options = Radio.channel('config').request(
            'get:options', 'business_types'
        );
        var channel = Radio.channel('facade');
        this.listenTo(channel, 'bind:validation', this.bindValidation);
        this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    },
    bindValidation(){
        Validation.bind(this);
    },
    unbindValidation(){
        Validation.unbind(this);
    },
    showErrors(model, errors){
        console.log("We show errors");
        this.$el.addClass('error');
    },
    hideErrors(model){
        console.log("We hide errors");
        this.$el.removeClass('error');
    },
    templateContext(){
        let result = {attachments: this.attachments};
        if (this.business_types_options.length > 1){
            result['business_type'] = this.model.getBusinessType();
        }
        return result;
    },
    showStatusHistory(){
        var collection = Radio.channel('facade').request(
            'get:status_history_collection'
        );
        if (collection.models.length > 0){
            var view = new StatusHistoryView({collection: collection});
            this.showChildView('status_history', view);
        }
    },
    onRender(){
        this.showStatusHistory();
        this.showChildView(
            'name',
            new InputWidget({
                title: "Nom du document",
                value: this.model.get('name'),
                field_name: 'name',
            })
        );
        if (_.has(this.section, 'financial_year')){
            this.showChildView(
                'financial_year',
                new InputWidget({
                    title: "Année comptable de référence",
                    value: this.model.get('financial_year'),
                    field_name: 'financial_year',
                })
            );
        }
    }
});
export default GeneralView;
