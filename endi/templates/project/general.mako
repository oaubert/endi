<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%block name="mainblock">
<h3>Client(s)</h3>
<div class="layout flex four_cols">
% for customer in project.customers:
    <div class="layout flex editable">
        <address>
            ${format_text(customer.full_address)}
        </address>
        <button onclick='openPopup("${request.route_path('customer', id=customer.id, _query={'action': 'edit'})}")'
            class='btn icon only'
            title='Modifier ce client'
            aria-label='Modifier ce client'
            >
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
        </button>
    </div>
% endfor
</div>
<div class="separate_top data_display">
	<h3>Informations générales</h3>
	<dl>
		<dt>Type de dossier :</dt><dd>${project.project_type.label}</dd>
		<dt>Mode de calcul :</dt>
		<dd><span class="icon status mode"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#mode-${project.mode}"></use></svg></span></dd>
		% if project.project_type.default_business_type:
			<dt>Type d'affaire par défaut:</dt>
			<dd>${project.project_type.default_business_type.label}</dd>
		% endif
		% if project.business_types:
			<dt>Autre types d'affaire :</dt>
			<dd>${','.join([s.label for s in project.business_types])}</dd>
		% endif
		%if project.description:
			<dt>Description succinte :</dt> <dd>${project.description}</dd>
		% endif
		% if project.starting_date:
			<dt>Début prévu le :</dt><dd>${api.format_date(project.starting_date)}</dd>
		% endif
		% if project.ending_date:
			<dt>Livraison prévue le :</dt><dd>${api.format_date(project.ending_date)}</dd>
		% endif
	</dl>
% if project.definition:
    <h3>Définition du dossier</h3>
    <p>
        ${format_text(project.definition)|n}
    </p>
</div>
% endif
</div>
</%block>
