import Mn from 'backbone.marionette';
import ExpenseCollectionView from './ExpenseCollectionView.js';
import Radio from 'backbone.radio';
import { formatAmount } from '../../math.js';


const ExpenseTableView = Mn.View.extend({
    template: require('./templates/ExpenseTableView.mustache'),
    regions: {
       lines: {
           el: 'tbody',
           replaceElement: true
       }
    },
    ui:{
        add_btn: 'button.add',
    },
    triggers: {
        'click @ui.add_btn': 'line:add',
    },
    childViewTriggers: {
        'line:edit': 'line:edit',
        'line:delete': 'line:delete',
        'line:duplicate': 'line:duplicate',
        'bookmark:add': 'bookmark:add',
    },
    collectionEvents: {
        'change:category': 'render',
    },
    initialize(){
        var channel = Radio.channel('facade');
        this.totalmodel = channel.request('get:totalmodel');

        this.categoryId = this.getOption('category').value;
        this.listenTo(
            channel,
            'change:lines_' + this.categoryId,
            this.render.bind(this)
        );
    },
    includesTvaOnMargin() {
        let that = this;
        let result = this.collection.find(function(expense) {
            return expense.hasTvaOnMargin() && expense.get('category') == that.categoryId;
        });
        return result;
    },
    templateContext(){
        var is_achat = false;
        if(this.getOption('category').value==2) is_achat=true;

        return {
            category: this.getOption('category'),
            edit: this.getOption('edit'),
            is_achat: is_achat,
            includes_tva_on_margin: this.includesTvaOnMargin(),
            total_ht: formatAmount(this.totalmodel.get('ht_' + this.categoryId)),
            total_tva: formatAmount(this.totalmodel.get('tva_' + this.categoryId)),
            total_ttc: formatAmount(this.totalmodel.get('ttc_' +this.categoryId)),
        };
    },
    onRender(){
        var view = new ExpenseCollectionView(
            {
                collection: this.collection,
                category: this.getOption('category'),
                edit: this.getOption('edit'),
            }
        );
        this.showChildView('lines', view);
    },
});
export default ExpenseTableView;
