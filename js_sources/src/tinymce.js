var tinymce = require('tinymce/tinymce');
const setupTinyMce = function(kwargs){
    /*
     * Setup a tinymce editor
     * kwargs are tinymce init options to be added to the default ones
     *
     * Should at least provide the selector key
     *
     * https://www.tinymce.com/docs/
     */
    let PLUGINS = [
        "lists",
        "searchreplace",
        "visualblocks",
        "fullscreen",
        "paste",
    ];
    let THEME = 'modern';

    // Those requires are for embeding plugins/theme into built code.
    require('tinymce/themes/' + THEME + '/theme');
    PLUGINS.forEach(
        plugin =>
            require('tinymce/plugins/' + plugin + '/plugin')
    );

    tinyMCE.remove(kwargs['selector']);
    let options = {
      body_class: 'form-control',
      theme_advanced_toolbar_location: "top",
      theme_advanced_toolbar_align: "left",
      content_css: "/fanstatic/fanstatic/css/richtext.css",
      language: "fr_FR",
      language_url: "/fanstatic/fanstatic/js/build/tinymce-assets/langs/fr_FR.js",
      skin_url: "/fanstatic/fanstatic/js/build/tinymce-assets/skins/lightgray",
      plugins: PLUGINS,
      theme_advanced_resizing: true,
      height: "100px", width: 0,
      theme: THEME,
      strict_loading_mode: true,
      mode: "none",
      skin: "lightgray",
      menubar: false,
      convert_fonts_to_spans: true,
      paste_as_text: true,
    };
    _.extend(options, kwargs);
    tinymce.init(options);
}
export default setupTinyMce;
