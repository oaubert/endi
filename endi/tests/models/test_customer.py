
def test_customer_company_address(customer):
    assert customer.full_address == \
        """customer\nLastname Firstname\n1th street\n01234 City"""
    customer.country = "England"
    assert customer.full_address == \
        """customer\nLastname Firstname\n1th street\n01234 City\nEngland"""


def test_customer_individual_address(individual_customer):
    assert individual_customer.full_address == \
        """M. et Mme Lastname Firstname\n1th street\n01234 City"""


def test_label_company(customer):
    assert customer.label == customer.name


def test_label_individual(dbsession, individual_customer):
    assert individual_customer.label == """M. et Mme Lastname Firstname"""
    individual_customer.civilite = None
    dbsession.merge(individual_customer)
    dbsession.flush()
    assert individual_customer.label == """Lastname Firstname"""


def test_check_project_id(customer, project):
    from endi.models.third_party.customer import Customer
    assert Customer.check_project_id(customer.id, project.id)
    assert not Customer.check_project_id(customer.id, project.id + 1)
