freezegun>=1.0.0,<1.1
WebTest>=2.0.35,<2.1
Faker>=4.1.3,<5.0
pytest>=6.0.2,<7.0
