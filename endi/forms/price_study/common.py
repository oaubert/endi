import colander
from endi.models.tva import (
    Tva,
    Product,
)


@colander.deferred
def deferred_default_tva_id(node, kw):
    """
    Collect the default tva id
    """
    result = kw['request'].dbsession.query(Tva.id).filter_by(
        default=True).filter_by(
            active=True
        ).scalar()
    if result is None:
        result = kw['request'].dbsession.query(Tva.id).filter_by(
            active=True
        ).scalar()

    return result


@colander.deferred
def deferred_default_product_id(node, kw):
    """
    Collect the default product id
    """
    tva_id = deferred_default_tva_id(node, kw)

    result = kw['request'].dbsession.query(Product.id).filter_by(
        ).filter_by(
            active=True
        ).filter_by(
            tva_id=tva_id
        ).first()

    if result:
        result = result[0]

    return result
