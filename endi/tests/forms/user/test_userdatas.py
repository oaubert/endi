import pytest


def test_add_edit_schema(content, dbsession, pyramid_request):
    import colander
    from endi.forms.user.userdatas import get_add_edit_schema
    schema = get_add_edit_schema()
    schema.bind(request=pyramid_request)

    result = schema.deserialize(
        {
            'situation_situation_id': 1,
            'coordonnees_firstname': "firstname",
            "coordonnees_lastname": "lastname",
            "coordonnees_email1": "email1@email.fr",
        }
    )
    assert 'coordonnees_firstname' in result

    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                'situation_situation_id': 1,
                'coordonnees_firstname': "firstname",
                'coordonnees_lastname': "lastname",
            }
        )

    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                'situation_situation_id': 1,
                'coordonnees_lastname': "lastname",
                "coordonnees_email1": "email1@email.fr",
            }
        )

    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                'situation_situation_id': 1,
                'coordonnees_firstname': "firstname",
                "coordonnees_email1": "email1@email.fr",
            }
        )
