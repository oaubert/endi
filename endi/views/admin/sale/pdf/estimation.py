import os
from endi.forms.admin import get_config_schema
from endi.views.admin.tools import BaseConfigView
from endi.views.admin.sale.pdf import (
    PdfIndexView,
    PDF_URL,
)

ESTIMATION_ROUTE = os.path.join(PDF_URL, 'estimation')


class EstimationConfigView(BaseConfigView):
    title = "Informations spécifiques aux devis"
    description = "Configurer les champs spécifiques aux devis dans les \
sorties PDF"
    keys = ["coop_estimationheader",]
    schema = get_config_schema(keys)
    validation_msg = "Vos modifications ont été enregistrées"
    route_name = ESTIMATION_ROUTE


def includeme(config):
    config.add_route(ESTIMATION_ROUTE, ESTIMATION_ROUTE)
    config.add_admin_view(
        EstimationConfigView,
        parent=PdfIndexView,
    )
