<%namespace file="/base/utils.mako" name="utils" />
<div class="space_top content_vertical_padding">
    <span class='icon status ${indicator.status}'>
    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${status_icon}"></use></svg>
    </span>
    ${indicator.file_type.label} :

    % if indicator.file_id:
        <a href="#" onclick="openPopup('${request.route_path('/files/{id}', id=indicator.file_object.id)}')">
            ${indicator.file_object.name} (${api.human_readable_filesize(indicator.file_object.size)})
        </a>
        % if request.has_permission('valid.indicator', indicator):
            % if not indicator.validation_status == 'valid':
            <% validate_file_url =  request.route_path(force_route, id=indicator.id, _query={'action': 'validation_status', 'validation_status': 'valid'}) %>
            &nbsp;<em>Fichier en attente de validation</em>
            <%utils:post_action_btn url="${validate_file_url}" icon="check"
              _class='btn icon only'
              title="Valider le fichier fourni"
              aria_label="Valider le fichier fourni"
            >
            </%utils:post_action_btn>
            % else:
            <% validate_file_url =  request.route_path(force_route, id=indicator.id, _query={'action': 'validation_status', 'validation_status': 'invalid'}) %>
            <%utils:post_action_btn url="${validate_file_url}" icon="times"
              _class='btn icon only negative'
              title="Invalider le fichier fourni"
              aria_label="Invalider le fichier fourni"
            >
            </%utils:post_action_btn>
            % endif
        % endif
    % elif indicator.forced:
        <em>Cet indicateur a été forcé manuellement</em>
    % else:
        <em>Aucun fichier fourni</em>
    % endif
    <div class="content_vertical_padding layout flex">
    % if request.has_permission('add.file', indicator):
		<button
			class='btn icon'
			onclick="window.openPopup('${file_add_url}?file_type_id=${indicator.file_type_id}')"
			title="Ajouter un fichier"
			aria-label="Ajouter un fichier"
			>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#paperclip"></use></svg>
			Ajouter un fichier
		</button>
    % endif
    % if request.has_permission('force.indicator', indicator):
        <% force_indicator_url = request.route_path(force_route, id=indicator.id, _query={'action': 'force'}) %>
        % if not indicator.forced:
            <%utils:post_action_btn url="${force_indicator_url}" icon="bolt"
                _class="btn icon only negative"
                onclick="return confirm('Êtes-vous sûr de vouloir forcer cet indicateur (il apparaîtra désormais comme valide) ?');"
                title="Forcer cet indicateur"
                aria_label="Forcer cet indicateur"
            >
            </%utils:post_action_btn>
        % else:
            <%utils:post_action_btn url="${force_indicator_url}" icon="undo-alt"
                _class='btn icon only negative'
                title="Invalider cet indicateur"
                aria_label="Invalider cet indicateur"
            >
            </%utils:post_action_btn>
            % endif
    % endif
	</div>
</div>
