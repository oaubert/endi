<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    % if request.has_permission('add.invoice'):
    <a class='btn btn-primary' href='${add_url}'>
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-invoice-euro"></use></svg> Créer une facture
    </a>
    % else:
    ## So the group stays right in the page (flex layout)
    &nbsp;
    % endif
    <div role='group'>
        <%
        ## We build the link with the current search arguments
        args = request.GET
        url_xls = request.route_path('/projects/{id}/invoices.{extension}', extension='xls', id=request.context.id, _query=args)
        url_ods = request.route_path('/projects/{id}/invoices.{extension}', extension='ods', id=request.context.id, _query=args)
        url_csv = request.route_path('/projects/{id}/invoices.{extension}', extension='csv', id=request.context.id, _query=args)
        %>
        <a class='btn' onclick="window.openPopup('${url_xls}');" href='javascript:void(0);' title="Export au format Excel (xls)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg> Excel
        </a>
        <a class='btn' onclick="window.openPopup('${url_ods}');" href='javascript:void(0);' title="Export au format Open Document (ods)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-spreadsheet"></use></svg> ODS
        </a>
        <a class='btn' onclick="window.openPopup('${url_csv}');" href='javascript:void(0);' title="Export au format csv">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-csv"></use></svg> CSV
        </a>
    </div>
</div>
</%block>

<%block name='mainblock'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        ${request.layout_manager.render_panel('task_list', records, datatype="invoice", is_admin_view=is_admin, is_project_view=True)}
    </div>
    ${pager(records)}
</div>

</%block>
