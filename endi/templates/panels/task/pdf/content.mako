<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
    <div class='pdf_mention_block'>
        <h4 class="title ${css}">${title}</h4>
        <p class='content'>${format_text(datas)}</p>
    </div>
</%def>

${request.layout_manager.render_panel("task_pdf_header", context=task)}

% if task.status != "valid":
    <style>
    .task_view {
        background-position: 50% 0;
        background-repeat: no-repeat;
        background-size: contain;
    }
    % if task.type_ == 'estimation':
        .task_view {
            background-image: url("${request.static_url('endi:static/watermark_estimation.jpg', _app_url='')}");
        }
    % else:
        .task_view {
            background-image: url("${request.static_url('endi:static/watermark_invoice.jpg', _app_url='')}");
        }
    % endif
    </style>
% endif

<main class='task_view'>

    <div class="information_intro">
        Le ${api.format_date(task.date, False)},<br />
        <%block name='information'></%block>
        % if task.customer.tva_intracomm:
            <b>Numéro de TVA Intracommunautaire</b>&nbsp;: ${task.customer.tva_intracomm}<br />
        % endif
    </div>

    <div class='pdf_spacer'><br></div>

    <div class='row pdf_task_table'>
        % for group in groups:
            ${request.layout_manager.render_panel("task_pdf_task_line_group", context=task, group=group, display_tvas_column=multiple_tvas, first_column_colspan=first_column_colspan, column_count=column_count, show_progress_invoicing=show_progress_invoicing, is_tva_on_margin_mode=is_tva_on_margin_mode)}
        % endfor
        % if len(groups) > 1:
            </div>
            <div class='pdf_spacer'><br></div>
            <div class='pdf_task_table'>
                <table>
                    <tbody>
        % endif
        % if task.expenses_ht not in (0, None):
            <tr>
                <td class='col_text description' colspan='${first_column_colspan}'>
                    Frais forfaitaires
                </td>
                <td class="col_number price">
                    ${task.format_amount(task.expenses_ht, precision=5)}&nbsp;€
                </td>
                % if multiple_tvas and not is_tva_on_margin_mode:
                    <td class='col_number tva'>
                        ${task.format_amount(task.expenses_tva, precision=2)}&nbsp;%
                    </td>
                % endif
                % if task.display_ttc:
                    <td class="col_number price">&nbsp;</td>
                % endif
            </tr>
        % endif
        </tbody>
        <tbody>
            %if hasattr(task, "discounts") and task.discounts:
                % if not is_tva_on_margin_mode:
                    <tr class="row_total">
                        <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                            Total HT
                        </th>
                        <td class='col_number price_total'>
                            ${task.format_amount(task.groups_total_ht() + task.expenses_ht, trim=False, precision=5)}&nbsp;€
                        </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
                <% discounts_with_tva = False %>
                % for discount in task.discounts:
                    % if discount.tva > 0:
                        <% discounts_with_tva = True %>
                    % endif
                    <tr>
                        <td colspan='${first_column_colspan}' class='col_text description'>
                            ${format_text(discount.description)}
                        </td>
                        <td class='col_number price'>
                            ${task.format_amount(discount.amount, precision=5)}&nbsp;€
                        </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class='col_number tva'>
                                ${task.format_amount(discount.tva, precision=2)}&nbsp;%
                            </td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endfor
                % if discounts_with_tva:
                    <tr class="row_total">
                        <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                            % if is_tva_on_margin_mode:
                                Total après remise
                            % else:
                                Total HT après remise
                            % endif
                        </th>
                        <td class='col_number price_total'>
                            ${task.format_amount(task.total_ht(), precision=5)}&nbsp;€
                        </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
            % else:
                % if not is_tva_on_margin_mode:
                    <tr class="row_total">
                        <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                            Total HT
                        </th>
                        <td class='col_number price_total'>
                            ${task.format_amount(task.total_ht(), precision=5)}&nbsp;€
                        </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
            % endif
            <%doc>
            Si l'on a qu'un seul taux de TVA dans le document, on affiche
            une seule ligne avec du texte pour les tvas à 0%

            Pour les documents avec plusieurs taux de TVA, on affiche le montant par taux de tva
            </%doc>
            <% tva_objects_dict = task.get_tva_objects() %>
            % if not is_tva_on_margin_mode:
                %for tva, tva_amount in task.get_tvas().items():
                    <% tva_object = tva_objects_dict.get(tva) %>
                        <tr>
                            <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                                % if tva_object:
                                    % if tva_object.mention:
                                        ${format_text(tva_object.mention)}
                                    % else:
                                        ${tva_object.name}
                                    % endif
                                % else:
                                    TVA (${api.format_amount(tva, precision=2)} %)
                                % endif
                            </th>
                            <td class='col_number price'>
                                % if tva > 0:
                                    ${task.format_amount(tva_amount, precision=5)}&nbsp;€
                                % else:
                                    0,00&nbsp;€
                                % endif
                            </td>
                            % if multiple_tvas:
                                <td class="col_number tva">&nbsp;</td>
                            % endif
                            % if task.display_ttc:
                                <td class="col_number price">&nbsp;</td>
                            % endif
                        </tr>
                % endfor
            % endif
            %if task.expenses:
                <tr>
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                        Frais réels
                    </th>
                    <td class='col_number price'>
                        ${task.format_amount(task.expenses_amount(), precision=5)}&nbsp;€
                    </td>
                    % if multiple_tvas and not is_tva_on_margin_mode:
                        <td class="col_number tva">&nbsp;</td>
                    % endif
                    % if task.display_ttc:
                        <td class="col_number price">&nbsp;</td>
                    % endif
                </tr>
            %endif
            <tr class="row_total">
                <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                    % if is_tva_on_margin_mode:
                        Total
                    % else:
                        Total TTC
                    % endif
                </th>
                <td class='col_number price_total'>
                    ${task.format_amount(task.total(), precision=5)}&nbsp;€
                </td>
                % if multiple_tvas and not is_tva_on_margin_mode:
                    <td class="col_number tva">&nbsp;</td>
                % endif
                % if task.display_ttc:
                    <td class="col_number price">&nbsp;</td>
                % endif
            </tr>
        </tbody>
        </table>
    </div>

    <div class='pdf_spacer'><br></div>

    %if task.notes:
        ${table("Notes", task.notes)}
    %endif

    <%block name="notes_and_conditions">
    ## All infos beetween document lines and footer text (notes, payment conditions ...)
    </%block>

    % for mentions in (task.mandatory_mentions, task.mentions):
        % for mention in mentions:
            <div class="pdf_mention_block">
                % if mention.title:
                    <h4>${mention.title}</h4>
                    % if mention.full_text is not None:
                        <p>${format_text(api.compile_template_str(mention.full_text, mention_tmpl_context))}</p>
                    % endif
                % else:
                    % if mention.full_text is not None:
                        <p>${format_text(api.compile_template_str(mention.full_text, mention_tmpl_context))}</p>
                    % endif
                % endif
            </div>
        % endfor
    % endfor

    <%block name="end_document">
    ## Add infos at the end of the document
    </%block>

</main>

% if with_cgv:
${request.layout_manager.render_panel('task_pdf_cgv', context=task)}
% endif
