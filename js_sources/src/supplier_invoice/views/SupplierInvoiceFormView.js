import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import CheckboxListWidget from '../../widgets/CheckboxListWidget.js';
import Radio from 'backbone.radio';


const SupplierInvoiceFormView = Mn.View.extend({
    tagName: 'div',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierInvoiceFormView.mustache'),
    regions: {
        date: '.date',
        name: '.name',
        supplier_orders: '.supplier-orders'
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'order:modified',
        'finish': 'data:persist',
    },
    childViewEvents: {
        'finish': 'onChange',
    },

    initialize(){
        var channel = Radio.channel('config');
        channel.trigger();
        this.orders_options = channel.request(
            'get:options',
            'suppliers_orders'
        );
    },

    onChange(name, value){
        this.model.set(name, value);
    },

    onRender(){
        var view;
        let editable = this.getOption('edit');

        view = new DatePickerWidget({
            value: this.model.get('date'),
            title: 'Date',
            description: 'Date de la facture',
            field_name: 'date',
            editable: editable,
        });
        this.showChildView('date', view);

        view = new InputWidget({
            value: this.model.get('name'),
            title: 'Nom',
            field_name: 'name',
            editable: editable,
        });
        this.showChildView('name', view);

        view = new CheckboxListWidget({
            value: this.model.get('supplier_orders'),
            title: 'Commandes fournisseur associées',
            field_name: 'supplier_orders',
            editable: editable,
            options: this.orders_options,
            togglable: true,
            multiple: true,
            id_key: 'id',
            optionFilter: function(option, currentOption) {
                // Keep only the orders from same supplier
                return (
                    (currentOption == undefined)
                    ||
                    (option.supplier_id == currentOption.supplier_id)
                );
            },
            removeItemConfirmationMsg: "Si vous rompez le lien vers cette "
                + "commande, toutes les lignes associées seront retirées de la"
                +" présente facture fournisseur.",
        });
        this.showChildView('supplier_orders', view);
    },

    onSuccessSync(){
        let facade = Radio.channel('facade');
        facade.trigger('navigate', 'index');
    },

});
export default SupplierInvoiceFormView;
