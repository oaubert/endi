import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';

var template = require("./templates/NotesBlockView.mustache");


const NotesBlockView = Mn.View.extend({
    tagName: 'div',
    className: 'separate_block border_left_block',
    template: template,
    regions: {
        notes: {
            el: '.notes',
            replaceElement: true
        }
    },
    behaviors: [
        {
            behaviorClass: FormBehavior,
            errorMessage: "Vérifiez votre saisie"
        }
    ],
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    isMoreSet: function(){
        if (this.model.get('notes')){
            return true;
        }
        return false;
    },
    templateContext: function(){
        return {
            is_more_set: this.isMoreSet(),
        }
    },
    onRender: function(){
        const view = new TextAreaWidget(
            {
                title: 'Notes',
                description: 'Notes complémentaires',
                field_name: 'notes',
                value: this.model.get('notes'),
            }
        );
        this.showChildView('notes', view);
    }
});
export default NotesBlockView;
