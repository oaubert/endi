<%inherit file="/tasks/view_only.mako" />
<%namespace file="/base/utils.mako" name="utils" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <% invoice = request.context %>
	<div role="group">
        % if api.has_permission('add_payment.invoice'):
            <a class='btn icon_only_mobile' title="Enregistrer un encaissement pour cette facture" aria-label="Enregistrer un encaissement pour cette facture" href="${request.route_path('/invoices/{id}/addpayment', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#euro-circle"></use></svg>
                Encaisser
            </a>
        % endif
        % if api.has_permission('gencinv.invoice'):
            <% gencinv_url = request.route_path('/invoices/{id}/gencinv', id=invoice.id) %>
            <%utils:post_action_btn url="${gencinv_url}" icon="plus-circle" _class="btn icon_only_mobile" title="Générer un avoir pour cette facture" aria-label="Générer un avoir pour cette facture">
                Avoir
            </%utils:post_action_btn>
        % endif
        % if api.has_permission('set_treasury.invoice'):
            <a class='btn' title="Configurer les codes produits de cette facture" aria-label="Configurer les codes produits de cette facture" href="${request.route_path('/invoices/{id}/set_products', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#cog"></use></svg>
                Codes<span class="no_mobile"> produits</span>
            </a>
        % endif
        % if not invoice.estimation and invoice.invoicing_mode == 'classic':
            <a class='btn' title="Rattacher cette facture à un devis" aria-label="Rattacher cette facture à un devis" href="${request.route_path('/invoices/{id}/attach_estimation', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#link"></use></svg>
                Rattacher à un devis
            </a>
        % endif
	</div>
	<div role="group">
        % if api.has_permission('draft.invoice'):
            <% set_draft_url = request.route_path('/invoices/{id}/set_draft', id=invoice.id) %>
            <%utils:post_action_btn url="${set_draft_url}" icon="pen-square" _class="btn icon only" title="Repasser cette facture en brouillon" aria-label="Repasser cette facture en brouillon">
            </%utils:post_action_btn>
        % endif
        <a class='btn icon only' title="Modifier cette facture" aria-label="Modifier cette facture" href="${request.route_path('/invoices/{id}/set_metadatas', id=invoice.id)}">
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
        </a>
        % if api.has_permission('duplicate.invoice'):
            <a class='btn icon only' title="Dupliquer cette facture" aria-label="Dupliquer cette facture" href="${request.route_path('/invoices/{id}/duplicate', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#copy"></use></svg>
            </a>
        % endif
        % if invoice.price_study_id is not None:
            <a class='btn' title="Voir l’étude de prix à l’origine de ce devis" href="${request.route_path('/price_studies/{id}', id=invoice.price_study_id)}">
             <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#calculator"></use></svg> Voir l’étude de prix
            </a>
        % endif
        % if hasattr(next, 'before_actions'):
            ${next.before_actions()}
        % endif
        <% href_pdf = request.route_path('/%ss/{id}.pdf' % request.context.type_, id=request.context.id) %>
        <a class='btn btn-primary icon only' title="Voir le PDF de cette facture" aria-label="Voir le PDF de cette facture" href="${href_pdf}">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>
        </a>
        % if hasattr(next, 'moreactions'):
            ${next.moreactions()}
        % endif
	</div>
</div>
</%block>

<%block name='panel_heading'>
    <% invoice = request.context %>
    % if invoice.official_number:
        ${api.overridable_label('invoice')} N<span class="screen-reader-text">umér</span><sup>o</sup> ${invoice.official_number} (${invoice.name})
    % else:
        <em>${invoice.name}</em>
    % endif
</%block>

<%block name='moretabs'>
    <% invoice = request.context %>
    <li role="presentation">
        <a href="#treasury" aria-control="treasury" role='tab' data-toggle='tab'>Comptabilité</a>
    </li>
    <li role="presentation">
        <a href="#payment" aria-control="payment" role='tab' data-toggle='tab'>Encaissements</a>
    </li>
</%block>

<%block name='before_summary'>
    <% invoice = request.context %>
    % if invoice.estimation:
    <div class="separate_bottom content_vertical_padding">
        <h4>
            Devis de référence :
            <a href="${request.route_path('/estimations/{id}.html', id=invoice.estimation.id)}">
                ${invoice.estimation.internal_number}
            </a>
			<a href="${request.route_path('/invoices/{id}/attach_estimation', id=invoice.id)}" class="btn icon only unstyled">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#link"></use></svg>
			</a>
        </h4>
    </div>
    % endif
    % if invoice.cancelinvoices:
    <div class="separate_bottom">
        <ul>
            % for cancelinvoice in invoice.cancelinvoices:
                <li>
					L’avoir (${api.format_cancelinvoice_status(cancelinvoice, full=False)}): \
					<a href="${request.route_path('/cancelinvoices/{id}.html', id=cancelinvoice.id)}">
						${cancelinvoice.internal_number}
						% if cancelinvoice.official_number:
							&nbsp;<small>( ${cancelinvoice.official_number} )</small>
						% endif
					</a> a été généré depuis cette facture.
                </li>
            % endfor
        </ul>
    </div>
    % endif
</%block>

<%block name='moretabs_datas'>
    <% invoice = request.context %>
    <div role="tabpanel" class="tab-pane row" id="treasury">
        <div>
            <div class='alert'>
                Cette facture est rattachée à l’année fiscale ${invoice.financial_year}.
                % if api.has_permission('set_treasury.invoice'):
                    <a class='btn btn-primary' href="${request.route_path('/invoices/{id}/set_treasury', id=invoice.id)}">
                    	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                        Modifier
                    </a>
                % endif
                <br />
                Elle porte le numéro ${invoice.official_number}.
            </div>
            <% url = request.route_path('/export/treasury/invoices/{id}', id=invoice.id, _query={'force': True}) %>
            % if invoice.exported:
                <div class='content_vertical_padding'>
                    <span class="icon status valid"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg></span>
                    Cette facture a été exportée vers la comptabilité
                </div>
                % if api.has_permission('admin_treasury'):
                <div class='content_vertical_padding'>
                    <a class='btn' href="${url}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                        Forcer la génération d’écritures pour cette facture
                    </a>
                </div>
                % endif
            % else:
                <div class='content_vertical_padding'>
                    <span class="icon status neutral"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg></span>
                    Cette facture n'a pas encore été exportée vers la comptabilité
                </div>
                % if api.has_permission('admin_treasury'):
                <div class='content_vertical_padding'>
                    <a class='btn btn-primary' href="${url}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                    	Générer les écritures pour cette facture
                    </a>
                </div>
                % endif
            % endif
        </div>
    </div>
    <div role="tabpanel" class="tab-pane row" id="payment">
        % if api.has_permission('add_payment.invoice'):
        <div class="content_vertical_padding">
            <a class='btn btn-primary' href="${request.route_path('/invoices/{id}/addpayment', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus-circle"></use></svg>
                Enregistrer un encaissement
            </a>
        </div>
        <div class="content_vertical_padding separate_top">
        % else:
        <div class="content_vertical_padding">
        % endif
            <h3>Liste des encaissements</h3>
            % if invoice.payments:
                <% total_payments = 0 %>
                <div class="table_container">
                    <table class="hover_table">
                        <thead>
                            <th scope="col" class="col_date">Date</th>
                            <th scope="col" class="col_text">Mode de règlement</th>
                            <th scope="col" class="col_text">TVA</th>
                            <th scope="col" class="col_text">N° de remise</th>
                            <th scope="col" class="col_number">Montant</th>
                            <th scope="col" class="col_actions width_one" title="Actions"><span class="screen-reader-text">Actions</span></th>
                        </thead>
                        <tbody>
                            % for payment in invoice.payments:
                                <% url = request.route_path('payment', id=payment.id) %>
                                <tr>
                                    <td class="col_date">${api.format_date(payment.date)}</td>
                                    <td class="col_text">${api.format_paymentmode(payment.mode)}</td>
                                    <td class="col_text">
                                        % if payment.tva is not None:
                                            ${payment.tva.name}
                                        % endif
                                    </td>
                                    <td class="col_text">${payment.bank_remittance_id}</td>
                                    <td class="col_number">${api.format_amount(payment.amount, precision=5)}&nbsp;€</td>
                                    <td class="col_actions width_one">
                                        <a href="${url}" class="btn icon only" title="Voir/Modifier cet encaissement">
                                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#eye"></use></svg> Voir
                                        </a>
                                    </td>
                                </tr>
                                <% total_payments += payment.amount %>
                            % endfor
                        </tbody>
                        <tfoot>
                            <tr class="row_recap">
                                <th scope="col" class="col_text" colspan="4">Total encaissé</th>
                                <th scope="col" class="col_number">${api.format_amount(total_payments, precision=5)}&nbsp;€</th>
                                <th scope="col" class="col_actions width_one">&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            % else:
                <em>Aucun encaissement n’a été saisi</em>
            % endif
        </div>
        <div class="content_vertical_padding separate_top">
            <h3>Avoir(s)</h3>
            % if invoice.cancelinvoices:
                <% hasone = False %>
                <ul>
                    % for  cancelinvoice in invoice.cancelinvoices:
                        % if cancelinvoice.status == 'valid':
                            <% hasone = True %>
                            <li>
								L’avoir : \
								<a href="${request.route_path('/cancelinvoices/{id}.html', id=cancelinvoice.id)}">
									${cancelinvoice.internal_number}
									(numéro ${cancelinvoice.official_number})
									d’un montant TTC de ${api.format_amount(cancelinvoice.ttc, precision=5)} €
								</a> a été généré depuis cette facture.
                            </li>
                        % endif
                    % endfor
                </ul>
                % if not hasone:
                    <em>Aucun avoir validé n’est associé à ce document</em>
                % endif
            % else:
                <em>Aucun avoir validé n’est associé à ce document</em>
            % endif
        </div>
    </div>
</%block>
