"""
    Task model
    represents a base task, with a status, an owner, a phase
"""
import logging
import colander
import deform
import datetime

from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    String,
    ForeignKey,
    Text,
    Boolean,
    Float,
    JSON,
    Date,
    desc,
)
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.event import listen

from sqlalchemy.orm import (
    relationship,
    validates,
    deferred,
    backref,
)
from sqlalchemy.ext.orderinglist import ordering_list

from endi_base.models.base import (
    DBBASE,
    default_table_args,
    DBSESSION,
)

from endi.utils.strings import (
    format_status_string,
)
from endi.compute.task import (
    TaskCompute,
    LineCompute,
    GroupCompute,
    TaskTtcCompute,
    DiscountLineTtcCompute,
    DiscountLineCompute,
    GroupTtcCompute,
    LineTtcCompute,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi.models.node import Node
from endi.models.project.business import Business
from endi.models.services.naming import NamingService
from endi.models.services.task_mentions import (
    TaskMentionService,
)
from endi.models.services.sale_file_requirements import (
    TaskFileRequirementService,
)
from endi.models.services.task import (
    TaskService,
    TaskLineGroupService,
    TaskLineService,
    DiscountLineService,
)
from endi.models.task.mentions import (
    MANDATORY_TASK_MENTION,
    TASK_MENTION,
)


logger = log = logging.getLogger(__name__)

ALL_STATES = ('draft', 'wait', 'valid', 'invalid')
# , 'geninv', 'aboest', 'gencinv', 'resulted', 'paid', )


class FrozenSettingsModelMixin:
    """ Allows to store/retrieve a frozen settings dict/list as JSON

    Could be used for something else than Task
    """
    def frozen_settings_initialize(self, frozen_settings) -> dict:
        raise NotImplementedError

    def freeze_settings(self):
        self.frozen_settings = self.frozen_settings_initialize()

    # attr should not be included in duplicate() to allow regeneration of
    # frozen_settings on
    frozen_settings = Column(
        JSON,
        default={},
        info={
            'export': {'exclude': True},
            'colanderalchemy': {'exclude': True},
        }
    )


class Task(FrozenSettingsModelMixin, Node):
    """
        Metadata pour une tâche (estimation, invoice)
    """
    __tablename__ = 'task'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'task'}
    _endi_service = TaskService
    file_requirement_service = TaskFileRequirementService
    mention_service = TaskMentionService
    naming_service = NamingService

    id = Column(
        Integer,
        ForeignKey('node.id'),
        info={'export': {'exclude': True}},
        primary_key=True,
    )
    phase_id = Column(
        ForeignKey('phase.id'),
        info={"export": {'exclude': True}},
    )
    status = Column(
        String(10),
        info={
            'colanderalchemy': {'title': "Statut"},
            'export': {'exclude': True}
        }
    )
    status_comment = Column(
        Text,
        info={
            "colanderalchemy": {"title": "Commentaires"},
            'export': {'exclude': True}
        },
        default="",
    )
    status_person_id = Column(
        ForeignKey('accounts.id', ondelete='SET NULL'),
        info={
            'colanderalchemy': {
                "title": "Dernier utilisateur à avoir modifié le document",
            },
            "export": {'exclude': True},
        },
    )
    status_date = Column(
        DATETIME(fsp=6),
        default=datetime.date.today,
        info={
            'colanderalchemy': {
                "title": "Date du dernier changement de statut",
            },
            'export': {'exclude': True}
        }
    )
    date = Column(
        Date(),
        info={"colanderalchemy": {"title": "Date du document"}},
        default=datetime.date.today
    )
    owner_id = Column(
        ForeignKey('accounts.id'),
        info={
            "export": {'exclude': True},
        },
    )
    description = Column(
        Text,
        info={'colanderalchemy': {"title": "Objet"}},
    )
    # ttc or ht compute base
    mode = Column(
        String(10),
        info={
            'colanderalchemy': {'title': "Mode de saisie"},
            'export': {'exclude': True}
        },
        default='ht'
    )
    ht = Column(
        BigInteger(),
        info={
            'colanderalchemy': {"title": "Montant HT (cache)"},
            'export': {'exclude': True},
        },
        default=0
    )
    tva = Column(
        BigInteger(),
        info={
            'colanderalchemy': {"title": "Montant TVA (cache)"},
            'export': {'exclude': True},
        },
        default=0
    )
    ttc = Column(
        BigInteger(),
        info={
            'colanderalchemy': {"title": "Montant TTC (cache)"},
            'export': {'exclude': True},
        },
        default=0
    )
    company_id = Column(
        Integer,
        ForeignKey('company.id'),
        info={
            'export': {'exclude': True},
        },
    )
    project_id = Column(
        Integer,
        ForeignKey('project.id'),
        nullable=False,
        info={
            'export': {'exclude': True},
        },
    )
    customer_id = Column(
        Integer,
        ForeignKey('customer.id'),
        info={
            'export': {'exclude': True},
        },
    )
    project_index = deferred(
        Column(
            Integer,
            info={
                'colanderalchemy': {
                    "title": "Index dans le dossier",
                },
                'export': {'exclude': True},
            },
        ),
        group='edit',
    )
    company_index = deferred(
        Column(
            Integer,
            info={
                'colanderalchemy': {
                    "title": "Index du document à l'échelle de l'enseigne",
                },
                'export': {'exclude': True},
            },
        ),
        group='edit',
    )
    official_number = Column(
        String(255),
        info={
            'colanderalchemy': {
                "title": "Identifiant du document (facture/avoir)",
            },
            'export': {'label': "Numéro de facture"},
        },
        default=None,
    )
    legacy_number = Column(
        Boolean,
        default=False,
        nullable=False,
        info={
            'export': {'exclude': True},
        },
    )
    internal_number = deferred(
        Column(
            String(255),
            default=None,
            info={
                'colanderalchemy': {
                    "title": "Identifiant du document dans la CAE",
                },
                'export': {'exclude': True},
            }
        ),
        group='edit'
    )
    display_units = deferred(
        Column(
            Integer,
            info={
                'colanderalchemy': {
                    "title": "Afficher le détail ?",
                    "validator": colander.OneOf((0, 1))
                },
                'export': {'exclude': True},
            },
            default=0
        ),
        group='edit'
    )
    display_ttc = deferred(
        Column(
            Integer,
            info={
                'colanderalchemy': {
                    "title": "Afficher les prix TTC ?",
                    "validator": colander.OneOf((0, 1))
                },
                'export': {'exclude': True},
            },
            default=0
        ),
        group='edit'
    )
    expenses_ht = deferred(
        Column(
            BigInteger(),
            info={
                'colanderalchemy': {'title': 'Frais'},
                'export': {'exclude': True},
            },
            default=0
        ),
        group='edit',
    )
    address = deferred(
        Column(
            Text,
            default="",
            info={
                'colanderalchemy': {'title': 'Adresse'},
                'export': {'exclude': True},
            },
        ),
        group='edit',
    )
    workplace = deferred(
        Column(
            Text,
            default='',
            info={
                'colanderalchemy': {'title': "Lieu d'éxécution"},
            }
        )
    )
    payment_conditions = deferred(
        Column(
            Text,
            info={
                'colanderalchemy': {
                    "title": "Conditions de paiement",
                },
                'export': {'exclude': True},
            },
        ),
        group='edit',
    )
    notes = deferred(
        Column(
            Text,
            default="",
            info={
                'colanderalchemy': {'title': 'Notes complémentaires'},
                'export': {'exclude': True},
            },
        ),
        group='edit',
    )
    round_floor = deferred(
        Column(
            Boolean(),
            default=False,
            info={
                'colanderalchemy': {
                    'exlude': True,
                    'title': "Méthode d'arrondi 'à l'ancienne' ? (floor)"
                },
                'export': {'exclude': True},
            }
        ),
        group='edit',
    )
    # Nombre de décimal à afficher dans les documents
    decimal_to_display = deferred(Column(Integer, default=2), group="edit")
    business_type_id = Column(ForeignKey("business_type.id"))
    business_id = Column(
        ForeignKey("business.id"),
        info={'colanderalchemy': {'exclude': True}},
    )
    pdf_file_id = deferred(
        Column(
            ForeignKey("file.id"),
            info={'colanderalchemy': {'exclude': True}}
        ),
        group='edit',
    )
    pdf_file_hash = deferred(
        Column(
            String(40),
            nullable=True
        ),
        group='edit',
    )
    price_study_id = Column(
        ForeignKey("price_study.id"),
        info={'colanderalchemy': {'exclude': True}},
    )
    start_date = Column(
        Date(),
        info={"colanderalchemy": {"title": "Date de début des prestations"}},
        nullable=True
    )

    # Organisationnal Relationships
    status_person = relationship(
        "User",
        primaryjoin="Task.status_person_id==User.id",
        backref=backref(
            "taskStatuses",
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        ),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    owner = relationship(
        "User",
        primaryjoin="Task.owner_id==User.id",
        backref=backref(
            "ownedTasks",
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        ),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    phase = relationship(
        "Phase",
        primaryjoin="Task.phase_id==Phase.id",
        backref=backref(
            "tasks",
            order_by='Task.date',
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        ),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    company = relationship(
        "Company",
        primaryjoin="Task.company_id==Company.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'related_key': "name", "label": "Enseigne"},
        },
    )
    project = relationship(
        "Project",
        primaryjoin="Task.project_id==Project.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    customer = relationship(
        "Customer",
        primaryjoin="Customer.id==Task.customer_id",
        backref=backref(
            'tasks',
            order_by='Task.date',
            info={
                'colanderalchemy': {'exclude': True},
                "export": {'exclude': True},
            },
        ),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'related_key': 'label', 'label': "Client"},
        },
    )
    business_type = relationship(
        "BusinessType",
        info={'colanderalchemy': {'exclude': True}}
    )
    business = relationship(
        "Business",
        primaryjoin="Business.id==Task.business_id",
        info={'colanderalchemy': {'exclude': True}}
    )
    price_study = relationship(
        "PriceStudy",
        primaryjoin="PriceStudy.id==Task.price_study_id",
        info={'colanderalchemy': {'exclude': True}}
    )

    # Content relationships
    discounts = relationship(
        "DiscountLine",
        info={
            'colanderalchemy': {'title': "Remises"},
            'export': {'exclude': True},
        },
        order_by='DiscountLine.tva',
        cascade="all, delete-orphan",
        back_populates='task',
    )
    payments = relationship(
        "Payment",
        primaryjoin="Task.id==Payment.task_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
        order_by='Payment.date',
        cascade="all, delete-orphan",
        back_populates='task',
    )
    mentions = relationship(
        "TaskMention",
        secondary=TASK_MENTION,
        order_by="TaskMention.order",
        info={'export': {'exclude': True}},
    )
    mandatory_mentions = relationship(
        "TaskMention",
        secondary=MANDATORY_TASK_MENTION,
        order_by="TaskMention.order",
        info={'export': {'exclude': True}},
    )
    line_groups = relationship(
        "TaskLineGroup",
        order_by='TaskLineGroup.order',
        cascade="all, delete-orphan",
        collection_class=ordering_list('order'),
        info={
            'colanderalchemy': {
                'title': "Unités d'oeuvre",
                "validator": colander.Length(
                    min=1,
                    min_err="Une entrée est requise"
                ),
                "missing": colander.required
            },
            'export': {'exclude': True},
        },
        primaryjoin="TaskLineGroup.task_id==Task.id",
        back_populates='task',
    )
    statuses = relationship(
        "TaskStatus",
        order_by="desc(TaskStatus.status_date), desc(TaskStatus.id)",
        cascade="all, delete-orphan",
        back_populates='task',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )
    pdf_file = relationship(
        "File",
        primaryjoin="Task.pdf_file_id==File.id",
        cascade="all, delete",
        backref="associated_cached_task",
        info={'colanderalchemy': {'exclude': True}}
    )

    # Not used in latest invoices
    expenses = deferred(
        Column(
            BigInteger(),
            info={
                'export': {'exclude': True},
            },
            default=0
        ),
        group='edit'
    )

    @property
    def _name_tmpl(self):
        if self.type_ == 'task':
            # Does not happen in real life ; but might in pytests
            return "Task {0}"
        else:
            return f"{self.get_type_label()} {{0}}"

    _number_tmpl = "{s.company.name} {s.date:%Y-%m} F{s.company_index}"

    validation_state_manager = None
    computer = None

    def __init__(self, user, company, **kw):
        project = kw['project']
        company_index = self._get_company_index(company)
        project_index = self._get_project_index(project)

        self.status = 'draft'
        self.company = company
        self.decimal_to_display = company.decimal_to_display
        if 'customer' in kw:
            customer = kw['customer']
            self.address = customer.full_address
        self.owner = user
        self.status_person = user
        self.date = datetime.date.today()
        self.mode = project.mode
        if self.mode == 'ttc':
            self.display_ttc = 1

        # Initialize parts of the kw that may be required for label overrides
        for key, value in list(kw.items()):
            if key not in ('date', 'name'):
                setattr(self, key, value)

        self.set_numbers(company_index, project_index)

        # Initialize part of the kw that must come *after* set_numbers

        # set_numbers expect the current date, not the date arg.
        # This is arguable, but better keep historic behavior…
        if 'date' in kw:
            self.date = kw['date']

        # Allows name overriding, even if set_numbers() set one
        if 'name' in kw:
            self.name = kw['name']

        # We add a default task line group
        self.line_groups.append(TaskLineGroup(order=0))

    def frozen_settings_initialize(self):
        return dict(
            label_overrides=self.naming_service.
            get_labels_for_business_type_id(
                self.business_type_id
            )
        )

    def initialize_business_datas(self, business=None):
        """
        Initialize the business datas related to this task

        :param obj business: instance of
        :class:`endi.models.project.business.Business`
        """
        if business is not None:
            self.business = business

        self.file_requirement_service.populate(self)
        self.mention_service.populate(self)

    def _get_project_index(self, project):
        """
        Return the index of the current object in the associated project
        :param obj project: A Project instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return -1

    def _get_company_index(self, company):
        """
        Return the index of the current object in the associated company
        :param obj company: A Company instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return -1

    def set_numbers(self, company_index, project_index):
        """
        Handle all attributes related to the given number

        :param int company_index: The index of the task in the company
        :param int project_index: The index of the task in its project
        """
        if company_index is None or project_index is None:
            raise Exception("Indexes should not be None")

        self.company_index = company_index
        self.project_index = project_index

        self.internal_number = self._number_tmpl.format(s=self)
        self.name = self._name_tmpl.format(project_index)

    def get_type_label(self):
        return self.naming_service.get_label_for_context(self.type_, self)

    @property
    def default_line_group(self):
        return self.line_groups[0]

    def has_price_study(self):
        return self._endi_service.has_price_study(self)

    def get_price_study(self):
        return self._endi_service.get_price_study(self)

    def __json__(self, request):
        """
        Return the datas used by the json renderer to represent this task
        """
        if self.start_date:
            task_start_date = self.start_date.isoformat()
        else:
            task_start_date = None
        return dict(
            id=self.id,
            name=self.name,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),
            phase_id=self.phase_id,
            business_type_id=self.business_type_id,
            price_study_id=self.price_study_id,
            status=self.status,
            status_comment=self.status_comment,
            status_person_id=self.status_person_id,
            date=self.date.isoformat(),
            owner_id=self.owner_id,
            description=self.description,
            mode=self.mode,
            ht=integer_to_amount(self.ht, 5),
            tva=integer_to_amount(self.tva, 5),
            ttc=integer_to_amount(self.ttc, 5),
            company_id=self.company_id,
            project_id=self.project_id,
            customer_id=self.customer_id,
            project_index=self.project_index,
            company_index=self.company_index,
            official_number=self.official_number,
            internal_number=self.internal_number,
            display_units=self.display_units,
            display_ttc=self.display_ttc,
            expenses_ht=integer_to_amount(self.expenses_ht, 5),
            address=self.address,
            workplace=self.workplace,
            payment_conditions=self.payment_conditions,
            notes=self.notes,
            start_date=task_start_date,
            status_history=[
                status.__json__(request) for status in self.statuses
            ],
            discounts=[
                discount.__json__(request) for discount in self.discounts
            ],
            payments=[
                payment.__json__(request) for payment in self.payments
            ],
            mentions=[mention.id for mention in self.mentions],
            line_groups=[
                group.__json__(request) for group in self.line_groups
            ],
            attachments=[
                file_.__json__(request) for file_ in self.files
            ],
            file_requirements=[
                file_req.__json__(request)
                for file_req in self.file_requirements
            ]
        )

    def set_status(self, status, request, **kw):
        """
        set the status of a task through the state machine
        """
        return self.validation_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def check_status_allowed(self, status, request, **kw):
        return self.validation_state_manager.check_allowed(
            status,
            self,
            request
        )

    @validates('status')
    def change_status(self, key, status):
        """
        fired on status change, barely logs what is happening
        """
        logger.debug("# Task status change #")
        current_status = self.status
        logger.debug(" + was {0}, becomes {1}".format(current_status, status))
        return status

    def get_company(self):
        """
            Return the company owning this task
        """
        return self.company

    def get_customer(self):
        """
            Return the customer of the current task
        """
        return self.customer

    def get_company_id(self):
        """
            Return the id of the company owning this task
        """
        return self.company.id

    def __repr__(self):
        return "<Task status:{s.status} id:{s.id}>".format(s=self)

    def get_groups(self):
        return [group for group in self.line_groups if group.lines]

    @property
    def all_lines(self):
        """
        Returns a list with all task lines of the current task
        """
        result = []
        for group in self.line_groups:
            result.extend(group.lines)
        return result

    def get_tva_objects(self):
        return self._endi_service.get_tva_objects(self)

    @classmethod
    def get_valid_tasks(cls, *args):
        return cls._endi_service.get_valid_tasks(cls, *args)

    @classmethod
    def get_waiting_estimations(cls, *args):
        return cls._endi_service.get_waiting_estimations(*args)

    @classmethod
    def get_waiting_invoices(cls, *args):
        return cls._endi_service.get_waiting_invoices(cls, *args)

    def gen_business(self):
        """
        Generate a business based on this Task

        :returns: A new business instance
        :rtype: :class:`endi.models.project.business.Business`
        """
        business = Business(
            name=self.name,
            project=self.project,
            business_type_id=self.business_type_id,
        )
        DBSESSION().add(business)
        DBSESSION().flush()
        business.populate_indicators()
        logger.debug("Business has id {}".format(business.id))
        business.file_requirement_service.populate(business)
        self.business_id = business.id
        DBSESSION().merge(self)
        return business

    def is_training(self):
        return self.business_type and self.business_type.name == 'training'

    @classmethod
    def from_price_study(cls, price_study, user, customer, **kwargs):
        return cls._endi_service.from_price_study(
            cls, price_study, user, customer, **kwargs
        )

    def sync_with_price_study(self):
        return self._endi_service.sync_with_price_study(self, self.price_study)

    @classmethod
    def find_task_status_date(cls, official_number, year):
        return cls._endi_service.find_task_status_date(
            cls, official_number, year
        )

    def get_main_sequence_number(self):
        from endi.models.task.sequence_number import SequenceNumber
        sequence_number = SequenceNumber.query().filter(
            SequenceNumber.task_id == self.id
        ).order_by(desc(SequenceNumber.index)).first()
        return sequence_number.index

    def _get_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of TaskCompute or TaskTtcCompute
        """
        if self.computer is None:
            if self.mode == 'ttc':
                self.computer = TaskTtcCompute(self)
            else:
                self.computer = TaskCompute(self)
        return self.computer

    def floor(self, amount):
        return self._get_computer().floor(amount=amount)

    def total_ht(self):
        return self._get_computer().total_ht()

    def get_tvas(self):
        return self._get_computer().get_tvas()

    def get_tvas_by_product(self):
        return self._get_computer().get_tvas_by_product()

    def tva_amount(self):
        return self._get_computer().tva_amount()

    def no_tva(self):
        return self._get_computer().no_tva()

    def total_ttc(self):
        return self._get_computer().total_ttc()

    def total(self):
        return self._get_computer().total()

    def expenses_amount(self):
        return self._get_computer().expenses_amount()

    def get_expense_ht(self):
        return self._get_computer().get_expense_ht()

    def tva_ht_parts(self):
        return self._get_computer().tva_ht_parts()

    def tva_ttc_parts(self):
        return self._get_computer().tva_ttc_parts()

    def groups_total_ht(self):
        return self._get_computer().groups_total_ht()

    def discount_total_ht(self):
        return self._get_computer().discount_total_ht()

    def add_ht_by_tva(self, ret_dict, lines):
        return self._get_computer().add_ht_by_tva(
            ret_dict=ret_dict, lines=lines
        )

    def format_amount(self, amount, trim=True, grouping=True, precision=2):
        return self._endi_service.format_amount(
            self, amount, trim, grouping, precision
        )


class DiscountLine(DBBASE):
    """
         A discount line
    """
    __tablename__ = 'discount'
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        nullable=False,
    )
    task_id = Column(
        Integer,
        ForeignKey(
            'task.id',
            ondelete="cascade",

        ),
        info={
            'colanderalchemy': {
                'title': "Identifiant du document",
            }
        }
    )
    description = Column(Text)
    amount = Column(
        BigInteger(),
        info={'colanderalchemy': {'title': 'Montant'}}
    )
    tva = Column(Integer, nullable=False, default=0)
    task = relationship(
        "Task",
        uselist=False,
        info={'colanderalchemy': {'exclude': True}},
    )

    _endi_service = DiscountLineService

    def __json__(self, request):
        return dict(
            id=self.id,
            task_id=self.task_id,
            description=self.description,
            amount=integer_to_amount(self.amount, 5),
            tva=integer_to_amount(self.tva, 2),
            mode=self.task.mode,
        )

    def duplicate(self):
        """
            return the equivalent InvoiceLine
        """
        line = DiscountLine()
        line.tva = self.tva
        line.amount = self.amount
        line.description = self.description
        return line

    @classmethod
    def from_price_study_discount(cls, discount):
        return cls._endi_service.from_price_study_discount(discount)

    def __repr__(self):
        return "<DiscountLine amount : {s.amount} tva:{s.tva} id:{s.id}>"\
            .format(s=self)

    def _get_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of DiscountLineCompute or DiscountLineTtcCompute
        """
        if self.task.mode == 'ttc':
            return DiscountLineTtcCompute(self)
        else:
            return DiscountLineCompute(self)

    def total_ht(self):
        return self._get_computer().total_ht()

    def tva_amount(self):
        return self._get_computer().tva_amount()

    def total(self):
        return self._get_computer().total()

    def get_tva(self):
        return self._get_computer().get_tva()


class TaskStatus(DBBASE):
    """
        Task status, should be used to record the task's status
    """
    __tablename__ = 'task_status'
    __table_args__ = default_table_args
    id = Column(Integer, primary_key=True)
    task_id = Column(
        Integer,
        ForeignKey('task.id', ondelete="cascade"),
    )
    status_code = Column(String(10))
    status_comment = Column(
        Text,
        info={"colanderalchemy": {'widget': deform.widget.TextAreaWidget()}},
    )
    status_person_id = Column(Integer, ForeignKey('accounts.id'))
    status_date = Column(
        Date(),
        default=datetime.date.today,
    )

    task = relationship("Task")
    status_person = relationship(
        "User",
        backref=backref(
            "task_statuses",
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        )
    )

    def __json__(self, request):
        result = {
            "date": self.status_date,
            'code': self.status_code,
            "label": format_status_string(self.status_code),
            "comment": self.status_comment,
        }
        if self.status_person is not None:
            result['account'] = self.status_person.label
        return result


class TaskLineGroup(DBBASE):
    """
    Group of lines
    """
    __table_args__ = default_table_args
    id = Column(Integer, primary_key=True)
    task_id = Column(
        Integer,
        ForeignKey('task.id', ondelete="cascade"),
        info={
            'colanderalchemy': {
                'title': "Identifiant du document",
            }
        }
    )
    description = Column(Text(), default="")
    title = Column(String(255), default="")
    order = Column(Integer, default=1)

    task = relationship(
        "Task",
        primaryjoin="TaskLineGroup.task_id==Task.id",
        info={'colanderalchemy': {'exclude': True}}
    )
    lines = relationship(
        "TaskLine",
        order_by='TaskLine.order',
        cascade="all, delete-orphan",
        back_populates='group',
        collection_class=ordering_list('order'),
        info={
            'colanderalchemy': {
                'title': "Prestations",
            }
        }
    )
    _endi_service = TaskLineGroupService

    def __json__(self, request):
        return dict(
            id=self.id,
            title=self.title,
            description=self.description,
            task_id=self.task_id,
            order=self.order,
            lines=[line.__json__(request) for line in self.lines],
            mode=self.task.mode
        )

    def duplicate(self):
        group = TaskLineGroup(
            title=self.title,
            description=self.description,
            task_id=self.task_id,
            lines=[line.duplicate() for line in self.lines],
            order=self.order,
        )
        return group

    def gen_cancelinvoice_group(self):
        res = self.duplicate()
        for line in res.lines:
            line.cost = -1 * line.cost
        return res

    @classmethod
    def from_price_study_product(cls, product):
        """
        Build an instance based on the given BasePriceStudyProduct
        """
        return cls._endi_service.from_price_study_product(
            cls, product
        )

    @classmethod
    def from_sale_product_work(cls, product, mode='ht'):
        """
        Build an instance based on the given SaleProductWork
        """
        return cls._endi_service.from_sale_product_work(
            cls, product, mode=mode
        )

    def _get_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of GroupCompute or GroupTtcCompute
        """
        if self.task.mode == 'ttc':
            return GroupTtcCompute(self)
        else:
            return GroupCompute(self)

    def total_ttc(self):
        return self._get_computer().total_ttc()

    def get_tvas(self):
        return self._get_computer().get_tvas()

    def get_tvas_by_product(self):
        return self._get_computer().get_tvas_by_product()

    def tva_amount(self):
        return self._get_computer().tva_amount()

    def total_ht(self):
        return self._get_computer().total_ht()


class TaskLine(DBBASE):
    """
        Estimation/Invoice/CancelInvoice lines
    """
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        info={'colanderalchemy': {'widget': deform.widget.HiddenWidget()}}
    )
    group_id = Column(
        Integer,
        ForeignKey('task_line_group.id', ondelete="cascade"),
        info={'colanderalchemy': {'exclude': True}}
    )
    order = Column(Integer, default=1,)
    description = Column(Text)
    # ttc or ht compute base
    mode = Column(
        String(10),
        info={
            'colanderalchemy': {'title': "Mode de saisie"},
            'export': {'exclude': True}
        },
        default='ht'
    )
    cost = Column(
        BigInteger(),
        info={
            'colanderalchemy': {
                'title': 'Montant',
            }
        },
        default=0,
    )
    quantity = Column(
        Float(),
        info={
            'colanderalchemy': {"title": "Quantité"}
        },
        default=1
    )
    unity = Column(
        String(100),
        info={
            'colanderalchemy': {'title': "Unité"}
        },
    )
    tva = Column(
        Integer,
        info={'colanderalchemy': {'title': 'Tva (en %)'}},
        nullable=False,
        default=2000
    )
    product_id = Column(Integer)
    group = relationship(
        TaskLineGroup,
        primaryjoin="TaskLine.group_id==TaskLineGroup.id",
        info={'colanderalchemy': {'exclude': True}}
    )
    product = relationship(
        "Product",
        primaryjoin="Product.id==TaskLine.product_id",
        uselist=False,
        foreign_keys=product_id,
        info={'colanderalchemy': {'exclude': True}}
    )
    _endi_service = TaskLineService
    computer = None

    def duplicate(self):
        """
            duplicate a line
        """
        newone = TaskLine()
        newone.order = self.order
        newone.mode = self.mode
        newone.computer = self.computer,
        newone.cost = self.cost
        newone.tva = self.tva
        newone.description = self.description
        newone.quantity = self.quantity
        newone.unity = self.unity
        newone.product_id = self.product_id
        return newone

    def gen_cancelinvoice_line(self):
        """
            Return a cancel invoice line duplicating this one
        """
        newone = TaskLine()
        newone.order = self.order
        newone.mode = self.mode
        newone.computer = self.computer,
        newone.cost = -1 * self.cost
        newone.tva = self.tva
        newone.description = self.description
        newone.quantity = self.quantity
        newone.unity = self.unity
        newone.product_id = self.product_id
        return newone

    def __repr__(self):
        return "<TaskLine id:{s.id} task_id:{s.group.task_id} cost:{s.cost} \
 quantity:{s.quantity} tva:{s.tva} product_id:{s.product_id}>".format(s=self)

    def __json__(self, request):
        result = dict(
            id=self.id,
            order=self.order,
            mode=self.mode,
            cost=integer_to_amount(self.cost, 5),
            tva=integer_to_amount(self.tva, 2),
            description=self.description,
            quantity=self.quantity,
            unity=self.unity,
            group_id=self.group_id,
            product_id=self.product_id,
        )
        return result

    @property
    def task(self):
        return self.group.task

    @classmethod
    def from_sale_product(cls, sale_product, mode='ht'):
        """
        Build an instance based on the given sale_product

        :param obj sale_product: A SaleProduct instance
        :returns: A TaskLine instance
        """
        return cls._endi_service.from_sale_product(
            cls, sale_product, mode
        )

    @classmethod
    def from_price_study_product(cls, product):
        return cls._endi_service.from_price_study_product(cls, product)

    @classmethod
    def from_price_study_work(cls, work):
        return cls._endi_service.from_price_study_work(cls, work)

    @classmethod
    def from_price_study_work_item(cls, work_item):
        return cls._endi_service.from_price_study_work_item(cls, work_item)

    @classmethod
    def from_sale_product_work_item(cls, work_item, mode='ht'):
        """
        Build an instance based on the given SaleProductWorkItem
        """
        return cls._endi_service.from_sale_product_work_item(
            cls, work_item, mode=mode
        )

    def _get_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of LineCompute or LineTtcCompute
        """
        # Bad hack TODO find why self.computer can be tuple
        self.computer = None
        if self.computer is None:
            if self.mode == 'ttc':
                self.computer = LineTtcCompute(self)
            else:
                self.computer = LineCompute(self)
        return self.computer

    def get_tva(self):
        return self._get_computer().get_tva()

    def unit_ht(self):
        return self._get_computer().unit_ht()

    def total_ht(self):
        return self._get_computer().total_ht()

    def tva_amount(self):
        return self._get_computer().tva_amount()

    def total(self):
        return self._get_computer().total()


def cache_amounts(mapper, connection, target):
    """
    Set amounts in the cached amount vars to be able to provide advanced search
    ... options in the invoice list page
    """
    logger.info("Caching the task amounts")
    if hasattr(target, 'total_ht'):
        target.ht = target.total_ht()
    if hasattr(target, 'total'):
        target.ttc = target.total()
    if hasattr(target, 'tva_amount'):
        target.tva = target.tva_amount()


def cache_parent_amounts(mapper, connection, target):
    """
    Set amounts in the cached amount vars to be able to provide advanced search
    ... options in the invoice list page
    """
    # Buggy since the original modification is not yet persisted
    # Ref https://framagit.org/endi/endi/issues/1055
    if hasattr(target, 'task'):
        logger.info("Caching the parent task amounts")
        task = target.task
        if hasattr(task, 'total_ht'):
            task.ht = task.total_ht()
        if hasattr(task, 'total'):
            task.ttc = task.total()
        if hasattr(task, 'tva_amount'):
            task.tva = task.tva_amount()


def freeze_settings(mapper, connection, target):
    if not target.frozen_settings:
        # Freezing is only done once
        target.freeze_settings()


def start_listening():
    listen(Task, "before_insert", cache_amounts, propagate=True)
    listen(Task, "before_update", cache_amounts, propagate=True)
    listen(Task, "before_insert", freeze_settings, propagate=True)
    listen(Task, "before_update", freeze_settings, propagate=True)
    listen(
        TaskLineGroup, "before_insert", cache_parent_amounts, propagate=True
    )
    listen(
        TaskLineGroup, "before_update", cache_parent_amounts, propagate=True
    )
    listen(TaskLine, "before_insert", cache_parent_amounts, propagate=True)
    listen(TaskLine, "before_update", cache_parent_amounts, propagate=True)
    listen(DiscountLine, "before_insert", cache_parent_amounts, propagate=True)
    listen(DiscountLine, "before_update", cache_parent_amounts, propagate=True)


def stop_listening():
    from sqlalchemy.event import remove
    remove(Task, "before_insert", cache_amounts)
    remove(Task, "before_update", cache_amounts)
    remove(Task, "before_insert", freeze_settings)
    remove(Task, "before_update", freeze_settings)
    remove(TaskLineGroup, "before_insert", cache_parent_amounts)
    remove(TaskLineGroup, "before_update", cache_parent_amounts)
    remove(TaskLine, "before_insert", cache_parent_amounts)
    remove(TaskLine, "before_update", cache_parent_amounts)
    remove(DiscountLine, "before_insert", cache_parent_amounts)
    remove(DiscountLine, "before_update", cache_parent_amounts)


start_listening()
