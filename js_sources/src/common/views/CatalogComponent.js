/*
 * Module name : CatalogComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import LoadingWidget from '../../widgets/LoadingWidget.js';
import {getOpt} from "../../tools.js";
import CatalogTreeView from './CatalogTreeView.js';

const template = require('./templates/CatalogComponent.mustache');

const CatalogComponent = Mn.View.extend({
    /*
    Class displaying the sale_product catalog allowing item selection

    Expects the following options :

        collection_name

            The name of the collection in the associated Facade

        query_params

            The parameters used when querying the server to get the catalog items
        
        multiple 

            Default false : multiple selection allowed ?

    Emits the following events :

        'catalog:insert' => (models)

            Emitted when the Insert button is clicked
            Pass the selected models : list of instances of `class:CatalogTreeModel`


    */
    template: template,
    regions: {
        main: '.main',
    },
    ui: {
        insert_btn: 'button[value=insert]',
        insert_for_edit_btn: 'button[value=insert_for_edit]',
        load_btn: 'button[value=load]',
        cancel_btn: 'button.reset',
        form: 'form'
    },
    // Listen to the current's view events
    events: {
        'click @ui.insert_btn': "onInsertClicked",
    },
    triggers: {
        'click @ui.cancel_btn': 'cancel:click'
    },
    initialize(){
        this.facade = Radio.channel('facade');
        // Params to use when querying the collections
        this.query_params = getOpt(this, 'query_params', {});
        // HT or TTC mode
        this.compute_mode = getOpt(this, 'compute_mode', 'ht');
        // Allow multiple selections ?
        this.multiple = getOpt(this, 'multiple', false);
        console.log("Compute mode %s", this.compute_mode)
    },
    showTree(tree){
        this.collection = tree;
        this.showChildView(
            'main',
            new CatalogTreeView({
                collection: tree, 
                compute_mode: this.compute_mode,
                multiple: this.multiple
            })
        );
        this.collection.on(
            'change:selected',
            this.onItemSelect,
            this
        );
        this.onItemSelect();
    },
    loadCatalogTree(){
        let serverRequest = this.facade.request(
            'load:collection',
            this.getOption('collection_name'),
            this.query_params        
        );
        let this_ = this;
        serverRequest.done(this.showTree.bind(this));
    },
    onRender(){
        this.myid = _.uniqueId();
        console.log(this.myid);
        this.showChildView('main', new LoadingWidget());
        this.loadCatalogTree();
    },
    onItemSelect: function(){
        let models = this.collection.getSelected();
        let btn = this.getUI('insert_btn');
        let value = ! (models.length > 0);
        btn.attr('disabled', value);
        btn.attr('aria-disabled', value);
    },
    onDomRemove(){
        this.collection.off('change:selected');
        this.collection.setNoneSelected();
    },
    onInsertClicked(){
        let models = this.collection.getSelected();
        this.triggerMethod('catalog:insert', models);
    }
});
export default CatalogComponent;