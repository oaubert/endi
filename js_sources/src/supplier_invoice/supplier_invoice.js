/* global AppOption; */
import $ from 'jquery';
import { applicationStartup } from '../backbone-tools.js';

import App from './components/App.js';
import Facade from './components/Facade.js';
import ToolbarAppClass from '../common/components/ToolbarAppClass.js';
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";

const ToolbarApp = new ToolbarAppClass();

$(function(){
    applicationStartup(AppOption, App, Facade, ToolbarApp, ExpenseTypeService);
});
