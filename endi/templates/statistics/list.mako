<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="table_btn"/>

<%block name='actionmenucontent'>
<div class="layout flex main_actions">
	<button class='btn btn-success btn-add' title="Créer une nouvelle feuille de statistiques" onclick="toggleModal('stat_add_form'); return false;">
		<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg> Ajouter
	</button>
</div>
</%block>

<%block name='afteractionmenu'>
<div class="alert alert-info">
	<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
	Configuration des modèles statistiques :
	<ul>
		<li>Créer une feuille de statistiques</li>
		<li>Composer vos entrées statistiques à l'aide de un ou plusieurs critères</li>
		<li>Générer les fichiers de sorties</li>
	</ul>
</div>
</%block>

<%block name='content'>
<div class="content_vertical_padding limited_width width40">
    <div class="table_container">
		<table class='table_hover'>
		<thead>
			<tr>
				<th scope="col" class="col_text">Nom de la feuille de statistiques</th>
				<th scope="col" class="col_date">Modifiée le</th>
				<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
			</tr>
		</thead>
		<tbody>
		% for sheet in sheets:
			<tr
				% if not sheet.active:
					class="locked"
				% endif
				>
				<td class="col_text">${sheet.title}</td>
				<td class="col_date">${api.format_date(sheet.updated_at)}</td>
				<td 
					% if not sheet.active:
					class="col_actions width_four"
					% else:
					class="col_actions width_three"
					%endif
					>
					<% url = request.route_path('statistic', id=sheet.id) %>
					${table_btn(url, "Voir/Modifier", "Voir cette feuille", icon="pen")}
					<% url = request.route_path('statistic', id=sheet.id, _query=dict(action='duplicate')) %>
					${table_btn(url, "Dupliquer", "Dupliquer cette feuille", icon="copy", method='post')}
					<% url = request.route_path('statistic', id=sheet.id, _query=dict(action='disable'), method='post') %>
					<% label = sheet.active and "Désactiver" or "Activer" %>
					<% icon = sheet.active and 'lock' or 'lock-open' %>
					<% css_class = sheet.active and 'btn-danger' or 'btn-success' %>
					${table_btn(url, label, "Ce modèle est-il toujours utilisé ?", icon=icon, css_class=css_class, method='post')}
					% if not sheet.active:
						<% url = request.route_path('statistic', id=sheet.id, _query=dict(action='delete')) %>
						<% label = "Supprimer" %>
						${table_btn(url, label, "Définitivement supprimer ce modèle ?", icon="trash-alt", css_class="negative", method='post')}
					% endif
				</td>
			</tr>
		% endfor
		</tbody>
		</table>
    </div>
</div>
<section id="stat_add_form" class="modal_view size_small" style="display:none;">
    <div role="dialog" id="stat-forms" aria-modal="true" aria-labelledby="stat-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('stat_add_form'); return false;">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
                </button>
                <h2 id="stat-forms_title">Ajouter une feuille de statistiques</h2>
            </header>
            <main>
				<div style="display:none;" id='form-container'></div>
            </main>
        </div>
    </div>
</section>
</%block>

<%block name='footerjs'>
AppOptions = {};
AppOptions['submiturl'] = "${submiturl}";
</%block>
