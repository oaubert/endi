"""
    Invoice views
"""
import logging
import datetime

from pyramid.httpexceptions import HTTPFound

from endi_base.utils.date import format_date
from endi.models.task import (
    Invoice,
    Estimation,
)
from endi.utils.widgets import ViewLink
from endi.forms.tasks.invoice import (
    EstimationAttachSchema,
    get_add_edit_invoice_schema,
)
from endi.resources import task_html_pdf_css
from endi.views import (
    BaseEditView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    add_panel_page_view,
)
from endi.views.files.views import FileUploadView
from endi.views.project.routes import PROJECT_ITEM_INVOICE_ROUTE
from endi.views.business.business import BusinessOverviewView
from endi.views.task.views import (
    TaskAddView,
    TaskEditView,
    TaskDeleteView,
    TaskHtmlView,
    TaskPdfView,
    TaskPdfDevView,
    TaskDuplicateView,
    TaskSetMetadatasView,
    TaskSetProductsView,
    TaskSetDraftView,
    TaskMoveToPhaseView,
    TaskSyncWithPriceStudyView,
)


logger = log = logging.getLogger(__name__)


class InvoiceAddView(TaskAddView):
    """
    Invoice add view
    context is a project or company
    """
    title = "Nouvelle facture"
    factory = Invoice

    def _more_init_attributes(self, invoice, appstruct):
        """
        Add Invoice's specific attribute while adding this task
        """
        invoice.financial_year = datetime.date.today().year
        invoice.prefix = self.request.config.get('invoiceprefix', '')
        business = invoice.gen_business()
        invoice.initialize_business_datas(business)
        return invoice

    def _after_flush(self, invoice):
        """
        Launch after the new invoice has been flushed
        """
        logger.debug(
            "  + Invoice successfully added : {0}".format(invoice.id)
        )

    def populate_actionmenu(self):
        if self.request.context.__name__ == 'company':
            self.request.actionmenu.add(
                ViewLink(
                    "Revenir à la liste des factures",
                    path="company_invoices",
                    id=self.request.context.id,
                )
            )
        else:
            super().populate_actionmenu()


class InvoiceEditView(TaskEditView):
    route_name = '/invoices/{id}'

    @property
    def title(self):
        customer = self.context.customer
        customer_label = customer.label
        if customer.code is not None:
            customer_label += " ({0})".format(customer.code)
        return (
            "Modification de la {tasktype_label} {task.name} avec le client "
            "{customer}".format(
                task=self.context,
                customer=customer_label,
                tasktype_label=self.context.get_type_label().lower(),
            )
        )

    def task_line_group_api_url(self):
        if self.context.invoicing_mode == self.context.PROGRESS_MODE:
            url = self.context_url() + "/progress_invoicing/groups"
        else:
            url = self.context_url() + "/task_line_groups"
        return url

    def discount_api_url(self):
        return self.context_url() + "/discount_lines"

    def more_js_app_options(self):
        return {
            'invoicing_mode': self.context.invoicing_mode,
            'discount_api_url': self.discount_api_url(),
        }


class InvoiceDeleteView(TaskDeleteView):
    msg = "La facture {context.name} a bien été supprimée."

    def pre_delete(self):
        """
        If an estimation is attached to this invoice, ensure geninv is set to
        False
        """
        self.invoice_id = self.context.id
        self.business = self.context.business
        if self.context.estimation is not None:
            if len(self.context.estimation.invoices) == 1:
                self.context.estimation.geninv = False
                self.request.dbsession.merge(self.context.estimation)

    def post_delete(self):
        if self.business:
            self.business.on_invoice_delete(self.invoice_id)


class InvoiceHtmlView(TaskHtmlView):
    @property
    def label(self):
        return self.context.get_type_label()
    route_name = '/invoices/{id}.html'


class InvoiceDuplicateView(TaskDuplicateView):
    @property
    def label(self):
        return f'la {self.context.get_type_label().lower()}'

    def _after_task_duplicate(self, task, appstruct):
        business = task.gen_business()
        task.initialize_business_datas(business)
        return task


class InvoicePdfView(TaskPdfView):
    pass


def gencinv_view(context, request):
    """
    Cancelinvoice generation view
    """
    try:
        cancelinvoice = context.gen_cancelinvoice(request.user)
        cancelinvoice.initialize_business_datas()
        request.dbsession.add(cancelinvoice)
        request.dbsession.flush()
    except:  # noqa
        logger.exception(
            "Error while generating a cancelinvoice for {0}".format(
                context.id
            )
        )
        request.session.flash(
            "Erreur à la génération de votre avoir, "
            "contactez votre administrateur",
            'error'
        )
        return HTTPFound(request.route_path("/invoices/{id}", id=context.id))
    return HTTPFound(
        request.route_path("/cancelinvoices/{id}", id=cancelinvoice.id)
    )


class InvoiceSetTreasuryiew(BaseEditView):
    """
    View used to set treasury related informations

    context

        An invoice

    perms

        set_treasury.invoice
    """
    factory = Invoice
    schema = get_add_edit_invoice_schema(
        includes=('financial_year',),
        title="Modifier l'année fiscale de référence du numéro de facture",
    )

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                "/invoices/{id}.html",
                id=self.context.id,
                _anchor="treasury"
            )
        )

    def before(self, form):
        BaseEditView.before(self, form)
        self.request.actionmenu.add(
            ViewLink(
                label=f"Revenir à la {self.context.get_type_label().lower()}",
                path="/invoices/{id}.html",
                id=self.context.id,
                _anchor="treasury",
            )
        )

    @property
    def title(self):
        return "{} numéro {} en date du {}".format(
            self.context.get_type_label(),
            self.context.official_number,
            format_date(self.context.date),
        )


class InvoiceSetMetadatasView(TaskSetMetadatasView):
    """
    View used for editing invoice metadatas
    """

    @property
    def title(self):
        return "Modification de la {tasktype_label} {task.name}".format(
            task=self.context,
            tasktype_label=self.context.get_type_label().lower(),
        )


class InvoiceSetProductsView(TaskSetProductsView):
    @property
    def title(self):
        return (
            "Configuration des codes produits pour la facture "
            "{0.name}".format(
                self.context
            )
        )


class InvoiceAttachEstimationView(BaseFormView):
    schema = EstimationAttachSchema()
    buttons = (submit_btn, cancel_btn,)

    def before(self, form):
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir à la facture",
                path="/invoices/{id}.html",
                id=self.context.id,
            )
        )
        if self.context.estimation_id:
            form.set_appstruct({'estimation_id': self.context.estimation_id})

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/invoices/{id}.html',
                id=self.context.id,
            )
        )

    def submit_success(self, appstruct):
        estimation_id = appstruct.get('estimation_id')
        self.context.estimation_id = estimation_id
        if estimation_id is not None:
            estimation = Estimation.get(estimation_id)
            estimation.geninv = True
            self.request.dbsession.merge(estimation)
        self.request.dbsession.merge(self.context)
        return self.redirect()

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


class InvoiceAdminView(BaseEditView):
    """
    Vue pour l'administration de factures /invoices/id/admin

    Vue accessible aux utilisateurs admin
    """
    factory = Invoice
    schema = get_add_edit_invoice_schema(
        title="Formulaire d'édition forcée de devis/factures/avoirs",
        help_msg="Les montants sont *10^5   10 000==1€",
    )


def add_routes(config):
    """
    add module related routes
    """
    config.add_route(
        '/invoices/{id}',
        '/invoices/{id:\d+}',
        traverse='/invoices/{id}',
    )
    for extension in ('html', 'pdf', 'preview'):
        config.add_route(
            '/invoices/{id}.%s' % extension,
            '/invoices/{id:\d+}.%s' % extension,
            traverse='/invoices/{id}'
        )
    for action in (
        'addfile',
        'delete',
        'duplicate',
        'admin',
        'set_treasury',
        'set_products',
        'gencinv',
        'set_metadatas',
        'attach_estimation',
        'set_draft',
        'move',
        "sync_price_study",
    ):
        config.add_route(
            '/invoices/{id}/%s' % action,
            '/invoices/{id:\d+}/%s' % action,
            traverse='/invoices/{id}'
        )


def includeme(config):
    add_routes(config)

    config.add_view(
        InvoiceAddView,
        route_name=PROJECT_ITEM_INVOICE_ROUTE,
        renderer='tasks/add.mako',
        permission='add.invoice',
        request_param="action=add",
        layout='default'
    )

    config.add_view(
        InvoiceAddView,
        route_name='company_invoices',
        renderer='tasks/add.mako',
        permission='add.invoice',
        request_param="action=add",
        layout='default'
    )

    config.add_tree_view(
        InvoiceEditView,
        parent=BusinessOverviewView,
        renderer='tasks/form.mako',
        permission='view.invoice',
        layout='opa',
    )

    config.add_view(
        InvoiceDeleteView,
        route_name='/invoices/{id}/delete',
        permission='delete.invoice',
        require_csrf=True,
        request_method='POST',
    )

    config.add_view(
        InvoiceAdminView,
        route_name='/invoices/{id}/admin',
        renderer="base/formpage.mako",
        permission="admin",
    )

    config.add_view(
        InvoiceDuplicateView,
        route_name="/invoices/{id}/duplicate",
        permission="duplicate.invoice",
        renderer='tasks/add.mako',
    )

    config.add_tree_view(
        InvoiceHtmlView,
        parent=BusinessOverviewView,
        renderer='tasks/invoice_view_only.mako',
        permission='view.invoice',
    )

    add_panel_page_view(
        config,
        'task_pdf_invoice_content',
        js_resources=(task_html_pdf_css,),
        route_name='/invoices/{id}.preview',
        permission="view.invoice",
    )

    config.add_view(
        InvoicePdfView,
        route_name='/invoices/{id}.pdf',
        permission='view.invoice',
    )

    config.add_view(
        TaskPdfDevView,
        route_name='/invoices/{id}.preview',
        request_param="action=dev_pdf",
        renderer='panels/task/pdf/content_wrapper.mako',
        permission='view.invoice',
    )

    config.add_view(
        FileUploadView,
        route_name="/invoices/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )

    config.add_view(
        gencinv_view,
        route_name="/invoices/{id}/gencinv",
        permission="gencinv.invoice",
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        InvoiceSetTreasuryiew,
        route_name="/invoices/{id}/set_treasury",
        permission="set_treasury.invoice",
        renderer='base/formpage.mako',
    )
    config.add_view(
        InvoiceSetMetadatasView,
        route_name="/invoices/{id}/set_metadatas",
        permission="view.invoice",
        renderer='tasks/add.mako',
    )
    config.add_view(
        TaskSetDraftView,
        route_name="/invoices/{id}/set_draft",
        permission="draft.invoice",
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        InvoiceSetProductsView,
        route_name="/invoices/{id}/set_products",
        permission="set_treasury.invoice",
        renderer='base/formpage.mako',
    )
    config.add_view(
        InvoiceAttachEstimationView,
        route_name="/invoices/{id}/attach_estimation",
        permission="view.invoice",
        renderer='base/formpage.mako',
    )
    config.add_view(
        TaskMoveToPhaseView,
        route_name="/invoices/{id}/move",
        permission="view.invoice",
        require_csrf=True,
        request_method="POST",
    )
    config.add_view(
        TaskSyncWithPriceStudyView,
        route_name="/invoices/{id}/sync_price_study",
        permission="edit.invoice",
    )
