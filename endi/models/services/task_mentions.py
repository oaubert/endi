from endi_base.models.base import DBSESSION
from endi.models.project.types import BusinessType


class TaskMentionService(object):
    @classmethod
    def populate(cls, task):
        with DBSESSION.no_autoflush:
            task.mandatory_mentions = BusinessType.get_mandatory_mentions(
                task.business_type_id,
                task.type_,
            )
