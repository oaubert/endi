"""
    render api, usefull functions usable inside templates
"""
from webhelpers2.html import literal

from endi_base.utils.date import (
    format_date,
    format_duration,
    format_short_date,
    format_long_date,
    format_datetime,
)
from endi_base.utils.strings import (
    format_quantity,
)
from endi.utils.strings import (
    format_amount,
    format_float,
    format_status,
    format_status_sentence,
    format_activity_status,
    format_expense_status,
    format_supplier_invoice_status,
    format_account,
    format_civilite,
    format_name,
    format_paymentmode,
    format_task_type,
    format_status_string,
    format_estimation_status,
    format_invoice_status,
    format_cancelinvoice_status,
    estimation_get_major_status,
    invoice_get_major_status,
    cancelinvoice_get_major_status,
    major_status,
    pluralize,
    human_readable_filesize,
    month_name,
    short_month_name,
    compile_template_str,
    remove_kms_training_zeros,
)
from endi.utils.html import clean_html
from endi.views.files.routes import FILE_ITEM, FILE_PNG_ITEM


STATUS_ICON = dict(
    (
        ('draft', 'pen'),
        ('wait', 'clock'),
        ('valid', 'check-circle'),
        ('invalid', 'times-circle'),
    )
)

STATUS_CSS_CLASS = dict(
    (
        ('draft', 'draft'),
        ('wait', 'caution'),
        ('valid', 'valid'),
        ('invalid', 'invalid'),
    )
)

EXPENSE_STATUS_CSS_CLASS = dict(
    (
        ('resulted', 'valid'),
        ('paid', 'partial_unpaid'),
    )
)

ESTIMATION_STATUS_ICON = dict(
    (
        ('aborted', 'times'),
        ('sent', 'envelope'),
        ('signed', 'check'),
        ('geninv', 'euro-sign'),
    )
)
INVOICE_STATUS_ICON = dict(
    (
        ('paid', 'euro-sign'),
        ("resulted", 'euro-sign'),
    )
)
INVOICE_WAITING_STATUS_ICON = dict(
    (
        ('valid', 'euro-slash'),
        ('invalid', 'euro-slash'),
    )
)
EXPENSE_STATUS_ICON = dict(
    (
        ('paid', 'euro-sign'),
        ('resulted', 'euro-sign'),
        ('justified', 'file-check'),
    )
)


def estimation_status_icon(estimation):
    """
    Return the name of the icon matching the status
    """
    if estimation.geninv:
        return ESTIMATION_STATUS_ICON.get("geninv")
    elif estimation.signed_status != 'waiting':
        return ESTIMATION_STATUS_ICON.get(estimation.signed_status)
    else:
        return STATUS_ICON.get(estimation.status)


def invoice_status_icon(invoice):
    """
    Return the name of the icon matching the status
    """
    if invoice.paid_status != 'waiting':
        return INVOICE_STATUS_ICON.get(invoice.paid_status)
    else:
        if invoice.status in ('draft', 'wait'):
            return STATUS_ICON.get(invoice.status)
        return INVOICE_WAITING_STATUS_ICON.get(invoice.status)


def cancelinvoice_status_icon(cinvoice):
    """
    Return the name of the icon matching the status
    """
    return STATUS_ICON.get(cinvoice.status)


def expense_status_icon(expense):
    """
    Return the name of the icon matching the status
    """
    if expense.paid_status != 'waiting':
        return EXPENSE_STATUS_ICON.get(expense.paid_status)
    elif expense.justified:
        return EXPENSE_STATUS_ICON.get("justified")
    else:
        return STATUS_ICON.get(expense.status)


def expense_status_css_class(expense):
    if expense.paid_status != 'waiting':
        return EXPENSE_STATUS_CSS_CLASS.get(expense.paid_status)
    else:
        return STATUS_CSS_CLASS.get(expense.status)


def status_log_entry_icon(status_log_entry):
    return STATUS_ICON.get(status_log_entry.status)


def supplier_order_status_icon(supplier_order):
    return STATUS_ICON.get(supplier_order.status)

def supplier_invoice_status_icon(supplier_invoice):
    return STATUS_ICON.get(supplier_invoice.status)

def status_icon(element):
    if element.__class__.__name__ == 'StatusLogEntry':
        return status_log_entry_icon(element)
    if element.type_ == 'estimation':
        return estimation_status_icon(element)
    elif element.type_ == 'invoice':
        return invoice_status_icon(element)
    elif element.type_ == 'cancelinvoice':
        return cancelinvoice_status_icon(element)
    elif element.type_ == 'expensesheet':
        return expense_status_icon(element)
    elif element.type_ == 'supplier_order':
        return supplier_order_status_icon(element)
    elif element.type_ == 'supplier_invoice':
        return supplier_invoice_status_icon(element)



def status_css_class(element):
    if element.__class__.__name__ in ('Invoice', 'CancelInvoice', 'Estimation'):
        return task_status_css_class(element)
    if element.__class__.__name__ == 'ExpenseSheet':
        return expense_status_css_class(element)
    else:
        return STATUS_CSS_CLASS.get(element.status, '')

class Api(object):
    """
        Api object passed to the templates hosting all commands we will use
    """
    format_amount = staticmethod(format_amount)
    format_float = staticmethod(format_float)
    format_date = staticmethod(format_date)
    format_status = staticmethod(format_status)
    format_status_sentence = staticmethod(format_status_sentence)
    format_expense_status = staticmethod(format_expense_status)
    format_supplier_invoice_status = staticmethod(format_supplier_invoice_status)
    format_activity_status = staticmethod(format_activity_status)
    format_account = staticmethod(format_account)
    format_civilite = staticmethod(format_civilite)
    format_name = staticmethod(format_name)
    format_paymentmode = staticmethod(format_paymentmode)
    format_short_date = staticmethod(format_short_date)
    format_long_date = staticmethod(format_long_date)
    format_quantity = staticmethod(format_quantity)
    format_datetime = staticmethod(format_datetime)
    format_duration = staticmethod(format_duration)
    format_task_type = staticmethod(format_task_type)
    compile_template_str = staticmethod(compile_template_str)

    format_status_string = staticmethod(format_status_string)
    format_estimation_status = staticmethod(format_estimation_status)
    format_invoice_status = staticmethod(format_invoice_status)
    format_cancelinvoice_status = staticmethod(format_cancelinvoice_status)
    estimation_status_icon = staticmethod(estimation_status_icon)
    estimation_get_major_status = staticmethod(estimation_get_major_status)
    invoice_status_icon = staticmethod(invoice_status_icon)
    invoice_get_major_status = staticmethod(invoice_get_major_status)
    cancelinvoice_status_icon = staticmethod(cancelinvoice_status_icon)
    cancelinvoice_get_major_status = staticmethod(
        cancelinvoice_get_major_status)
    major_status = staticmethod(major_status)
    pluralize = staticmethod(pluralize)
    status_icon = staticmethod(status_icon)
    status_css_class = staticmethod(status_css_class)

    human_readable_filesize = staticmethod(human_readable_filesize)
    month_name = staticmethod(month_name)
    short_month_name = staticmethod(short_month_name)
    clean_html = staticmethod(clean_html)
    remove_kms_training_zeros = staticmethod(remove_kms_training_zeros)

    def __init__(self, context, request):
        self.request = request
        self.context = context
        if getattr(request, 'template_api', None) is None:
            request.template_api = self

    def has_permission(self, perm_name, context=None):
        context = context or self.context
        return self.request.has_permission(perm_name, context)

    def urlupdate(self, args_dict={}):
        """
            Return the current url with updated GET params
            It allows to keep url params when :
            * sorting
            * searching
            * moving from one page to another

            if current url ends with :
                <url>?foo=1&bar=2
            when passing {'foo':5}, we get :
                <url>?foo=5&bar=2
        """
        get_args = self.request.GET.copy()
        get_args.update(args_dict)
        path = self.request.current_route_path(_query=get_args)
        return path

    def file_url(self, fileobj):
        """
        Return the url to access the given fileobj
        """
        if fileobj is not None and fileobj.id is not None:
            return self.request.route_path(FILE_ITEM, id=fileobj.id)
        else:
            return ""

    def img_url(self, fileobj):
        """
        Return the url to access the given fileobj as an image
        """
        if fileobj is not None and fileobj.id is not None:
            return self.request.route_path(FILE_PNG_ITEM, id=fileobj.id)
        else:
            return ""

    def icon(self, icon_name):
        """
        Crafts the HTML to include the named icon.
        :param icon_name: see https://endi.sophieweb.com/html/icones.html
        """
        out = '<svg><use href="{}#{}"></use></svg>'.format(
            self.request.static_url('endi:static/icons/endi.svg'),
            icon_name,
        )
        return literal(out)

    def overridable_label(self, label_key: str) -> str:
        """
        Gets a label, possibly overriden by db setting and/or frozen setting
        """
        from endi.models.services.naming import NamingService
        return NamingService.get_label_for_context(label_key, self.context)
