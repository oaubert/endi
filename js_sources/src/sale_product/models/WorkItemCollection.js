/*
 * File Name :  WorkItemCollection
 */
import OrderableCollection from '../../base/models/OrderableCollection.js';
import Radio from 'backbone.radio';
import WorkItemModel from './WorkItemModel.js';
import {ajax_call} from '../../tools.js';

const WorkItemCollection = OrderableCollection.extend({
    model: WorkItemModel,
    load_from_catalog: function(models){
        let base_sale_product_ids = _.pluck(models, 'id');
        var serverRequest = ajax_call(
            this.url() + '?action=load_from_catalog',
            {base_sale_product_ids: base_sale_product_ids},
            'POST'
        );
        return serverRequest.then(this.fetch.bind(this));
    },
    syncAll(models){
        console.log("WorkItemCollection Syncing all models");
        var promises = [];
        this.each(function(model){
            promises.push(model.fetch());
        });
        var resulting_deferred = $.when(...promises);
        return resulting_deferred;
    },
});
export default WorkItemCollection;
