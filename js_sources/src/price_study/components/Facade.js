/*
Global Api, handling all the model and collection fetch

facade = Radio.channel('facade');
facade.request('get:collection', 'sale_products');
*/
import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';
import { ajax_call } from '../../tools.js';

import PriceStudyModel from '../models/PriceStudyModel.js';
import ProductCollection from '../models/ProductCollection.js';
import DiscountCollection from '../models/DiscountCollection.js';
import CatalogTreeCollection from '../../common/models/CatalogTreeCollection.js';
import FacadeModelApiMixin from '../../base/components/FacadeModelApiMixin.js';


const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    radioEvents: {
        'changed:product': 'onChangedProduct',
        'changed:discount': 'onChangedDiscount',
    },
    radioRequests: {
        'get:model': 'getModelRequest',
        'get:collection': 'getCollectionRequest',
        'load:collection': 'loadCollection',
        'insert:from:catalog': 'insertProductFromCatalog',
        'workitem:from:catalog': 'insertWorkitemFromCatalog',
        'is:valid': "isDatasValid",
    },
    initialize(options){
        this.models = {};
        this.models['price_study'] = new PriceStudyModel({});
        this.collections = {};
        let collection;

        collection = new ProductCollection();
        this.collections['products'] = collection;

        collection = new CatalogTreeCollection();
        this.collections['catalog_tree'] = collection;

        collection = new DiscountCollection();
        this.collections['discounts'] = collection;
    },
    setup(options){
        this.setModelUrl('price_study', options['context_url']);
		this.setCollectionUrl('products', options['product_collection_url']);
        this.setCollectionUrl('catalog_tree', options['catalog_tree_url']);
        this.setCollectionUrl('discounts', options['discount_collection_url']);
    },
    _afterLoadFromCatalog(collection, models){
        console.log("Fetching collection")
        let serverRequest = collection.fetch();
        this.models.price_study.fetch();
        return serverRequest.then(function(){
            console.log('Returnin models');
            console.log(models);
            return models;
        })
    },
    _insertFromCatalog(collection, sale_products){
        /*
         Return a Promise resolving the model insertion
        */
       console.log("_insertFromCatalog");

        const sale_product_ids = sale_products.map((item) => item.id);
        let url = _.result(collection, 'url'); // Lance la fonction ou récupère l'attribut
        url += "?action=load_from_catalog";

        let serverRequest = ajax_call(
            url,
            {sale_product_ids: sale_product_ids},
            'POST'
        );

        return serverRequest.then(
            (models) => {return this._afterLoadFromCatalog(collection, models)}
        );
    },
    insertProductFromCatalog(sale_products){
        const collection = this.collections['products']
        console.log(collection);
       console.log(sale_products);
        return this._insertFromCatalog(collection, sale_products);
    },
    insertWorkitemFromCatalog(collection, sale_products){
        /*
         Return a Promise resolving the model insertion
        */
       console.log(collection);
       console.log(sale_products);
        return this._insertFromCatalog(collection, sale_products);
    },
    start(){
        /*
         * Fires initial One Page application Load
         */
        console.log("Facade.start : Loading current models");
        let modelRequest = this.loadModel('price_study');
        let collectionRequest = this.loadCollection('products');
        let discountColectionRequest = this.loadCollection('discounts');
        return $.when(modelRequest, collectionRequest, discountColectionRequest);
    },
    onChangedProduct(){
        console.log("Product datas chanegd");
        this.models.price_study.fetch();
        this.collections.discounts.fetch();
    },
    onChangedDiscount(){
        console.log("A discount has been saved");
        this.models.price_study.fetch();
    },
    isDatasValid(){
        let channel = Radio.channel('facade');
        channel.trigger('bind:validation');
        let result = this.collections.products.validateModels();
        channel.trigger('unbind:validation');
        return result;
    }
});
const Facade = new FacadeClass();
export default Facade;
