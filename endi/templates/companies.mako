<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/utils.mako" import="company_list_badges"/>
<%namespace file="/base/utils.mako" import="login_disabled_msg"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='content'>

${searchform()}

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
		<table class="hover_table">
			<thead>
				<tr>
					<th scope="col" class="col_text">${sortable("Nom", "name")}</th>
					<th scope="col" class="col_text">Adresse e-mail</th>
					<th scope="col" class="col_text">Entrepreneur(s)</th>
					<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
				</tr>
			</thead>
			<tbody>
				% for company in records:
					<% url = request.route_path('company', id=company.id, _query=dict(action='edit')) %>
					<tr>
						<td class="col_text">
							<% company_url = request.route_path('company', id=company.id) %>
							% if request.has_permission('view.company', company):
								<a href="${company_url}"><strong>${company.name}</strong> (<small>${company.goal}</small>)</a>
								% if request.has_permission('admin_company', company):
									${company_list_badges(company)}
								% endif
							% else:
								${company.name} (<small>${company.goal}</small>)
							% endif
						</td>
						<td class="col_text">${company.email}</td>
						<td class="col_text">
							<ul>
								% for user in company.employees:
									<li>
										% if request.has_permission('view.user', user):
											<a href="${request.route_path('/users/{id}', id=user.id)}">
												${api.format_account(user)}
											</a>
											% if user.login is None:
												<span class="icon">
													<svg class="caution"><use href="${request.static_url('endi:static/icons/endi.svg')}#exclamation-circle"></use></svg>
													Ce compte ne dispose pas d’identifiants
												</span>
											% elif not user.login.active:
												${login_disabled_msg()}
											% endif
										% else:
											${api.format_account(user)}
										% endif
									</li>
								% endfor
							</ul>
						</td>
						<td class="col_actions width_one">
							${request.layout_manager.render_panel('action_dropdown', links=stream_actions(company))}
						</td>
					</tr>
				% endfor
			</tbody>
		</table>
	</div>
	${pager(records)}
</div>
</%block>
