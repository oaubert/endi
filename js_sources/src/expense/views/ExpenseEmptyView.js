import Mn from 'backbone.marionette';

const ExpenseEmptyView = Mn.View.extend({
    tagName: 'tr',
    template: require('./templates/ExpenseEmptyView.mustache'),
    templateContext(){
        var colspan = this.getOption('colspan');
        if (this.getOption('edit')){
            colspan += 1;
        }
        return {
            colspan: colspan
        };
    }
});
export default ExpenseEmptyView;
