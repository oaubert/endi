import _ from 'underscore';
import BaseFormWidget from './BaseFormWidget.js';
import { getOpt, setDatePicker } from '../tools.js';
import { formatDate, dateToIso } from '../date.js';


var template = require('./templates/DatePickerWidget.mustache');

const DatePickerWidget = BaseFormWidget.extend({
    /*
    A jquery datepicker widget
    */
    template: template,
    ui: {
        altdate: "input[name=altdate]" ,
        'btnErase': 'button.erase'
    },
    events: {
        'click @ui.btnErase': "clear"
    },
    clear(){
        $(this.getUI('altdate')).datepicker('setDate', null);
        this.onDateSelect();
    },
    onDateSelect: function(){
        /* On date select trigger a change event */
        const value = $(this.getSelector()).val();
        this.triggerMethod('finish', this.getOption('field_name'), value);
    },
    getSelector: function(){
        /* Return the selector for the date input */
        return "input[name=" + getOpt(this, 'field_name', 'date') + "]";
    },
    getValue(){
        let value = getOpt(this, "date", this.getOption('value'));
        return value;
    },
    onAttach: function(){
        /* On attachement in case of edit, we setup the datepicker */
        if (getOpt(this, 'editable', true)){
            var today = dateToIso(new Date());
            var kwargs = {
                // Bind the method to access view through the 'this' param
                onSelect: this.onDateSelect.bind(this),
                default_value: getOpt(this, 'default_value', today)
            };
            if (getOpt(this, 'current_year', false)){
                kwargs['changeYear'] = false;
                kwargs['yearRange'] = '-0:+0';
            }


            let date = this.getValue();
            let selector = this.getSelector();
            setDatePicker(
                this.getUI('altdate'),
                selector,
                date,
                kwargs
            );
        }
    },
    templateContext: function(){
        /*
         * Give parameters for the templating context
         */
        let ctx = this.getCommonContext();
        let more_ctx = {};
        if (!getOpt(this, 'editable', true)){
            more_ctx['date'] = formatDate(this.getValue());
        }
        more_ctx['erasable'] = false;
        if (!getOpt(this, 'required', false)){
            if (this.getValue()){
                more_ctx['erasable'] = true;
            }
        }
        return Object.assign(ctx, more_ctx);;
    }
});
export default DatePickerWidget;
