"""
    Nodes model is a base model for many other models (projects, documents,
    files, events)
    This way we can easily use the parent/children relationship on an agnostic
    way as in a CMS
"""
import colander
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
)
from sqlalchemy.orm import relationship, backref

from endi_base.models.mixins import (
    PersistentACLMixin,
    TimeStampedMixin,
)
from endi_base.models.types import (
    ACLType,
    MutableList,
)
from endi_base.models.base import (
    DBBASE,
    default_table_args,
)


class Node(PersistentACLMixin, TimeStampedMixin, DBBASE):
    """
    A base node providing a parent<->children structure for most of the models
    (usefull for file attachment)
    """
    __tablename__ = 'node'
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_on': 'type_',
        'polymorphic_identity': 'nodes',
    }

    id = Column(
        Integer,
        primary_key=True,
    )
    name = Column(
        String(255),
        info={
            'colanderalchemy': {
                'title': "Nom",
                "missing": colander.required,
            },
        },
        nullable=True,
    )
    parent_id = Column(
        ForeignKey('node.id'),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    children = relationship(
        'Node',
        primaryjoin='Node.id==Node.parent_id',
        backref=backref(
            'parent',
            remote_side=[id],
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        ),
        cascade='all',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    files = relationship(
        "File",
        primaryjoin="Node.id==File.parent_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    type_ = Column(
        'type_',
        String(30),
        info={'colanderalchemy': {'exclude': True}},
        nullable=False,
    )
    _acl = Column(
        MutableList.as_mutable(ACLType),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )
    file_requirements = relationship(
        "SaleFileRequirement",
        back_populates='node',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )

    NODE_LABELS = {
        "estimation": "Devis",
        "invoice": "Facture",
        "cancelinvoice": "Avoir",
        "business": "Affaire",
        "project": "Dossier",
        "expense_sheet": "Note de dépenses",
        "workshop": "Atelier",
        "activity": "Rendez-vous",
        "third_party": "Tiers",
        "supplier_order": "Commande fournisseur",
        "supplier_invoice": "Facture fournisseur",
    }

    @property
    def type_label(self):
        return self.NODE_LABELS.get(self.type_, 'Donnée')
