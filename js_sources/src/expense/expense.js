import $ from 'jquery';
import _ from 'underscore';

import {applicationStartup} from '../backbone-tools.js';

import App from './components/App.js';
import Facade from './components/Facade.js';
import ToolbarApp from './components/ToolbarApp.js';
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";

$(function(){
    applicationStartup(AppOption, App, Facade, ToolbarApp, ExpenseTypeService);
});
