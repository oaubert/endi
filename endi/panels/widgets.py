
def _concat_class_attr(*args):
    """
    Concat several values to be put together in an HTML class attribute.
    None or empty *args will be ignored.

    >>> _concat_class_attr("", "btn btn-danger", "huge", None)
    "btn btn-danger huge"

    :param *args str: string or None
    """
    nonempty_args = [i for i in args if i]
    return ' '.join(nonempty_args)


def link_panel(context, request, extra_classes=''):
    """
    simple link panel used to render links

    :param obj context: The context to render, an instance of the Link class
    :param obj request: The current pyramid request
    :param extra_classes: string to be appendend to the <a> class attr
    """
    return dict(
        link=context,
        css_classes=_concat_class_attr(context.css, extra_classes),
    )


def post_button_panel(context, request, extra_classes=''):
    """
    simple form+submit panel used to render single-POST action links.

    :param obj context: The context to render, an instance of POSTButton
    :param extra_classes: string to be appendend to the <button> class attr
    :param obj request: The current pyramid request
    """
    return dict(
        link=context,
        get_csrf_token=request.session.get_csrf_token,
        css_classes=_concat_class_attr(context.css, extra_classes),
        extra_fields=context.extra_fields,
    )


def admin_index_nav_panel(context, request):
    """
    A panel to render the navigation inside the administration interface

    :param obj context: The context to render, list of Link or AdminLin
    :param obj request: The current pyramid request
    """
    return dict(menus=context, item_panel_name="admin_index_link")


def menu_dropdown_panel(context, request, label, links, icon=None):
    """
    Menu dropdown panel

    :param obj context: The current context
    :param obj request: The current pyramid request
    :param str label: the label to use
    :param str icon: An optionnal icon to add
    :param list buttons: List of endi.widgets.Link
    """
    return dict(label=label, links=links, icon=icon)


def action_dropdown_panel(context, request, label="Actions", links=()):
    return menu_dropdown_panel(context, request, label, links)


def action_buttons_panel(context, request, buttons):
    """
    Action buttons panel (icon only)

    :param obj context: The current context
    :param obj request: The current pyramid request
    :param list buttons: List of endi.widgets.Link
    """
    return dict(buttons=buttons)


def legend_panel(context, request, legends):
    """
    a legend panel shows a legend link with a dropdown div containing the
    legends

    :param obj context: The request's context
    :param obj request: The current Pyramid request
    :param list legends: List of 2-uples (status-css class, label)
    """
    return dict(context=context, request=request, legends=legends)


def status_title_panel(context, request):
    """ Syntethizing the statuses of an object into a sentence and an icon
    """
    return dict(context=context)


def includeme(config):
    config.add_panel(
        link_panel,
        "link",
        renderer="endi:templates/panels/widgets/link.pt",
    )
    config.add_panel(
        post_button_panel,
        "post_button",
        renderer="endi:templates/panels/widgets/post_button.pt",
    )
    config.add_panel(
        admin_index_nav_panel,
        'admin_index_nav',
        renderer="endi:templates/panels/widgets/admin_index_nav.pt",
    )
    config.add_panel(
        link_panel,
        "admin_index_link",
        renderer="endi:templates/panels/widgets/admin_index_link.pt",
    )
    config.add_panel(
        menu_dropdown_panel,
        "menu_dropdown",
        renderer="endi:templates/panels/widgets/menu_dropdown.pt",
    )
    config.add_panel(
        action_dropdown_panel,
        "action_dropdown",
        renderer="endi:templates/panels/widgets/menu_dropdown.pt",
    )
    config.add_panel(
        action_buttons_panel,
        "action_buttons",
        renderer="endi:templates/panels/widgets/action_buttons.pt",
    )
    config.add_panel(
        legend_panel,
        name="list_legend",
        renderer="endi:templates/panels/widgets/legend.pt",
    )
    config.add_panel(
        status_title_panel,
        name="status_title",
        renderer="endi:templates/panels/widgets/status_title.pt",
    )
