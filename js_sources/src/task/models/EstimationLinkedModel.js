import BaseModel from "../../base/models/BaseModel.js";
import Radio from "backbone.radio";


const EstimationLinkedModel = BaseModel.extend({
    props: [
        'estimation_id',
        'estimation',
    ],
    hasEstimationId(){
        return this.has('estimation_id');
    },
    label(){
        if (this.hasEstimationId()) {
            const estimation = this.get('estimation')
            const { name, internal_number }  = estimation;
            return `${name} ${internal_number}`;
        }
        return '';
    }
});
export default EstimationLinkedModel;