/*
 * Module name : RootComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import BaseProductModel from "../models/BaseProductModel.js";
import ProductWorkModel from "../models/ProductWorkModel.js";
import TrainingModel from "../models/TrainingModel.js";

import ProductListComponent from './ProductListComponent.js';
import ProductForm from './ProductForm.js';
import AddProductForm from './AddProductForm.js';

const template = require('./templates/RootComponent.mustache');

const RootComponent = Mn.View.extend({
    template: template,
    regions: {
        main: '.main',
        modalRegion: '.modal-container'
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {
        'list:filter': 'onListFilter',
        'list:navigate': 'onListNavigate',
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.filter_model = this.facade.request('get:model', 'ui_list_filter');
    },
    index(){
        /*
         * Show the List view
         */
        const collection = this.facade.request('get:collection', 'products');
        this.showChildView(
            'main', new ProductListComponent({
                collection: collection,
            })
        );
    },
    templateContext(){
        return {};
    },
    isLoaded(){
        /*
         * Check if datas has already been loaded (can not be the case in case
         * of other route income)
         */
        return ! _.isUndefined(this.collection)
    },
    showAddProductForm(model, collection){
        /*
         * Launched when an add button is clicked, build a temporary model and
         * shows the add form
         */

        let view = new AddProductForm(
            {
                model: model,
                destCollection: collection
            }
        );
        this.showChildView('main', view);
    },
    showEditProductForm(model){
        let view = new ProductForm(
            {
                model: model,
                destCollection: model.collection,
            }
        );
        this.showChildView('main', view);
    },
    showModal(view){
        this.showChildView('modalRegion', view);
    }
});
export default RootComponent
