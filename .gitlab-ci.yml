# Tested target system is Debian Buster with MariaDB 10

variables:
  MYSQL_DATABASE: endi
  MYSQL_USER: endi
  MYSQL_PASSWORD: endi
  MYSQL_ROOT_PASSWORD: root
  TZ: "Europe/Paris"
  LC_ALL: fr_FR.UTF-8
  LANG: fr_FR.UTF-8
  LANGUAGE: fr_FR.UTF-8
  PYTHONUNBUFFERED: 0
  PYTHONIOENCODING: UTF-8
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

services:
    - mariadb:10.3

stages:
  - build
  - tests
  - tests_with_data

################ COMMON BEFORE #################

before_script:
    - echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen
    - apt-get update -qq
    - "apt-get install -qq
      curl locales libfreetype6
      python3-minimal python3-wheel python3-distutils python-mysqldb
      shared-mime-info python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0
      "


################# JOB TEMPLATES ################

.debian_10_env:
  image: debian:10

.build_job:
  script:
    - "apt-get install -qq
      nodejs npm
      build-essential
      python3-pip python3-venv
      libmariadb-dev-compat libmariadbclient-dev
      libffi-dev
      libjpeg-dev libssl-dev libfreetype6-dev libxml2-dev zlib1g-dev libxslt1-dev # other py lib deps
      "
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install --upgrade setuptools pip
    - npm --cache .npm --prefix js_sources install
    - make devjs prodjs
    - pip install -e .
    - pip install -r test_requirements.txt

  artifacts:
    expire_in: 1 day
    paths:
      - venv/
      - endi.egg-info/
      - js_sources/node_modules/
      - endi/static/js/build/
  cache:
    paths:
      - .cache/pip/
      - .npm/


##################### JOBS ####################

build_debian_10:
  stage: build
  extends:
    - .debian_10_env
    - .build_job


unit_tests_debian_10:
  stage: tests
  needs:
    - build_debian_10
  extends:
    - .debian_10_env
  script:
    - source venv/bin/activate
    - py.test


import_reference_dump:
  stage: tests
  extends:
    - .debian_10_env
  needs:
    - build_debian_10

  script:
    - apt-get install -qq mariadb-client
    - source venv/bin/activate
    - endi-load-demo-data gitlab-ci.ini
    # Would fail in two cases:
    # 1. bug in a migration
    # 2. missing merge revision
    - endi-migrate gitlab-ci.ini upgrade
  after_script:
    - mysqldump -h mariadb --max-allowed-packet=1G -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > reference_dump_migrated.sql
  artifacts:
    expire_in: 1 day
    paths:
      - reference_dump_migrated.sql


test_anonymize_script:
  stage: tests_with_data
  # Ce job prend ~30min, il n'est pas raisonable de le lancer systématiquement
  when: manual
  extends:
    - .debian_10_env
  needs:
    - build_debian_10
    - import_reference_dump
  script:
    - source venv/bin/activate
    - apt-get install -qq mariadb-client
    - mysql -h mariadb -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < reference_dump_migrated.sql
    - endi-anonymize gitlab-ci.ini run
