<%inherit file="${context['main_template'].uri}" />
<%block name="content">
<div>
	<form method='POST' enctype="multipart/form-data" accept-charset="utf-8">
		% if api.has_permission('admin_competences', request.context):
			<div class="form-group">
				<label for="contractor_id">Entrepreneur à évaluer</label>
				<select name='contractor_id' class='form-control'>
					% for id, label in user_options:
						<option value='${id}'>${label}</option>
					% endfor
				</select>
			</div>
		% else:
			<h2>Mes compétences</h2>
			<input type="hidden" name='contractor_id' value="${request.context.id}" />
		% endif
		<div class="limited_width width40">
			<div class="form-group">
				<h3>Choisissez une échéance</h3>
				<div class='btn-group' role='group'>
					% for deadline in deadlines:
						<button
							class='btn'
							type='submit'
							name='deadline'
							value='${deadline.id}'>
							${deadline.label}
						</button>
					% endfor
				</div>
			</div>
		</div>
	</form>
</div>
</%block>
<%block name="footerjs">
% if api.has_permission('admin_competences', request.context):
$('select[name=contractor_id]').select2();
% endif
</%block>
