import Mn from 'backbone.marionette';

import FileRequirementCollection from '../models/FileRequirementCollection.js';
import CommonModel from "../models/CommonModel.js";
import GeneralModel from "../models/GeneralModel.js";
import ExpenseHtModel from "../models/ExpenseHtModel.js";
import NotesModel from "../models/NotesModel.js";
import PaymentConditionsModel from "../models/PaymentConditionsModel.js";
import TaskGroupCollection from '../models/TaskGroupCollection.js';
import { default as ProgressInvoicingTaskGroupCollection} from '../models/progress_invoicing/TaskGroupCollection.js';
import DiscountCollection from '../models/DiscountCollection.js';
import PaymentLineCollection from '../models/PaymentLineCollection.js';
import StatusHistoryCollection from '../models/StatusHistoryCollection.js';
import TotalModel from '../models/TotalModel.js';
import Radio from 'backbone.radio';
import CatalogTreeCollection from '../../common/models/CatalogTreeCollection.js';
import FacadeModelApiMixin from '../../base/components/FacadeModelApiMixin.js';
import EstimationLinkedModel from "../models/EstimationLinkedModel";
import { round } from "../../math.js"
import { ajax_call } from '../../tools.js';

const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    ht: 5,
    radioEvents: {
        'changed:task': 'computeTotals',
        'changed:discount': 'computeTotals',
        'changed:expense_ht': 'computeTotals',
        'changed:payment_lines': "updatePaymentLines",
        'changed:date': "onDateChanged",
    },
    radioRequests: {
        'get:model': 'getModelRequest',
        'get:collection': 'getCollectionRequest',
        'get:filerequirementcollection': 'getFileRequirementCollectionRequest',
        'get:totalmodel': 'getTotalModelRequest',
        'get:status_history_collection': 'getStatusHistory',
        'is:valid': "isDataValid",
        'has:filewarning': "hasFileWarning",
        'get:attachments': 'getAttachments',
        'load:model': 'loadModel',
        'load:collection': 'loadCollection',
        'save:model': 'saveModel',
        'save:all': 'saveAll'
    },
    initialize(options){
        this.syncModel = this.syncModel.bind(this);
        this.models = {};
        this.collections = {};
    },
    setupModels(loaded_datas){
        const context_datas = loaded_datas[0];
        console.log("setupModels");
        console.log(context_datas);
        this.attachments = context_datas.attachments;
        let collection;
        let model;

        this.models['general'].set(context_datas);
        this.models['common'].set(context_datas);
        this.models['expense_ht'].set(context_datas);
        this.models['notes'].set(context_datas);
        this.models['payment_conditions'].set(context_datas);
        this.models['estimation_linked'].set(context_datas);


        var file_requirements = context_datas['file_requirements'];
        this.file_requirements_collection = new FileRequirementCollection(
            file_requirements
        );


        if (_.has(context_datas, 'payment_lines')){
            var payment_lines = context_datas.payment_lines;
            this.collections['payment_lines'] = new PaymentLineCollection(
                payment_lines
            );
        }
        if (_.has(context_datas, 'status_history')){
            var history = context_datas['status_history'];
            this.collections['status_history'] = new StatusHistoryCollection(history);
        }
        this.computeTotals();
    },
    setup(options){
        /*
         * Lancé avant tout appel ajax, options : les options passées depuis la
         * page html
         */
        console.log("Facade.setup");
        this.app_options = _.clone(options);
    },
    onBeforeStart(options){
        /*
         * Lancé au lancement de l'application, met en place tous les modèles
         * ...
         */
        const collection = new CatalogTreeCollection();
        this.collections['catalog_tree'] = collection;
        this.models['total'] = new TotalModel();
        this.url = options['context_url'];

        this.collections.catalog_tree.url = options['catalog_tree_url'];
        
        if (! options.hasOwnProperty('invoicing_mode') || options.invoicing_mode == 'classic'){
            this.collections['task_groups'] = new TaskGroupCollection({});
        } else {
            this.collections['task_groups'] = new ProgressInvoicingTaskGroupCollection({});
        }
        if (options.hasOwnProperty('discount_api_url')){
            this.collections['discounts'] = new DiscountCollection({});
            this.collections['discounts'].url = options['discount_api_url'];
        }
        this.collections.task_groups.url = options['task_line_group_api_url'];

        this.models['general'] = new GeneralModel();
        this.models['general'].url = options['context_url'];
        this.models['common'] = new CommonModel();
        this.models['common'].url = options['context_url'];
        this.models['expense_ht'] = new ExpenseHtModel();
        this.models['expense_ht'].url = options['context_url'];
        this.models['notes'] = new NotesModel();
        this.models['notes'].url = options['context_url'];
        this.models['payment_conditions'] = new PaymentConditionsModel();
        this.models['payment_conditions'].url = options['context_url'];
        this.models['estimation_linked'] = new EstimationLinkedModel();
        this.models['estimation_linked'].url = options['context_url'];
    },
    start(){
        console.log("Starting the facade");
        this.onBeforeStart(this.app_options)
        let deferred = ajax_call(this.url);
        const load_task_line_group_request = this.loadCollection('task_groups');

        let requests;

        if (this.collections.hasOwnProperty('discounts')){
            const load_discount_request = this.loadCollection('discounts');
            requests = $.when(
                deferred,
                load_task_line_group_request,
                load_discount_request
            );
        } else {
            requests = $.when(
                deferred,
                load_task_line_group_request,
            );
        }
        return requests.then(this.setupModels.bind(this));
    },
    getAttachments(){
        return this.attachments;
    },
    getStatusHistory(){
        return this.collections['status_history'];
    },
    syncModel(modelName){
        var modelName = modelName || 'common';
        return this.models[modelName].save(null, {wait:true, sync: true, patch:true});
    },
    getPaymentCollectionRequest(){
        return this.payment_lines_collection;
    },
    getFileRequirementCollectionRequest(){
        return this.file_requirements_collection;
    },
    getTotalModelRequest(){
        return this.models['total'];
    },
    updatePaymentLines(){
        var channel = Radio.channel('facade');
        channel.trigger('update:payment_lines', this.models['total']);
    },
    computeTotals(){
        this.models['total'].set({
            'ht_before_discounts': this.HTBeforeDiscounts(),
            'ttc_before_discounts': this.TTCBeforeDiscounts(),
            'ht': this.HT(),
            'tvas': this.TVAParts(),
            'ttc': this.TTC()
        });
    },
    tasklines_ht(){
        return this.collections['task_groups'].ht();
    },
    getComputeCollections(){
        let result = [this.collections['task_groups']]
        if (this.collections.hasOwnProperty('discounts')){
            result.push(this.collections['discounts']);
        }
        return result;
    },
    getComputeModels(){
        return [this.models['expense_ht']];
    },
    HTBeforeDiscounts(){
        let result = this.HT();
        if (this.collections.hasOwnProperty('discounts')){
            result = result - this.collections.discounts.ht();
        }
        return result;
    },
    TTCBeforeDiscounts(){
        let result = this.TTC();
        if (this.collections.hasOwnProperty('discounts')){
            result = result - this.collections.discounts.ttc();
        }
        return result;
    },
    HT(){
        var result = 0;
        _.each(this.getComputeCollections(), function(collection){
            result += collection.ht();
        });
        _.each(this.getComputeModels(), function(model){
            result += model.ht();
        });
        console.log("Computing HT : %s", result)
        return result;
    },
    TVAParts(){
        var result = {};
        _.each(this.getComputeCollections(), function(collection){
            var tva_parts = collection.tvaParts();
            _.each(tva_parts, function(value, key){
                if (key in result){
                    value += result[key];
                }
                result[key] = value;
            });
        });
        _.each(this.getComputeModels(), function(model){
            var tva_parts = model.tvaParts();
            _.each(tva_parts, function(value, key){
                if (key in result){
                    value += result[key];
                }
                result[key] = value;
            });
        });
        return result;
    },
    TTC(){
        var result = round(this.HT());
        _.each(this.TVAParts(), function(value){
            result += round(value);
        });
        return result;
    },
    hasFileWarning(){
        return ! this.file_requirements_collection.validate();
    },
    isDataValid(){
        var channel = Radio.channel('facade');
        channel.trigger('bind:validation');
        var result = {};
        _.each(this.models, function(model){
            const validate_func = model['validate'];
            if (validate_func){
                var res = model.validate();
                if (res){
                    _.extend(result, res);
                }
            }
        });
        _.each(
            this.collections,
            function(collection, label){
                const validate_func = collection['validate'];
                if (validate_func){
                    var res = collection.validate();
                    if (res){
                        _.extend(result, res);
                    }
                }
            }
        );
        channel.trigger('unbind:validation');
        console.log("End of isDataValid");
        console.log(result);
        return result;
    },
    saveAll(){
        let this_ = this;
        let keys = _.keys(this.models);
        let deferreds = []
        keys.forEach(function(modelName){
            if (modelName != 'total'){
                deferreds.push(this_.saveModel(modelName));
            }
        })
        return $.when(...deferreds);
    },
    onDateChanged(){
        this.models.general.fetch();
    }
});
const Facade = new FacadeClass();
export default Facade;
