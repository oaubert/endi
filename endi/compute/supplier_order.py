from endi.compute import math_utils
from endi.compute.base_line import BaseLineCompute


class SupplierOrderCompute(object):
    """
    Attributs utilisés pour le calcul:

    lines : SupplierOrderLine[]
    cae_percentage: int
    """

    @property
    def total(self):
        return sum([line.total for line in self.lines])

    @property
    def cae_total(self):
        return math_utils.percentage(self.total, self.cae_percentage)

    @property
    def worker_total(self):
        return self.total - self.cae_total

    @property
    def total_tva(self):
        return sum([line.total_tva for line in self.lines])

    @property
    def total_ht(self):
        return sum([line.total_ht for line in self.lines])


class SupplierOrderLineCompute(BaseLineCompute):
    pass
