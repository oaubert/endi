/*
 * Module name : WorkItemView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import { formatAmount } from '../../math';

const template = require('./templates/WorkItemView.mustache');

const WorkItemView = Mn.View.extend({
    tagName: "tr",
    template: template,
    modelEvents: {'sync': 'render'},
    initialize(){
        this.user_prefs = Radio.channel('user_preferences');
    },
    templateContext(){
        console.log("WorkItemView Calling the templating context")
        const ht_label = this.user_prefs.request('formatAmount', this.model.get('ht'), false);
        const ht_full_label = formatAmount(this.model.get('ht'));
        
        return {
            tva_label: this.model.tva_label(),
            ht_label: ht_label,
            ht_full_label: ht_full_label,
            ht_rounded: ht_label != ht_full_label,    
            supplier_ht_label: this.user_prefs.request('formatAmount', this.model.get('supplier_ht'), false),
            supplier_ht_mode: this.model.supplier_ht_mode(),
            product_label: this.model.product_label(),
            work_unit_ht_label: this.user_prefs.request('formatAmount', this.model.get('work_unit_ht'), false),
            total_ht_label:this.user_prefs.request('formatAmount', this.model.get('total_ht'), false),
        };
    },
});
export default WorkItemView
