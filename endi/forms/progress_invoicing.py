"""
Form schemas used to edit an Invoice in progress_invoicing edition mode
"""
import colander

from endi.forms.tasks.base import deferred_default_name
from endi.forms.custom_types import (
    QuantityType,
)


class GroupStatusSchema(colander.Schema):
    @colander.instantiate()
    class LineStatusSchema(colander.SequenceSchema):
        name = 'lines'

        @colander.instantiate()
        class lines(colander.MappingSchema):
            id = colander.SchemaNode(colander.Integer(), title="Identifiant")
            current_percent = colander.SchemaNode(
                QuantityType(),
                title="Pourcentage à facturer",
                validator=colander.Range(0, 100)
            )


def get_edit_schema():
    """
    Build an edition schema used to validate the progress invoicing process

    :returns: An colanderalchemy SQLAlchemySchemaNode object
    """
    return GroupStatusSchema()


class NewInvoiceSchema(colander.Schema):
    name = colander.SchemaNode(
        colander.String(),
        title="Nom du document",
        description="Ce nom n'apparaît pas dans le document final",
        validator=colander.Length(max=255),
        default=deferred_default_name,
        missing="",
    )


def get_new_invoice_schema():
    """
    Build a colander schema for invoice add in progressing mode
    """
    return NewInvoiceSchema()
