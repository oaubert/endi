import _ from 'underscore';
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';


const CommonModel = BaseModel.extend({
    props: [
        'id',
        'name',
        'altdate',
        'date',
        'description',
        'address',
        'mentions',
        'workplace',
        'display_units',
        'display_ttc',
        'paymentDisplay',
        'deposit',
        'payment_times',
        'start_date',
        'validity_duration',
    ],
    validation: {
        date: {
            required: true,
            msg: "Veuillez saisir une date"
        },
        description: {
            required: true,
            msg: "Veuillez saisir un objet",
        },
        address: {
            required: true,
            msg: "Veuillez saisir une adresse",
        },
        start_date: {
            required: false,
            msg: "Veuillez saisir une date"
        },
        validity_duration: {
            required: false,
            msg: "Veuillez saisir une limite de validité"
        },
    },
    initialize(options){
        this.on('saved', this.onDateChanged);
    },
    onDateChanged(attributes){
        if (_.isObject(attributes)){
            if (attributes.hasOwnProperty('date')){
                const channel = Radio.channel('facade');
                channel.trigger('changed:date');
            }
        }
    }
});
export default CommonModel;
