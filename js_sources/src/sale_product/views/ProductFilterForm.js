/*
 * Module name : ProductFilterForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import { serializeForm } from '../../tools.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';

const template = require('./templates/ProductFilterForm.mustache');

const ProductFilterForm = Mn.View.extend({
    template: template,
    regions: {
        type_: '.field-type_',
        'name': '.field-name',
        'description': '.field-description',
        'category_id': '.field-category-id',
        'ref': '.field-ref',
        'supplier_id': '.field-supplier-id',
        'supplier_ref': '.field-supplier-ref',
    },
    ui: {
        submit_button: 'button[type=submit]',
        form: 'form'
    },
    events: {
        'click @ui.submit_button': 'onSubmitClicked'
    },
    initialize(){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.model = this.getOption('model');
        this.categories = this.facade.request(
            'get:collection',
            'categories'
        );
        this.listenTo(this.categories, 'add', this.render);
        this.listenTo(this.categories, 'sync', this.render);
        this.listenTo(this.categories, 'remove', this.render);
    },
    onRender(){
        let product_types = this.config.request('get:options', "product_types");
        console.log(product_types);
        this.showChildView(
            'type_',
            new SelectWidget({
                field_name: 'type_',
                label: 'Type de produit',
                options: product_types,
                id_key: 'value',
                label_key: 'label',
                defaultOption: {value: '', label: "Choisir un type de produit"},
                value: this.model.get('type_'),
            })
        );

        this.showChildView(
            'name',
            new InputWidget({
                field_name: 'search',
                placeholder: 'Nom',
                label: 'Nom interne',
                dataList: this.config.request('get:options', 'product_labels'),
                value: this.model.get('search'),
            })
        );

        this.showChildView(
            'description',
            new InputWidget({
                field_name: 'description',
                placeholder: 'Description',
                label: 'Description interne et notes',
                dataList: this.config.request('get:options', 'product_descriptions'),
                value: this.model.get('description'),
            })
        );

        this.showChildView(
            'supplier_ref',
            new InputWidget({
                field_name: 'supplier_ref',
                placeholder: 'Référence fournisseur',
                label: 'Référence fournisseur',
                dataList: this.config.request('get:options', 'product_suppliers_refs'),
                value: this.model.get('supplier_ref'),
            })
        );

        let categories = this.categories.toJSON();
        if (categories.length > 0){
            this.showChildView(
                'category_id',
                new SelectWidget({
                    field_name: 'category_id',
                    label: 'Catégorie',
                    options: categories,
                    id_key: 'id',
                    label_key: 'title',
                    defaultOption: {'id': '', 'title': 'Choisir une catégorie'},
                    value: this.model.get('category_id'),
                })
            );
        }
        this.showChildView(
            'ref',
            new InputWidget({
                field_name: 'ref',
                placeholder: 'Référence',
                label: 'Référence',
                dataList: this.config.request('get:options', 'references'),
                value: this.model.get('ref'),
            })
        );
        let suppliers = this.config.request('get:options', 'suppliers');
        if (suppliers.length > 0){
            this.showChildView(
                'supplier_id',
                new SelectWidget({
                    field_name: 'supplier_id',
                    label: 'Fournisseur',
                    options: suppliers,
                    id_key: 'id',
                    label_key: 'label',
                    defaultOption: {'id': '', 'label': 'Choisir un fournisseur'},
                    value: this.model.get('supplier_id'),
                })
            );
        }
    },
    templateContext(){
        return {
            productReferences: this.config.request('get:options', 'references'),

        };
    },
    onSubmitClicked(event){
        event.preventDefault();
        let datas = serializeForm(this.ui.form);
        this.model.set(datas);
        this.triggerMethod('filter:submit', this, datas);
    }
});
export default ProductFilterForm
