import Mn from 'backbone.marionette';
import { ajax_call } from '../../tools.js';
import { getPercent } from '../../math.js';
import TotalModel from '../models/TotalModel.js';
import SupplierOrderModel from '../models/SupplierOrderModel.js';
import SupplierOrderLineCollection from '../models/SupplierOrderLineCollection.js';


const FacadeClass = Mn.Object.extend({
    // FIXME: could take advantage of FacadeModelApiMixin
    channelName: 'facade',
    radioEvents: {
        "changed:line": "computeLineTotal",
        "changed:totals": "computeFundingTotals",
        "changed:order.cae_percentage": "computeFundingTotals",
    },
    radioRequests: {
        'get:collection': 'getCollectionRequest',
        'get:totalmodel': 'getTotalModelRequest',
        'get:model': 'getModelRequest',
    },
    start(){
        console.log("Starting the facade");
        let deferred = ajax_call(this.url);
        return deferred.then(this.setupModels.bind(this));
    },
    setup(options){
        console.log("Facade.setup");
        console.table(options);
        this.url = options['context_url'];
},
    setupModels(context_datas){
        this.datas = context_datas;
        this.models = {};
        this.collections = {};
        this.totalmodel = new TotalModel();

        var lines = context_datas['lines'];
        this.collections['lines'] = new SupplierOrderLineCollection(lines);

        this.supplierOrder = new SupplierOrderModel(context_datas);
        this.supplierOrder.url = AppOption['context_url'];

        this.computeLineTotal();
        this.computeFundingTotals();
    },
    computeLineTotal(){
        var collection = this.collections['lines'];

        var datas = {};
        datas['ht'] = collection.total_ht();
        datas['tva'] = collection.total_tva();
        datas['ttc'] = collection.total();
        var channel = this.getChannel();
        channel.trigger('change:lines');
        this.totalmodel.set(datas);

        // Refresh funding totals as totals changed
        this.computeFundingTotals();
    },
    computeFundingTotals() {
        var order = this.supplierOrder;
        var datas = {};
        var ttc = this.totalmodel.get('ttc');
        var caePercentage = order.get('cae_percentage');

        datas['ttc_cae'] = getPercent(ttc, caePercentage);
        datas['ttc_worker'] = ttc - datas['ttc_cae'];
        this.totalmodel.set(datas);
    },
    getCollectionRequest(label){
        return this.collections[label];
    },
    getTotalModelRequest(){
        return this.totalmodel;
    },
    getModelRequest() {
        return this.supplierOrder;
    },
});
const Facade = new FacadeClass();
export default Facade;
