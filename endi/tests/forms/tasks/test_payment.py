import colander
import pytest
import datetime
from endi.forms.tasks.payment import get_add_edit_payment_schema


NOW = datetime.datetime.now()


def test_paymentform_schema(
    dbsession,
    invoice,
    mode,
    get_csrf_request_with_db_and_user,
    bank,
    customer_bank,
    task_line,
    task_line_group,
    tva,
    user,
    customer,
):
    task_line_group.lines = [task_line]
    invoice.line_groups = [task_line_group]
    invoice.customer = customer
    from endi.forms.tasks.payment import PaymentSchema
    get_csrf_request_with_db_and_user.user = user
    get_csrf_request_with_db_and_user.context = invoice
    schema = PaymentSchema().bind(request=get_csrf_request_with_db_and_user)

    value = {
        'bank_remittance_id': 'Remittance',
        'amount': '12.53',
        'date': '2015-08-07',
        'bank_id': str(bank.id),
        'customer_bank_id': str(customer_bank.id),
        'check_number': '0123456789',
        'mode': mode.label,
        'resulted': True,
        'tva_id': str(tva.id),
        'issuer': customer.label,
    }
    expected_value = {
        'come_from': '',
        'bank_remittance_id': 'Remittance',
        'amount': 1253000,
        'date': datetime.date(2015, 8, 7),
        'bank_id': bank.id,
        'customer_bank_id': customer_bank.id,
        'check_number': '0123456789',
        'mode': mode.label,
        'resulted': True,
        'tva_id': tva.id,
        'issuer': customer.label,
    }
    assert schema.deserialize(value) == expected_value


def test_deferred_total_validator(
    invoice,
    get_csrf_request_with_db_and_user,
    mode,
    bank,
    customer_bank,
    task_line,
    task_line_group,
    tva,
    user,
    customer,
):
    invoice.line_groups = [task_line_group]
    invoice.customer = customer
    task_line_group.lines = [task_line]
    get_csrf_request_with_db_and_user.user = user
    from endi.forms.tasks.payment import PaymentSchema
    get_csrf_request_with_db_and_user.context = invoice
    schema = PaymentSchema().bind(request=get_csrf_request_with_db_and_user)

    value = {
        'bank_remittance_id': 'Remittance',
        'amount': '20.0',
        'date': '2015-08-07',
        'bank_id': str(bank.id),
        'customer_bank_id': str(customer_bank.id),
        'check_number': '0123456789',
        'mode': mode.label,
        'resulted': True,
        'tva_id': str(tva.id),
        'issuer': customer.label,
    }
    expected_value = {
        'bank_remittance_id': 'Remittance',
        'amount': 15500000,
        'date': datetime.date(2015, 8, 7),
        'bank_id': str(bank.id),
        'customer_bank_id': str(customer_bank.id),
        'check_number': '0123456789',
        'mode': mode.label,
        'resulted': True,
        'tva_id': tva.id,
        'issuer': customer.label,
    }
    schema.deserialize(value) == expected_value

    value = {
        'bank_remittance_id': 'Remittance',
        'amount': '21',
        'date': '2015-08-07',
        'bank_id': str(bank.id),
        'customer_bank_id': str(customer_bank.id),
        'check_number': '0123456789',
        'mode': mode.label,
        'resulted': True,
        'tva_id': str(tva.id),
        'issuer': customer.label,
    }
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_mode(mode):
    schema = get_add_edit_payment_schema(includes=('mode',))
    schema = schema.bind()

    value = {'mode': mode.label}
    assert schema.deserialize(value) == value

    value = {'mode': "error"}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)

    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_amount():
    schema = get_add_edit_payment_schema(includes=('amount',))
    schema = schema.bind()

    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)

    value = {'amount': 12.5}
    assert schema.deserialize(value) == {'amount': 1250000}

    value = {'amount': 'a'}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)
    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_bank_remittance_id():
    schema = get_add_edit_payment_schema(includes=('bank_remittance_id',))
    schema = schema.bind()

    value = {'bank_remittance_id': "test"}
    assert schema.deserialize(value) == value


def test_payment_date():
    schema = get_add_edit_payment_schema(includes=('date',))
    schema = schema.bind()
    value = {'date': NOW.isoformat()}
    assert schema.deserialize(value) == {'date': NOW}
    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_tva_id(tva):
    schema = get_add_edit_payment_schema(includes=('tva_id',))
    schema = schema.bind()

    value = {'tva_id': tva.id}

    value = {'tva_id': tva.id + 1}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)

    value = {}
    assert schema.deserialize(value) == value


def test_payment_bank_id(bank):
    schema = get_add_edit_payment_schema(includes=('bank_id',))
    schema = schema.bind()

    value = {'bank_id': bank.id}
    assert schema.deserialize(value) == value

    value = {'bank_id': bank.id + 1}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_customer_bank_id(customer_bank):
    schema = get_add_edit_payment_schema(includes=('customer_bank_id',))
    schema = schema.bind()

    value = {'customer_bank_id': customer_bank.id}
    assert schema.deserialize(value) == value

    value = {'customer_bank_id': customer_bank.id + 1}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_check_number():
    schema = get_add_edit_payment_schema(includes=('check_number',))
    schema = schema.bind()

    value = {'check_number': "0123456789"}
    assert schema.deserialize(value) == value


def test_payment_issuer(customer):
    schema = get_add_edit_payment_schema(includes=('issuer',))
    schema = schema.bind()

    value = {'issuer': customer.label}
    assert schema.deserialize(value) == value


def test_payment_user_id():
    schema = get_add_edit_payment_schema(includes=('user_id',))
    schema = schema.bind()

    value = {'user_id': 5}
    assert schema.deserialize(value) == value

    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment_task_id():
    schema = get_add_edit_payment_schema(includes=('task_id',))
    schema = schema.bind()

    value = {'task_id': 5}
    assert schema.deserialize(value) == value

    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_payment(mode, tva, bank, customer_bank, customer):
    schema = get_add_edit_payment_schema()
    schema = schema.bind()

    value = {
        'mode': mode.label,
        "amount": 12.5,
        "bank_remittance_id": "Remittance",
        "date": NOW.isoformat(),
        "tva_id": tva.id,
        "bank_id": bank.id,
        "customer_bank_id": customer_bank.id,
        "check_number": "0123456789",
        "user_id": 5,
        "task_id": 5,
        "issuer": customer.label,
    }

    expected_value = {
        'mode': mode.label,
        "amount": 1250000,
        "bank_remittance_id": "Remittance",
        "date": NOW,
        "tva_id": tva.id,
        "bank_id": bank.id,
        "customer_bank_id": customer_bank.id,
        "check_number": "0123456789",
        "user_id": 5,
        "task_id": 5,
        "issuer": customer.label,
    }
    result = schema.deserialize(value)

    for key, value in expected_value.items():
        assert result[key] == value
