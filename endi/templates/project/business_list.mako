<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
% if request.has_permission('add.estimation') or request.has_permission('add.invoice'):
	<div class='layout flex main_actions'>
		<div role='group'>
            % if request.has_permission('add.price_study'):
				<a class='btn btn-primary' href='${add_price_study_url}'>
                    ${api.icon('calculator')} Créer une étude de prix
				</a>
            % endif
			% if request.has_permission('add.estimation'):
				<a class='btn btn-primary' href='${add_estimation_url}'>
                    ${api.icon('file-list')} Créer un devis
				</a>
			% endif
			% if request.has_permission('add.invoice'):
				<a class='btn btn-primary' href='${add_invoice_url}'>
                    ${api.icon('file-invoice-euro')} Créer une facture
				</a>
			% endif

		</div>
	</div>
% endif
</%block>

<%block name='mainblock'>

${searchform()}

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
		<table class="top_align_table hover_table">
			<thead>
				<tr>
					<th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
					<th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
					<th scope="col" class="col_text">${sortable("Nom", "name")}</th>
					<th scope="col" class="col_text">Documents</th>
					<th scope="col" class="col_number">CA</th>
					<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
				</tr>
			</thead>
			<tbody>
				% if records:
					% for id, business in records:
						<tr class='tableelement business-closed-${business.closed} business-check-${business.file_requirement_service.check(business)}' title="Voir/Modifier cette affaire" aria-label="Voir/Modifier cette affaire">
                            <% url = request.route_path("/businesses/{id}", id=business.id) %>
                            <% onclick = "document.location='{url}'".format(url=url) %>
							% if business.closed:
								<td  onclick="${onclick}" class="col_status" title="Affaire clôturée">
									<span class="icon status draft">
                                        ${api.icon('folder')}
                                    </span>
							% else:
								<td  onclick="${onclick}" class="col_status" title="Affaire en cours">
									<span class="icon status valid">
                                        ${api.icon('folder-open')}
                                    </span>
							% endif
							</td>
							<td  onclick="${onclick}" class="col_date">${api.format_date(business.created_at)}</td>
							<td  onclick="${onclick}" class="col_text">${business.name}</td>
							<td  onclick="${onclick}" class="col_text">
								<ul>
									% for estimation in business.estimations:
										<li>Devis : ${estimation.name}</li>
									% endfor
									% for invoice in business.invoices:
										<li>
											${api.format_task_type(invoice)}
											% if invoice.official_number:
												n<span class="screen-reader-text">umér</span><sup>o</sup> ${invoice.official_number}
											% endif
											: ${invoice.name}
										</li>
									% endfor
								</ul>
							</td>
							<td  onclick="${onclick}" class="col_number">
								${api.format_amount(sum([i.ttc for i in business.invoices]), precision=5)}&nbsp;€
							</td>
							<td class='col_actions width_one'>
                               <a class='btn icon only' href="${request.route_path('/businesses/{id}', id=business.id)}" title="Voir/Modifier cette affaire" aria-label="Voir/Modifier cette affaire">
                                    ${api.icon('pen')}
                                </a>
							</td>
						</tr>
					% endfor
				% else:
					<tr>
						<td colspan='7' class="col_text"><em>Aucune affaire n’a été initiée pour l’instant</em></td>
					</tr>
				% endif
			</tbody>
		</table>
    </div>
    ${pager(records)}
</div>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
