<%inherit file="${context['main_template'].uri}" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <div role='group'></div>
    <div role='group'>
        <a class='btn' onclick="window.openPopup('${export_xls_url}');" href='javascript:void(0);' title="Export au format Excel (xls)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg> Excel
        </a>
        <a class='btn' onclick="window.openPopup('${export_ods_url}');" href='javascript:void(0);' title="Export au format Open Document (ods)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-spreadsheet"></use></svg> ODS
        </a>
    </div>
</div>
</%block>

<%block name='content'>

<div class='text_block'>
    <h2>
    	Vos comptes de résultat pour l’année <strong>${selected_year}</strong>
        % if current_year != selected_year:
            <% url = request.route_path("/companies/{id}/accounting/income_statement_measure_grids", id=request.context.id) %>
            <small>( <a href="${url}">voir l'année courante</a> )</small>
        % endif
    </h2>
    <h3>
        % if not grid.is_void and grid.get_updated_at() is not None:
            Mis à jour le 
            <strong>${grid.get_updated_at().strftime("%d/%m/%y")}</strong>
        % endif
    </h3>
</div>
<div class='search_filters'>
    ${form|n}
</div>

<div>
    % if not grid.is_void:
        <div class='table_container'>
            <table class="compte_resultat">
                <thead>
                    <tr class="row_month">
                        % for header in grid.stream_headers():
                            <th scope="col">${header}</th>
                        % endfor
                    </tr>
                </thead>
                <tbody>
                    % for type_, row in grid.rows:
                        <tr
                        % if type_.is_total:
                            class='row_recap row_number'
                        % else:
                            class='row_number'
                        % endif
                        >
                            <th scope="row">${type_.label |n }</th>
                            % for cell in row:
                                <td class='col_number'>${cell | n}</td>
                            % endfor
                        </tr>
                    % endfor
                </tbody>
            </table>
        </div>
    % else:
        <h4>Aucun compte de résultat n'est disponible</h4>
    % endif
    </div>
</div>
</%block>
