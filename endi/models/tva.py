"""
    Model for tva amounts
"""
import colander
import deform
import deform_extensions
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Boolean,
    Text,
    not_,
)
from sqlalchemy.orm import (
    relationship,
)

from endi.utils.html import clean_html
from endi.compute.math_utils import integer_to_amount
from endi.forms.custom_types import AmountType
from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)


TVA_GRID = (
    (('active', 6,),),
    (('name', 6,), ('value', 6),),
    (('mention', 12),),
    (('compte_cg', 4), ('code', 4), ('compte_a_payer', 4)),
    (('default', 6),),
    (('products', 12),),
)

PRODUCT_GRID = (
    (('name', 6), ('compte_cg', 6), ),
    (('active', 12), ),
)


class Tva(DBBASE):
    """
        `id` int(2) NOT NULL auto_increment,
        `name` varchar(8) NOT NULL,
        `value` int(5)
        `default` int(2) default 0 #rajouté par mise à jour 1.2
    """
    __colanderalchemy_config__ = {
        "title": "un taux de TVA",
        "validation_msg": "Les taux de Tva ont bien été configurés",
        "help_msg": """Configurez les taux de Tva disponibles utilisés dans \
enDI, ainsi que les produits associés.<br /> \
        Une Tva est composée :<ul><li>D'un libellé (ex : TVA 20%)</li> \
        <li>D'un montant (ex : 20)</li> \
        <li>D'un ensemble d'informations comptables</li> \
        <li>D'un ensemble de produits associés</li> \
        <li> D'une mention : si elle est renseignée, celle-ci viendra se placer
        en lieu et place du libellé (ex : Tva non applicable en vertu ...)
        </ul><br /> \
        <strong>Note : les montants doivent tous être distincts, si vous utilisez \
        plusieurs Tva à 0%, utilisez des montants négatifs pour les \
        différencier.</strong> \
        """,
        'widget': deform_extensions.GridFormWidget(named_grid=TVA_GRID)
    }
    __tablename__ = 'tva'
    __table_args__ = default_table_args
    id = Column(
        'id',
        Integer,
        primary_key=True,
        info={'colanderalchemy': {'widget': deform.widget.HiddenWidget()}},
    )
    active = Column(
        Boolean(),
        default=True,
        info={
            'colanderalchemy': {'exclude': True}
        },
    )
    name = Column(
        "name",
        String(15),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': 'Libellé du taux de TVA',
                }
        },
    )
    value = Column(
        "value",
        Integer,
        info={
            "colanderalchemy": {
                'title': 'Valeur',
                'typ': AmountType(),
                'description': "Le pourcentage associé (ex : 19.6)",
            }
        },
    )
    compte_cg = Column(
        "compte_cg",
        String(125),
        default="",
        info={'colanderalchemy': dict(title="Compte CG de Tva")}
    )
    code = Column(
        "code",
        String(125),
        default="",
        info={'colanderalchemy': dict(title="Code de Tva")}
    )
    compte_a_payer = Column(
        String(125),
        default='',
        info={'colanderalchemy': dict(
            title="Compte de Tva à payer",
            description="Utilisé dans les exports comptables des \
encaissements",
        )}
    )
    mention = Column(
        Text,
        info={
            'colanderalchemy': {
                'title': "Mentions spécifiques à cette TVA",
                'description': """Si cette Tva est utilisée dans un
devis/une facture,la mention apparaitra dans la sortie PDF
(ex: Mention pour la tva liée aux formations ...)""",
                'widget': deform.widget.TextAreaWidget(rows=1),
                'preparer': clean_html,
                'missing': "",
            }
        }
    )
    default = Column(
        "default",
        Boolean(),
        info={
            "colanderalchemy": {
                'title': 'Cette tva doit-elle être proposée par défaut ?'
            }
        },
    )
    products = relationship(
        "Product",
        cascade="all, delete-orphan",
        info={
            'colanderalchemy': {
                'title': "Comptes produit associés",
                "widget": deform.widget.SequenceWidget(
                    add_subitem_text_template="Ajouter un compte produit",
                )
            },
        },
        back_populates='tva',
    )

    @classmethod
    def query(cls, include_inactive=False):
        q = super(Tva, cls).query()
        if not include_inactive:
            q = q.filter(Tva.active == True)
        return q.order_by('value')

    @classmethod
    def by_value(cls, value):
        """
        Returns the Tva matching this value
        """
        return super(Tva, cls).query().filter(cls.value == value).one()

    @classmethod
    def get_default(cls):
        return cls.query().filter_by(default=True).first()

    def __json__(self, request):
        return dict(
            id=self.id,
            value=integer_to_amount(self.value, 2),
            label=self.name,
            name=self.name,
            default=self.default,
            products=[product.__json__(request) for product in self.products],
        )

    @classmethod
    def unique_value(cls, value, tva_id=None):
        """
        Check that the given value has not already been attributed to a tva
        entry

        :param int value: The value currently configured
        :param int tva_id: The optionnal id of the current tva object (edition
        mode)
        :returns: True/False
        :rtype: bool
        """
        query = cls.query(include_inactive=True)
        if tva_id:
            query = query.filter(not_(cls.id == tva_id))

        return query.filter_by(value=value).count() == 0


class Product(DBBASE):
    __colanderalchemy_config__ = {
        'title': "Compte produit",
        'widget': deform_extensions.GridMappingWidget(named_grid=PRODUCT_GRID)
    }
    __tablename__ = 'product'
    __table_args__ = default_table_args
    id = Column(
        'id',
        Integer,
        primary_key=True,
        info={'colanderalchemy': {'widget': deform.widget.HiddenWidget()}},
    )
    name = Column(
        "name",
        String(125),
        nullable=False,
        info={'colanderalchemy': {'title': 'Libellé', 'width': 6}}
    )
    compte_cg = Column(
        "compte_cg",
        String(125),
        info={'colanderalchemy': {'title': 'Compte CG', 'width': 6}}
    )
    active = Column(
        Boolean(),
        default=True,
        info={
            'colanderalchemy': {
                'title': "Activer ce produit ?",
                'description': "Si ce produit est inactif, il ne sera plus \
proposé dans l'interface de configuration des produits",
            }
        },
    )
    tva_id = Column(
        Integer,
        ForeignKey("tva.id", ondelete="cascade"),
        info={'colanderalchemy': {'exclude': True}}
    )
    tva = relationship(
        "Tva",
        back_populates="products",
        info={'colanderalchemy': {'exclude': True}}
    )

    def __json__(self, request):
        return dict(
            id=self.id,
            name=self.name,
            label=self.name,
            compte_cg=self.compte_cg,
            tva_id=self.tva_id,
        )

    @classmethod
    def query(cls, include_inactive=False):
        q = super(Product, cls).query()
        if not include_inactive:
            q = q.join(cls.tva)
            q = q.filter(Product.active == True)
            q = q.filter(Tva.active == True)
        return q.order_by('name')

    @classmethod
    def first_by_tva_value(cls, tva_value):
        try:
            tva = Tva.by_value(tva_value)
        except:
            return None

        res = DBSESSION().query(Product.id).filter_by(active=True).filter_by(
            tva_id=tva.id
        ).first()
        if res is not None:
            res = res[0]
        return res
